import { Dimensions } from 'react-native';
import Settings from '../constants/Settings'
var currencyFormatter = require('currency-formatter');




function FormateCurrency(numberString){
    return (currencyFormatter.format(numberString,{
        symbol: Settings.currency,
        decimal: '.',
        thousand: ',',        
        precision: 2,
        format: '%s %v' // %s is the symbol and %v is the value
      }))
 }
 
 function FormateCurrencyForPaypal(numberString){
  return (currencyFormatter.format(numberString,{
     
      decimal: '.',       
      precision: 2,
     
    }))
}

export default{
  FormateCurrency,
  FormateCurrencyForPaypal
};

