import React, { Component } from 'react';
import { TextInput, ScrollView, FlatList, SafeAreaView, TouchableOpacity, StyleSheet, Dimensions, Text, View } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from '../../constants/Settings';
import SearchBar from 'react-native-search-bar'

const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class SideBarContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchData: []
        };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
       
    }




    renderPage(Name, index) {
        return (
            <View key={index} style={{  width: '100%',borderBottomColor:'#c6c6c6',borderBottomWidth:1 }}>
            <View key={index} style={{ flexDirection: "column", width: '100%' }}>
                <TouchableOpacity style={{width:'100%'}} onPress={this.ProductDetails.bind(this, Name)}>
                  <Text style={{
                    fontSize: 14,
                    fontFamily: 'lucidabrightdemiregular', width: '100%',
                    textAlign: 'left',
                    paddingTop: 20,
                    paddingBottom: 20,
                    alignSelf: 'center',
                }}>{Name}</Text></TouchableOpacity>

            </View>
            </View>
        )
    }

    handleInputChangeName = (text) => {

        const newData = Settings.SerachData.filter(item => {

            const itemData = `${item.brief.name.toUpperCase()}   `;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });

        // console.log("newData", newData)
        this.setState({
            searchData: newData
        })

        Settings.AllPrtoductFilterName = text
    }
    onSubmitEdit() {
        this.props.action4(true);


    }
    ProductDetails(Name){
        Settings.AllPrtoductFilterName = Name
        this.props.action4(true);
    }
    
    async componentDidMount() {

        this.setState({ searchData: Settings.SerachData })
    }


    render() { 
        return (
            <View >
                <View style={{ backgroundColor: '#fff' }}>
                    <StickyHeaderFooterScrollView
                        makeScrollable={true}
                        fitToScreen={true}
                        contentBackgroundColor={'#fff'}

                        renderStickyHeader={() => (

                            <SafeAreaView>

                                <View style={s.header}>
                                    <View style={{ width: 40, alignContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={(term) => { this.props.action4(true); }} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>
                                    </View>
                                    <View style={{ height: 35, borderRadius: 11, borderWidth: 1, paddingRight: 5, paddingLeft: 6, borderColor: 'gray', width: windowhalfwidthSecond * 4, alignItems: "center", alignContent: 'center', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row" }}>
                                        <Icon2 name="Search" color={"gray"} size={14} style={{ fontWeight: 5 }} />
                                        <TextInput
                                            placeholder="Search"
                                            onChangeText={this.handleInputChangeName}
                                            onSubmitEditing={this.onSubmitEdit.bind(this)}
                                            value={this.state.Name}
                                            clearButtonMode='while-editing'
                                            style={{ borderWidth: 0, borderColor: '#fff', paddingRight: 5, paddingLeft: 6, width: '100%' }}
                                            underlineColorAndroid="transparent"
                                        />


                                    </View>
                                </View>

                            </SafeAreaView>
                        )}


                    >

                        <View style={{ margin: 20 }}>

                            {this.state.searchData.map((image, index) => this.renderPage(image.brief.name, index))}

                        </View>
                    </StickyHeaderFooterScrollView>

                </View>
            </View >
        );
    }
}

const s = StyleSheet.create({
    containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: .6,
    },
});