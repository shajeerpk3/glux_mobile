import settingss from '../../constants/Settings';

const servername = settingss.serverUrl2;
const endUrl = {
    accountdetails: servername + "/admin/user/detail",
    accountdetailsUpdate:servername + "/user/updateuser",
    // getordersummery: servername + "/admin/order",

    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
    } catch (e) {
        bodyIsJson = false;
    }

    const fullUrl = endpoint + constructParams(params);
    console.log("full monnnnnn", fullUrl)
    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...contentTypeHeader,
        },
        body: body,
    };

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {


            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });

}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}



function accountdetails(userid) {
    console.log("useridApi", userid)
    return callApi(endUrl.accountdetails + "?_id=" + userid, "GET", null, null);

}

function accountdetailsUpdate(Firstname, Lastname, Email, Language, Country, Gender, PhoneNo, DateofBirth, UserID) {

    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        Firstname: Firstname,
        Lastname: Lastname,
        Email: Email,
        Language: Language,
        Country: Country,
        Gender: Gender,
        PhoneNo: PhoneNo,
        DateofBirth: DateofBirth,
        UserID: UserID,
     
    });
console.log("body",body)
    return callApi(endUrl.accountdetailsUpdate +"?_id=" + UserID, "POST", body, null, settingss.authorization)
}


export default {
    accountdetails,
    accountdetailsUpdate
};