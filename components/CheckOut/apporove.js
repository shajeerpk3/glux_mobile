import React, { Component } from 'react';
import { ActivityIndicator, Picker, Alert, Image, Animated, TouchableOpacity, WebView, Text, View, Button, StyleSheet, ScrollView, Dimensions, Platform, TextInput } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
const halfwidth = Dimensions.get('window').width / 8;
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').height;

var date = new Date().getDate();
var month = new Date().getMonth() + 1; //Current Month
var year = new Date().getFullYear()
export default class Approve extends Component {

    static navigationOptions = {
        header: null,
    };



    CheckOUt() {
        Settings.nav.navigate(commonsettings.backButtonPage)
    }



    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>

                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>ORDERS</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>

                        </View>
                    </View>

                )}
                renderStickyFooter={() => (
                    <View>

                        <View style={s.containerButton}>
                            <TouchableOpacity onPress={this.CheckOUt.bind(this)} style={s.buttonContainer}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>CONTINUE SHOPPING </Text>
                            </TouchableOpacity >
                        </View>
                    </ View>

                )}
            >

                <View style={{ borderColor: '#fff', }}>
                    <View style={{ margin: 10, borderWidth: 1, borderColor: '#414142' }}>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 10,
                            alignSelf: 'center',
                            color: '#d87136'
                        }}>#{Settings.PaypaltranxactionId}</Text>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 10,
                            alignSelf: 'center',
                        }}>ORDER DATE: {month} ,{date},{year}</Text>
                        <View style={{ borderTopWidth: 1, borderColor: '#414142', justifyContent: 'flex-start' }}>
                            <View style={{ flexDirection: "row" }}>

                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helvetica', width: '100%',
                                    width: 100,
                                    padding: 5

                                }}>Total:</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'lucidabrightdemiregular', width: '100%',
                                    padding: 5


                                }}>{Settings.PaypalNetTotal}</Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>

                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helvetica', width: '100%',
                                    width: 100,
                                    padding: 5

                                }}>STATUS:</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'lucidabrightdemiregular', width: '100%',
                                    padding: 5


                                }}>Payment Taken</Text>
                            </View>
                        </View>
                    </View>

                </View>




            </StickyHeaderFooterScrollView>
        )
    }

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
