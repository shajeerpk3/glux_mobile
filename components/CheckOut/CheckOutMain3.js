import React from 'react';
import { Divider } from 'react-native-elements'
import { WebView, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import Api2 from "../../utils/Api/Checkout";
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Carousel from 'react-native-banner-carousel';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';
import ModalDropdown from 'react-native-modal-dropdown';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import CheckOUt3 from '../CheckOut/CheckOut3'
import { Row } from 'native-base';

const Icon = createIconSetFromFontello(fontelloConfig);
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowhalfwidthForIcon = ((windowwidth - 10) / 2) / 3;
const windowhalfwidthForLine = ((windowwidth - 10) / 2) / 2;
var order = [];
var productsinArray = [];
var productsinArrayPaypal = [];

export default class CheckOut3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            MerchId: 0,
            isLoading: true,
            images: [],
            dataSource: [],
            showDialogInfo: false
        };

        this.CreateProductObject = this.CreateProductObject.bind(this)
    }

    Checkout1() {
       
        this.saving(order)

       
    }

    checkoutsaving() {
     
        productsinArray = []
      
        productsinArrayPaypal = []
        Settings.productsinArrayPaypal = []
        console.log("poveyyyy",Settings.data)
        var prod = Settings.data.map((product, index) => this.productCreation(product, index))
        console.log("prooooo",prod);
        var amount = (parseFloat(Settings.ShoppingBagSubTotal) - parseFloat(Settings.promocode)) - parseFloat(Settings.gpoint)
        const BillingInfo = {
            Shipping: 0,
            SubTotal: parseFloat(currencyFormatter.FormateCurrencyForPaypal(Settings.ShoppingBagSubTotal)),
            Gpoints: currencyFormatter.FormateCurrencyForPaypal(Settings.TotalGpointErn),
            GpointsUsed: currencyFormatter.FormateCurrencyForPaypal(Settings.gpoint),
            NetAmount:parseFloat(currencyFormatter.FormateCurrencyForPaypal(amount))
        };

        Settings.PaypalSubTotal = currencyFormatter.FormateCurrencyForPaypal(Settings.ShoppingBagSubTotal);
        Settings.paypalGpointTotal = "-" + currencyFormatter.FormateCurrencyForPaypal(Settings.gpoint);
        Settings.PaypalNetTotal = currencyFormatter.FormateCurrencyForPaypal(amount);

        const deliveryadrress = {
            B_Fullname: Settings.Billingaddress[0].B_Fullname,
            B_Address1: Settings.Billingaddress[0].B_Address1,
            B_Address2: Settings.Billingaddress[0].B_Address2,
            B_Address3: Settings.Billingaddress[0].B_Address3,
            B_ContactNo: Settings.Billingaddress[0].B_ContactNo,
            B_Country: "Singapore",
            B_Postalcode: Settings.Billingaddress[0].B_Postalcode,
            S_Fullname: Settings.Billingaddress[0].S_Fullname,
            S_Address1: Settings.Billingaddress[0].S_Address1,
            S_Address2: Settings.Billingaddress[0].S_Address2,
            S_Address3: Settings.Billingaddress[0].S_Address3,
            S_ContactNo: Settings.Billingaddress[0].S_ContactNo,
            S_Country: "Singapore",
            S_Postalcode: Settings.Billingaddress[0].S_Postalcode,
        }

        Settings.shippingName = Settings.Billingaddress[0].S_Fullname;
        Settings.ShippingAddline1 = Settings.Billingaddress[0].S_Address1;
        Settings.ShippingAddline2 = Settings.Billingaddress[0].S_Address2;
        Settings.ShippingAddline3 = Settings.Billingaddress[0].S_Address3;
        Settings.shippingPostalCode = Settings.Billingaddress[0].S_Postalcode;
        Settings.shippingPhonenumber = Settings.Billingaddress[0].S_ContactNo;


        const arr = []

        const tranformOrder = {
            merchant_id: "5a017ec2818c02c9a798e8d9",
            products: productsinArray,
            BillingInfo: BillingInfo,
            delivery: deliveryadrress,
            memo: Settings.PaymentMethod,
            timeslot: Settings.DeliveryTime 
        }
        arr.push(tranformOrder)
        const promocode1 = []
        promocode1.push(Settings.promocodecode)
        order = {
            orders: arr,
            promo_codes: promocode1
        };


    }
    saving(order) {
        Settings.productsinArrayPaypal = productsinArrayPaypal;
        
        Api2.checkout(order)
            .then((responseJson) => {
                console.log("ResponseAnallleeeeeeee", responseJson[0])
                Settings.LastInsertOrderIdToPayapal=responseJson[0];
                if(Response[0]!="undefined"){
                    Settings.nav.navigate("Paypal")
                }
            })
            .catch((error) => {
                alert(error.message);
                console.log("EERRRR", error.message)
            });
    }
    productCreation(product, index) {

        product.products.map((item, index) => this.CreateProductObject(product.merchant._id, item.GLux_Pnts, item.product_id._id, item.cart_id, item.product_id.brief.images[0], item.product_id.pricing.discount_price2, item.qty, item.product_id.brief.stock, (item.product_id.brief.price - item.product_id.pricing.discount_price2), item.product_id.brief.name))
        return null
    }

    CreateProductObject(_id, GLux_Pnts, product_id, cart_id, images, price, qty, stock, discount_price, name) {

        this.setState({ MerchId: _id })


        productsinArrayPaypal.push({
            name: name,
            quantity: qty,
            price: price * qty,
            currency: 'SGD'
        })



        const variance = {
            qty: qty,
            cart_id: cart_id,
            product_id: product_id

        }
        const variance1 = []
        variance1.push(variance)
        productsinArray.push({
            product_id: product_id,
            variants: variance1
        })

        return null

    }

    async componentWillMount() {


        this.checkoutsaving();
    }

    clickBack() {

        Settings.nav.navigate("CheckOut2")

    }

    showdialog(showorhide) {
        this.setState({ showDialogInfo: showorhide })
    }



    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                            {commonsettings.backbuttonstatus && (<TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>)}
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>CHECKOUT</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                )}
                renderStickyFooter={() => (
                    <View>
                        <View style={s.containerButton}>
                            <TouchableOpacity style={s.buttonContainer} onPress={this.Checkout1.bind(this)}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>CONFIRM AND PAY ({Settings.BagItemCOunt} items) </Text>
                            </TouchableOpacity >
                        </View>

                    </ View>

                )}
            >





                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>


                    <Dialog
                        title="Please add Billing Address"
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                                backgroundColor: '#fff'
                            }
                        }
                        onTouchOutside={() => this.showdialog(false)}
                        visible={this.state.showDialogInfo}
                    >
                        <View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "center", marginTop: 10 }}>

                                <TouchableOpacity onPress={() => this.showdialog(false)} style={[s.buttonContainer1, { marginLeft: 2 }]}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Close </Text>
                                </TouchableOpacity >
                            </View>
                        </View>



                    </Dialog>

                    <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                    }}>


                        <View style={{ width: 270, height: 22, marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: '#262626'


                            }}>
                                SHIPPING
                            </Text>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: '#262626'


                            }}>
                                PAYMENT
                            </Text>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: '#262626'


                            }}>
                                CONFIRMATION
                            </Text>

                        </View>
                    </View>
                    <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                    }}>


                        <View style={{ width: 250, height: 22, marginTop: 5, flexDirection: 'row' }}>
                            {/* <Image
                                resizeMode="contain"
                                source={require('../../assets/images/cHECKoUT11.png')}
                                style={{ flex: 3, height: undefined, width: undefined }}
                            /> */}
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} />
                            </View>
                            <View style={{ width: 93, height: 22, flexDirection: 'row' }}>
                                <View style={{ width: 93, height: 13, borderColor: '#262626', borderBottomWidth: 2, flexDirection: 'row' }}></View>
                            </View>
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} />
                            </View>
                            <View style={{ width: 93, height: 22, flexDirection: 'row' }}>
                                <View style={{ width: 93, height: 13, borderColor: '#262626', borderBottomWidth: 2, flexDirection: 'row' }}></View>
                            </View>
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} />
                            </View>

                        </View>
                    </View>
                    <View style={{


                        alignItems: 'center',
                        marginTop: 15

                    }}>
                        <CheckOUt3 ></CheckOUt3>
                    </View>
                </View>
            </StickyHeaderFooterScrollView>
        )
    }


}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
