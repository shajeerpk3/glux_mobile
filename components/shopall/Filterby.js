
import React from 'react';
import { Dimensions, TouchableOpacity, ScrollView, View, StyleSheet, Text } from 'react-native';
import Api from "../../utils/Api/ShopallApis"
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import * as _ from 'lodash';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { ListItem, Separator, Row } from 'native-base';
import Settings from "../../constants/Settings"
const Windowheight = (Dimensions.get('window').height) / 2;
const Icon = createIconSetFromFontello(fontelloConfig);
const widthPannel = (Dimensions.get('window').width) - 46;
const color = [
    "White",
    "Tan",
    "Yellow",
    "Orange",
    "Red",
    "Pink",
    "Purple",
    "Blue",
    "Aqua",
    "Artichoke",
    "black",
    "Green"
];
const Designers = [
    "FENDI",
    "GUCCI",
    "LOUIS VUITTON",
    "LOVE MOSCHINO",
    "PRADA",
    "TORY BURCH",
    "TORY BURCH ",
    "FURLA",
    "SAINT LAURENT"
]

export default class Shopall extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            gender: false,
            dataSource: [],
            Designer: [],
            genderstate: Settings.Gender_filter,
            categorystate: Settings.category_filter,
            designstate: Settings.Designer_filter
        };
        this.onPressLearnMore = this.onPressLearnMore.bind(this);
    }


    handlefilter(item) {
        this.setState({ dataSource: item });

    }

    _dropdown_4_onSelect(idx, value) {
        console.log(idx, value);
    }


    groupBy(r, n) {
        if (3 === n) {
            return r;
        }
        return Object.keys(r).filter(e => 'undefined' !== e).map(k => {
            const e = _.groupBy(r[k], n + 1);
            const sub = this.groupBy(e, n + 1);
            return {
                name: k,
                sub: sub,
            };
        });
    }


    async componentWillMount() {

        // api calling at loading time for category level list and split and group category using lodash npm module
        Api.GetCategory({

        }
        )
            .then((responseJson) => {

                const array = responseJson.map(c => c.path.split(',').filter(e => '' !== e));

                this.setState({ dataSource: this.groupBy(_.groupBy(array, 0), 0) });

            })
            .catch((error) => {
                console.error(error);
            });


        // api calling at loading time for Designer
        // Api.GetDesigner(
        // )
        //     .then((responseJson) => {

        //         this.setState({ Designer: responseJson.value });
        //         console.log(this.state.Designer)
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });



    }

    // items binding in filter by
    renderItems(items, index) {
        return (
            <View key={index} style={styles.items}>
                <TouchableOpacity onPress={this.onPressLearnMore.bind(this, 'Designer', items)} style={{ width: '100%' }}>
                    <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}> {items}</Text>

                </TouchableOpacity >
            </View>
        )
    }


    //    second category item binding
    renderSubCateg2(Subcateg2, index, maincateg,subcateg1) {
        return (
            <View key={index} style={[styles.items]}>
                <TouchableOpacity onPress={this.onPressLearnMore.bind(this, "Category", maincateg+','+ subcateg1+','+ Subcateg2,)} style={{ width: '100%' }}>
                    <Text style={{ fontFamily: 'Helveticaregular', padding: 2, marginLeft: 30 }}> {Subcateg2}</Text>
                </TouchableOpacity >
            </View>
        )
    }

    onPressLearnMore(type, names) {

        if (type == "Category") {
            this.setState({ categorystate: names })
            Settings.category_filter = names;
        }
        if (type == "Designer") {
            Settings.Designer_filter = names;
           
            this.setState({ designstate: names })

        }
        if (type == "Gender") {
            this.setState({ genderstate: names })
            Settings.Gender_filter = names;
        }
        // // toggle time up and down icon changing
        // switch (type) {
        //     case "gender":
        //         return (
        //             this.setState({
        //                 gender: (this.state.gender === true) ? false : true,
        //             }));


        //     case "Colorss":
        //         return (
        //             this.setState({
        //                 Colorss: (this.state.Colorss === true) ? false : true,
        //             }));



        //     case "Desiner":
        //         return (
        //             this.setState({
        //                 Desiner: (this.state.Desiner === true) ? false : true,
        //             }));



        // }
    }


    renderSubCateg(Subcateg1, index, maincateg) {
        return (
            <Collapse key={index}>
                <CollapseHeader>
                    <View style={[styles.items, { flexDirection: "row", paddingLeft: 20 }]}>
                        <Text style={{ fontFamily: 'Helveticaregular', fontSize: 14, flex: 3 }}>{Subcateg1.name}</Text>
                        {/* {this.state.gender && (<Icon name="up-open" size={13} style={{ transform: [{ rotate: '180deg' }], fontWeight: 5 }} />)}
                                    {!this.state.gender && (<Icon name="up-open" size={13} style={{ fontWeight: 5 }} />)} */}
                        <Icon name="up-dir" size={16} style={{ fontWeight: 5 }} />
                    </View>
                </CollapseHeader>
                <CollapseBody>
                    {/* bind Subcategory second level list from api */}
                    {Subcateg1.sub.map((Subcateg2, index) =>
                        this.renderSubCateg2(Subcateg2.name, index, maincateg,Subcateg1.name)
                    )}
                </CollapseBody>
            </Collapse>
        )
    }
    renderMainCateg(Maincateg, index) {
        return (
            <Collapse key={index}>
                <CollapseHeader>

                    <View style={[styles.items, { flexDirection: "row", paddingLeft: 10 }]}>
                        <Text style={{ fontFamily: 'Helvetica', fontSize: 14, flex: 3, fontWeight: 'bold' }}>{Maincateg.name}</Text>
                        {/* {this.state.gender && (<Icon name="up-open" size={13} style={{ transform: [{ rotate: '180deg' }], fontWeight: 5 }} />)}
                                    {!this.state.gender && (<Icon name="up-open" size={13} style={{ fontWeight: 5 }} />)} */}
                        <Icon name="up-dir" size={16} style={{ fontWeight: 5 }} />
                    </View>

                </CollapseHeader>
                <CollapseBody>


                    {/* bind Subcategory first level list from api */}
                    {Maincateg.sub.map((Subcateg1, index) =>
                        this.renderSubCateg(Subcateg1, index, Maincateg.name)
                    )}


                </CollapseBody>
            </Collapse>
        )
    }
    render() {
        return (

            <View style={{ flexDirection: "column" }}>
                <ScrollView>
                    <View style={{ width: '100%', height: 80, borderBottomColor: 'gray', borderBottomWidth: .5, flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'column' }}><Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1, textDecorationLine: "underline" }}> Gender:</Text>
                            <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1 }}> {this.state.genderstate}</Text></View>

                        <View style={{ flexDirection: 'column' }}><Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1, textDecorationLine: "underline" }}> Designer:</Text>
                            <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1 }}> {this.state.designstate}</Text></View>
                        <View style={{ flexDirection: 'column', width:(widthPannel/3)-5 }}><Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1, textDecorationLine: "underline" }}> Category:</Text>
                            <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1 }}> {this.state.categorystate}</Text></View>
                    </View>
                    {/* <View style={{ Windowheight: Windowheight}}> */}

                    <View>


                        {/* Gender part */}
                        <Collapse
                            // isCollapsed={this.state.gender}
                            onToggle={({ nativeEvent }) => {
                                { }
                            }}>
                            <CollapseHeader>
                                <View style={[styles.CategoryMainContainer]}>
                                    <Text style={{ fontFamily: 'Helvetica', fontSize: 14, flex: 3, fontWeight: 'bold' }}>Gender</Text>
                                    {this.state.gender && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '270deg' }], fontWeight: 5 }} />)}
                                    {!this.state.gender && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '90deg' }], fontWeight: 5 }} />)}
                                </View>
                            </CollapseHeader>
                            <CollapseBody>
                                <View style={[styles.container, { borderBottomWidth: 0 }]}>
                                    <View style={styles.items}>
                                        <TouchableOpacity onPress={this.onPressLearnMore.bind(this, 'Gender', '')} style={{ width: '100%' }}>
                                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>All Gender</Text>

                                        </TouchableOpacity >
                                    </View>
                                    <View style={styles.items}>
                                        <TouchableOpacity onPress={this.onPressLearnMore.bind(this, 'Gender', 'Women')} style={{ width: '100%' }}>
                                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>Women</Text>

                                        </TouchableOpacity >
                                    </View>
                                    <View style={styles.items}>
                                        <TouchableOpacity onPress={this.onPressLearnMore.bind(this, 'Gender', 'Men')} style={{ width: '100%' }}>
                                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>Men</Text>

                                        </TouchableOpacity >


                                    </View>
                                </View>
                            </CollapseBody>
                        </Collapse>

                        {/* Designer  part */}

                        <Collapse isCollapsed={this.state.Desiner}
                            onToggle={({ nativeEvent }) => {
                                { this.onPressLearnMore('Desiner') }
                            }}>
                            <CollapseHeader>
                                <View style={[styles.CategoryMainContainer]}>
                                    <Text style={{ fontFamily: 'Helvetica', fontSize: 14, flex: 3, fontWeight: 'bold', fontWeight: 'bold' }}>Designer</Text>
                                    {this.state.Desiner && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '270deg' }], fontWeight: 5 }} />)}
                                    {!this.state.Desiner && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '90deg' }], fontWeight: 5 }} />)}
                                </View>
                            </CollapseHeader>
                            <CollapseBody>
                                <View style={[styles.container, { borderBottomWidth: 0 }]}>
                                    <View style={styles.items}>
                                        <TouchableOpacity onPress={this.onPressLearnMore} style={{ width: '100%' }}>
                                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>All Designer</Text>

                                        </TouchableOpacity >
                                    </View>
                                    {Designers.map((desiner, index) =>

                                        this.renderItems(desiner, index)
                                    )}
                                </View>
                            </CollapseBody>


                            {/* Color  part */}
                        </Collapse>
                        {/* <Collapse isCollapsed={this.state.Colorss}
                            onToggle={({ nativeEvent }) => {
                                { this.onPressLearnMore('Colorss') }
                            }}>
                            <CollapseHeader>
                                <View style={[styles.CategoryMainContainer]}>
                                    <Text style={{ fontFamily: 'Helvetica', fontSize: 14, flex: 3, fontWeight: 'bold' }}>Color</Text>
                                    {this.state.Colorss && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '270deg' }], fontWeight: 5 }} />)}
                                    {!this.state.Colorss && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '90deg' }],fontWeight: 5 }} />)}
                                </View>
                            </CollapseHeader>
                            <CollapseBody>
                                <View style={[styles.container, { borderBottomWidth: 0 }]}>
                                    <View style={styles.items}>
                                        <TouchableOpacity onPress={this.onPressLearnMore} style={{ width: '100%' }}>
                                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>All Color</Text>

                                        </TouchableOpacity >
                                    </View>
                                    {color.map((color, index) =>

                                        this.renderItems(color, index)
                                    )}
                                </View>
                            </CollapseBody>
                        </Collapse> */}



                        {/* Category  part */}

                        <Collapse isCollapsed={this.state.Category}
                            onToggle={({ nativeEvent }) => {
                                { }
                            }} >
                            <CollapseHeader>
                                <View style={[styles.CategoryMainContainer]}>
                                    <Text style={{ fontFamily: 'Helvetica', fontSize: 14, flex: 3, fontWeight: 'bold' }}>Category</Text>
                                    {this.state.Category && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '270deg' }], fontWeight: 5 }} />)}
                                    {!this.state.Category && (<Icon name="left-open" size={13} style={{ transform: [{ rotate: '90deg' }], fontWeight: 5 }} />)}
                                </View>
                            </CollapseHeader>
                            <CollapseBody>
                                <View style={[styles.container, { borderBottomWidth: 0 }]}>
                                    {/* bind category list from api */}

                                    {this.state.dataSource.map((Maincateg, index) =>

                                        this.renderMainCateg(Maincateg, index)
                                    )}
                                </View>
                            </CollapseBody>
                        </Collapse>



                    </View>
                </ScrollView>
                {/* </View> */}
            </View>
        );
    }





}

const styles = StyleSheet.create({

    CategoryMainContainer: {
        backgroundColor: "#00000000",
        borderBottomWidth: 1,
        borderBottomColor: '#969696',
        flexDirection: "row",
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 4,
        borderWidth: 1,
        borderColor: '#dedfe2',

    },
    items: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#dedfe2',
    },
    containerCateg: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 4,
        borderWidth: 1,
        borderColor: '#dedfe2',
        flexDirection: "row",
    },

});