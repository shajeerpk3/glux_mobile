import React from 'react';
import { TouchableOpacity, Dimensions, View, StyleSheet, Text } from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import Bagitems from '../ShoppingBag/BagitemMain'
import Settings from '../../constants/Settings';
import currencyFormatter from '../../constants/currncy_DateFormate';
const Icon = createIconSetFromFontello(fontelloConfig);
const windowwidth = Dimensions.get('window').width;



export default class ShyippingDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            ItemSummeryTab: false,
            // height: new Animated.Value(50),

        };


    }

    ClickItemSummeryTab() {
        if (this.state.ItemSummeryTab == false) {
            // tovalue = 500
            this.setState({ shippingTab: false })
            this.setState({ ItemSummeryTab: true })
        } else {
            // tovalue = 50
            this.setState({ shippingTab: true })
            this.setState({ ItemSummeryTab: false })
        }
    }



    _renderTensionView() {
        return (
            <Bagitems></Bagitems>
        )
    }


    render() {
        return (

            <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 5 }}>
                <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                    <TouchableOpacity onPress={this.ClickItemSummeryTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                        <View></View>
                        <Text style={{
                            fontSize: 18,
                            fontFamily: 'Helvetica',
                            textAlign: 'center',
                            alignSelf: 'center',
                            fontWeight: 'bold'
                        }}>ITEM SUMMARY
                   </Text>
                        <View style={{ justifyContent: 'center', alignItems: "center" }}>
                            {!this.state.ItemSummeryTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                }} />)}

                            {this.state.ItemSummeryTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                }} />
                            )}
                        </View>
                    </TouchableOpacity>
                </View>
                {!this.state.ItemSummeryTab && (<View >
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 3,marginTop:20 }}>
                        <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}>TOTAL G-POINTS EARNED   </Text>
                        <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}><Icon name="g-points" size={15} marginRight={5} style={{ fontWeight: 5 }} /> {Settings.TotalGpointErn} </Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 3, }}>
                        <Text style={{ fontSize: 17, fontFamily: 'Helvetica', fontWeight: 'bold' }}>TOTAL <Text style={{ fontSize: 17, fontFamily: 'Helveticaregular', fontWeight: 'normal' }}>({Settings.BagItemCOunt} items)</Text> </Text>
                        <Text style={{ fontSize: 17, fontFamily: 'Helvetica', fontWeight: 'bold' }}> {currencyFormatter.FormateCurrency(Settings.PaypalNetTotal)} </Text>
                    </View>
                </View>)}
                {this.state.ItemSummeryTab && (<View >
                    {this._renderTensionView()}
                </View>)}
            </View>
        )
    }
}