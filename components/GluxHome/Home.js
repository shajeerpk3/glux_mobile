import React from "react";
import { Text, Dimensions, svg, StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import commonsettings from '../../constants/commonsettings';
import DropdownAlert from 'react-native-dropdownalert';
import IconBadge from 'react-native-icon-badge';
import Api from "../../utils/Api/ProductDetailsApis";
import Settings from "../../constants/Settings"
import { Dialog } from 'react-native-simple-dialogs';


const windowhalfwidthSecond = Dimensions.get('window').width / 5;


const Icon = createIconSetFromFontello(fontelloConfig);
export default class HOME extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      BadgeCount: 0,
      dialogVisible:false
    }
  
    this.bindmsg = this.bindmsg.bind(this)
    this.cartCount = this.cartCount.bind(this);
    this.clickBag = this.clickBag.bind(this);
  }

  clickBack() {
    // props.navigation.goBack(commonsettings.backButtonPage)
    Settings.nav.navigate(commonsettings.backButtonPage)
  }

  clickBag() {
    // console.log("hello lolaa",props)
    if (Settings.LoginStatus != false) { Settings.nav.navigate("ShopingBag") }
    else {
      this.setState({dialogVisible:true})
    }

  }
  onPressRegister() {
    Settings.backfromLogin = "ShopallStack";
    Settings.RegisterLogin = "REGISTER";
    Settings.nav.navigate("MainLoginRegister")
  }
  onPressLogin() {
    Settings.backfromLogin = "ShopallStack";
    Settings.RegisterLogin = "LOGIN";
    Settings.nav.navigate("MainLogin")
  }

  cartCount() {

    let counting: Number = 0;
    Api.CartItem()

      .then((responseJson) => {
        responseJson.cart.map(function (product, i) {
          counting = counting + product.products.length;
        })

        if (this.state.BadgeCount != counting) { this.setState({ BadgeCount: counting }) }


      })
      .catch((error) => {


        this.bindmsg({ type: 'info', title: 'Stock', message: error.message });



      });

  }


  bindmsg(item) {
    this.Alert(item);
  }


  Alert(item) {

    switch (item.type) {
      case 'close':
        this.forceClose();
        break;
      default:
        const random = Math.floor(Math.random() * 1000 + 1);
        const title = item.type + ' #' + random;
        this.dropdown.alertWithType(item.type, item.title, item.message);
    }
  }


  onClose(data) {
    console.log("closing", data);
  }
  onCancel(data) {
    console.log(data);
  }
  async componentDidMount() {
    //  this.cartCount.bind(this);
    if (Settings.authorization != "") { this.cartCount(); }
  }


  render() {

    return (
      <View style={styles.header}>
        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
          {commonsettings.backbuttonstatus && (<TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>)}
        </View>
        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
        <Icon name="glux-logo" size={45} style={{ fontWeight: 5 }} ></Icon>
        </View>
        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row',marginLeft:10 }}>
          {/* <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
            <View style={{flexDirection:'column'}}><Icon name="shopping-bag" size={23} style={{ fontWeight: 5}} ></Icon>
              <View style={{ justifyContent: 'center',flexDirection:'row', alignContent: 'center'}}><Text style={{color:'red',fontSize:10}}>50</Text></View>
            </View>
          </TouchableOpacity> */}
           <Dialog
            visible={this.state.dialogVisible}
            title="SIGN IN"
            onTouchOutside={() => this.setState({ dialogVisible: false })} >
            <View>
              <View style={styles.container2}>
                <TouchableOpacity onPress={this.onPressRegister} style={styles.buttonContainer3}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>REGISTER</Text>

                </TouchableOpacity >
                <TouchableOpacity onPress={this.onPressLogin} style={styles.buttonContainer2}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc' }}>SIGN IN</Text>

                </TouchableOpacity >

              </View>
            </View>
          </Dialog>
          <TouchableOpacity onPress={this.clickBag.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
            <IconBadge
              MainElement={

                <Icon name="shopping-bag" size={30} style={{ fontWeight: 5,marginTop:10 }} ></Icon>


              }
              BadgeElement={

                <Text style={{ color: '#000000', textAlign: 'center', fontFamily: 'Helvetica' }}>{this.state.BadgeCount}</Text>
              }
              IconBadgeStyle={{
                width: 7.8,
                height: 13.8, backgroundColor: '#00000000', marginTop: 21,
              }}
              Hidden={this.state.BadgeCount == 0}
            />
          </TouchableOpacity>
        </View>
        <DropdownAlert
          ref={ref => this.dropdown = ref}
          showCancel={true}
          onClose={data => this.onClose(data)}
          onCancel={data => this.onCancel(data)}

        />
      </View >
    );
  }
}

const styles = StyleSheet.create({

  header: {
    height: 90,
    marginTop: 20,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 5,
    
    justifyContent: 'space-between'
  },
  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 15,
    paddingBottom: 15,
  
},
buttonContainer3: {
  backgroundColor: '#fff',
  borderRadius: 2,
  padding: 10,
  shadowColor: '#f9fafc',
  shadowOffset: {
      width: 0,
      height: 3
  },
  shadowRadius: 1,
  shadowOpacity: 0.25,
  borderWidth: 1, borderColor: '#414142',
  marginRight: 3,
  width: 110,
  alignItems: "center"

},
buttonContainer2: {
  backgroundColor: '#39393a',
  borderRadius: 2,
  padding: 10,
  shadowColor: '#ffffff',
  shadowOffset: {
      width: 0,
      height: 3
  },
  shadowRadius: 1,
  shadowOpacity: 0.25,
  borderWidth: 1, borderColor: '#414142',
  marginLeft: 3,
  width: 110,
  alignItems: "center"
},

});