import React from "react";
import { WebView, ScrollView, Text, TouchableOpacity, StyleSheet, View, Dimensions } from "react-native";
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import icoMoonConfig from '../../src/selection.json';
import Settings from "../../constants/Settings";
import GluxHome from "../GluxHome/Home";
import SearchHome from "../GluxHome/search";
import fontelloConfig from '../../src/config.json';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon = createIconSetFromFontello(fontelloConfig);
export default class Accounts extends React.Component {

    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);

    }

    clickBack() {
        Settings.nav.navigate("AccountsStack")

    }


    render() {
        return (
            <View>


                <StickyHeaderFooterScrollView
                    makeScrollable={true}
                    fitToScreen={true}
                    contentBackgroundColor={'#fff'}
                    renderStickyHeader={() => (

                        <View style={s.header}>
                            <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>
                            </View>
                            <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>{Settings.CondtionHeader}</Text>
                            </View>
                            <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                    <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                >

                    <View style={{padding:10}}>


                        <ScrollView>
                            <WebView
                                source={{ uri: Settings.CondtionUrl }}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                startInLoadingState={false}
                                style={{ marginTop: 20, height: 5000 }}
                            />
                        </ScrollView>


                    </View>


                </StickyHeaderFooterScrollView>
            </View>

        )
    }
}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});