import React from 'react';
import { ScrollView, Dimensions, Text, WebView, View, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import Api from "../../utils/Api/Paypal";
import Api2 from "../../utils/Api/PaypalPaiment";
import Api3 from "../../utils/Api/paypalExicut";
import Api4 from "../../utils/Api/PaypalSuccess";
import fontelloConfig from '../../src/config.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import Settings from '../../constants/Settings';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";

const Icon = createIconSetFromFontello(fontelloConfig);
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowhalfwidthForIcon = ((windowwidth - 10) / 2) / 3;
const windowhalfwidthForLine = ((windowwidth - 10) / 2) / 2;

var time = 1;


export default class Paypal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            approvalUrl: '',
            access_Token: '',
            payer_id: '',
            payment_Id: '',
            dialogVisible: false
        };
        this._onNavigationStateChange = this._onNavigationStateChange.bind(this)
        this.calltoSuccess=this.calltoSuccess.bind(this);
    }

    async componentWillMount() {
        Api.BasicAuthorization()

            .then((responseJson) => {
                this.setState({ access_Token: responseJson.access_token })
                this.callPayment(responseJson.access_token)
            })
            .catch((error) => {
                console.log("error313", error);
            });

    }
    clickBack() { Settings.nav.navigate("CheckOut3") }
    callPayment(Authtkn) {


        Api2.PaypallPaymnet(Authtkn)

            .then((responseJson) => {

                this.setState({ approvalUrl: responseJson.links[1].href })
                this.setState({ payment_Id: responseJson.id })

                this.setState({ isLoading: false });
            })
            .catch((error) => {


                console.log("error101", error);

            });



    }
    hideSpinner() {
        this.setState({ isLoading: false });
    }
    execute_Payment() {

        Api3.Execution(this.state.access_Token, this.state.payer_id, this.state.payment_Id)

            .then((responseJson) => {

                if (responseJson.state == "approved") {
                    
                    Settings.PaypaltranxactionId = responseJson.transactions[0].related_resources[0].sale.id;
                    Settings.nav.navigate("OrderSummery")
                    this.calltoSuccess(responseJson)
                }
            })
            .catch((error) => {


                console.log("error1001", error);

            });



    }
    _onNavigationStateChange(webViewState) {

        if (webViewState.loading == true && time == 1) {
            time = 2;
            this.setState({ isLoading: false });

        }
        this.setState({ dialogVisible: webViewState.loading });

        var paymentStatus = false;
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while (match = regex.exec(webViewState.url)) {


            if (match[1] == "PayerID") {
                this.setState({ payer_id: match[2] })

                paymentStatus = true;


            }
        }
        if (paymentStatus == true && this.state.payer_id != "") {
            this.execute_Payment();
        }


    }


    calltoSuccess(response) {
        var paypalresult={
            paymentToken:response.transactions[0].related_resources[0].sale.id,
            orderID:response.payer.payer_info.payer_id,
            payerID:response.transactions[0].payee.merchant_id
        }

        // console.log("paypalresult",paypalresult,Settings.LastInsertOrderIdToPayapal)
        Api4.PaypalSuccess(Settings.LastInsertOrderIdToPayapal,paypalresult)

        .then((responseJson) => {
           console.log("payapalMailsendingresult",responseJson)
        })
        .catch((error) => {
            console.log("error313", error);
        });

        // console.log("shajeer", paypalresult)
    }



    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ height: windowheight }}>
                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>PAYMENT</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 100 }}>
                        <ActivityIndicator size="large" color="gray" />
                    </View>
                </View>
            );

        }
        return (
            <View style={{ backgroundColor: "#fff", }}>
                <View style={s.header}>
                    <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                    </View>
                    <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>PAYMENT</Text>
                    </View>
                    <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                            <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                    <ProgressDialog
                        visible={this.state.dialogVisible}
                        title="Secure Payment"
                        message="Please, wait..."
                    />

                    <WebView
                        onLoad={() => this.hideSpinner()}
                        source={{ uri: this.state.approvalUrl }}
                        onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        injectedJavaScript={this.state.cookie}
                        startInLoadingState={false}
                        style={{ marginTop: 20, height: 500 }}
                    />
                </ScrollView>

            </View>
        )
    }
}


const s = StyleSheet.create({

    header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
})