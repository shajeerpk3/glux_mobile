import React, { Component } from 'react';
import { Alert, Image, Animated, TouchableOpacity, WebView, Text, View, Button, StyleSheet, ScrollView, Dimensions } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import ProductDeatailsMain from './ProductDeatailsMain';
import GluxHome from "../GluxHome/Home";
import ProductDetails from "./ProductDeatails";
import SearchHome from "../GluxHome/search";
import Api from "../../utils/Api/ProductDetailsApis";
import { Loading, EasyLoading } from 'react-native-easy-loading';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import DropdownAlert from 'react-native-dropdownalert';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings'
import { Dialog } from 'react-native-simple-dialogs';

const windowhalfwidthSecond = Dimensions.get('window').width / 3;
const halfwidth = Dimensions.get('window').width / 8;
const windowHeight = Dimensions.get('window').height;
const Icon = createIconSetFromFontello(fontelloConfig);


var prodid = ''
var merchid = ''
var imageurl = '' 
var smalltext = ''
var Name = ''
var Price = ''
var stock = 1
var GLux_Pnts = ''
let total: 0
export default class AllproductPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      stockForbutton: 1,
      height: new Animated.Value(1),
      dialogVisible: false
    };

    
  }

  static navigationOptions = {
    header: null,
  };
  async componentDidMount() {
    commonsettings.backbuttonstatus = true;
    commonsettings.backButtonPage = 'ShopallStack';
  }
  onPressRegister() {
    Settings.backfromLogin = "ShopallStack";
    Settings.RegisterLogin = "REGISTER";
    Settings.nav.navigate("MainLoginRegister")
  }
  onPressLogin() {
    Settings.backfromLogin = "ShopallStack";
    Settings.RegisterLogin = "LOGIN";
    Settings.nav.navigate("MainLogin")
  }

  async componentDidMount() {

    prodid = this.props.navigation.state.params.id;
    merchid = this.props.navigation.state.params.Merchentid;
    imageurl = this.props.navigation.state.params.ImageUrl;
    smalltext = this.props.navigation.state.params.smallText;
    Name = this.props.navigation.state.params.name;
    Price = this.props.navigation.state.params.Price;
    GLux_Pnts = Settings.GpointOfProductDetails;
    this.setState({ stockForbutton: this.props.navigation.state.params.stock })

    this.refs.ProductDetailsMain.componentDidMount(this.props.navigation.state.params.id);


  }

  CheckOut() {
    Api.CartItem()
      .then((responseJson) => {
      
        Settings.ShoppingBagSubTotal=0;
        Settings.PaypalNetTotal=0
        var counting = 0;
        Settings.data = responseJson.cart;
        Settings.data.map(function (product, i) {
          total = total + product.products[0].product_id.pricing.discount_price2;

          counting = counting + product.products.length;
          Settings.ShoppingBagSubTotal = total;
          Settings.PaypalNetTotal = total;
        })
        Settings.BagItemCOunt = counting;

        if (counting == 0) {
          commonsettings.bagCount = counting;
        }

        Settings.SHippingChageValue = Settings.ShippingCharge[0].value;
        Settings.SHippingChageLabel = Settings.ShippingCharge[0].label;
        Settings.SHippingChageLabel1 = Settings.ShippingCharge[0].label1;
        Settings.fillingfrom = false;

        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopallStack';

        Settings.nav.navigate("CheckOut")

      })
      .catch((error) => {
        console.log(error.message)
      });

  }

 


  GotoShoppingBag() {
    Settings.nav.navigate("ShopingBag")
  }

  Alert(item) {

    switch (item.type) {
      case 'close':
        this.forceClose();
        break;
      default:
        const random = Math.floor(Math.random() * 1000 + 1);
        const title = item.type + ' #' + random;
        this.dropdown.alertWithType(item.type, item.title, item.message);
    }
  }


  onClose(data) {
    console.log(data);
  }
  onCancel(data) {
    console.log(data);
  }


  addtoCart() {

    if (Settings.LoginStatus == false) {
      this.setState({ dialogVisible: true })
      return
    }


    setTimeout(() => {
      EasyLoading.show('', 1000);
    })
    // Api.AddToCart(datavalue.product._id, datavalue.product.merchant_id._id, 1)
    Api.AddToCart(prodid, merchid, 1, Settings.GpointOfProductDetails)

      .then((responseJson) => {

        // this.props.action1({ type: 'success', message: 'bghjghgh', title: 'khjjhj' });
        // console.log("responseJson", responseJson)
        // this.Alert({ type: 'sucess', message: 'bghjghgh', title: 'khjjhj' })
        //console.log("responseJson1",responseJson);

        if (responseJson.message == "Added to cart.") {
          this.refs.GluxHome.cartCount();
          this.setState({ modalVisible: true });
          EasyLoading.dismis();
          Animated.timing(                  // Animate over time
            this.state.height,            // The animated value to drive
            {
              toValue: (windowHeight / 2) + 100,                   // Animate to opacity: 1 (opaque)
              duration: 450,              // Make it take a while
            }
          ).start();
        }
      })
      .catch((error) => {

        if (error.message == "Insufficient stock") {
          EasyLoading.dismis();
          // Alert.alert("Insufficient stock",'kkkk')
          // Alert.alert(
          //   'Insufficient stock',
          //   'No stock',

          // )

          this.Alert({ type: 'info', title: 'Stock', message: 'Out of stock' });
        }
        // this.props.action1({ type: 'success', message: 'bghjghgh', title: 'khjjhj' });
        this.Alert({ type: 'error', message: error.message, title: 'Out of stock' })
        // console.log("error", error);

      });


  }



  render() {
    return (
      <StickyHeaderFooterScrollView
        makeScrollable={true}
        fitToScreen={true}
        contentBackgroundColor={'#fff'}

        renderStickyHeader={() => (
          <View>

          </View>
        )}
        renderStickyFooter={() => (
          <View>
            {!this.state.modalVisible && (<View style={s.containerButton}>
              {this.state.stockForbutton != 0 && (<TouchableOpacity style={s.buttonContainer} onPress={this.addtoCart.bind(this)}>
                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>ADD TO SHOPPING BAG </Text>
              </TouchableOpacity >)}

              {this.state.stockForbutton == 0 && (<View opacity={0.3} style={s.buttonContainer} onPress={this.addtoCart.bind(this)}>
                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>SOLD OUT </Text>
              </View >)}

            </View>)}

            {this.state.modalVisible && (<Animated.View style={{
              height: this.state.height,
              backgroundColor: '#fff',
              justifyContent: 'center',
              alignItems: 'center',
              borderTopWidth: 0.3,
              borderColor: '#000',
              shadowColor: '#000',
              shadowOffset: { width: 0, height: 10 },
              shadowOpacity: 10,
              marginTop: 10,
              elevation: 10,

            }}>
              <View style={{ height: '100%' }}>

                <View style={{ height: '95%', flexDirection: 'column', justifyContent: 'space-between', padding: 5 }}>

                  <View style={{ flexDirection: "row", paddingTop: 10, marginTop: 0 }}>
                    {/* <View style={{ width: halfwidth * 7, alignContent: 'center', alignItems: 'center', padding: 2 }}> */}
                    <View style={{ width: '85%', alignContent: 'center', alignItems: 'center', padding: 2, marginRight: -5 }}>
                      <Text style={{ fontFamily: 'Helvetica', color: 'black', fontSize: 17, textAlign: 'center' }}>JUST ADDED TO SHOPPING BAG</Text>
                    </View>
                    <View style={{ width: '15%', padding: 5, alignContent: 'center', alignItems: 'center' }}>
                      <TouchableOpacity onPress={() => this.setState({ modalVisible: false })} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="cancel-1" size={18} style={{ fontWeight: 5 }} /></TouchableOpacity>
                    </View>

                  </View>
                  <ScrollView style={{ marginBottom: 5 }}>
                    <View style={{ marginTop: 10 }}>
                      <View style={{ flexDirection: 'column', width: '100%', alignContent: 'center', justifyContent: 'center', alignItems: 'center' }}>

                        <Image style={{
                          width: '90%',
                          height: (windowHeight / 2) - 50,
                          overflow: 'hidden',
                          alignItems: 'center',

                          position: 'relative',

                        }} source={{ uri: imageurl }} />
                        <Text style={{
                          fontSize: 15,
                          width: '70%',
                          fontFamily: 'lucidabrightdemiregular',
                          textAlign: 'center',
                          paddingTop: 20,
                          alignSelf: 'center',
                        }}>{Name}</Text>
                        <Text style={{
                          fontSize: 15,
                          width: '60%',
                          fontFamily: 'Helveticaregular',
                          textAlign: 'center',
                          paddingTop: 1,
                          alignSelf: 'center',
                        }}>{smalltext}</Text>

                        <Text style={{
                          fontSize: 12,
                          width: '60%',
                          fontFamily: 'Helvetica',
                          textAlign: 'center',
                          paddingTop: 1,
                          fontWeight: 'bold'
                        }} >{Price}</Text>
                      </View>
                    </View>
                  </ScrollView>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                    <View style={[s.container, { width: '96%', marginLeft: 3, marginRight: 1, alignContent: 'center', justifyContent: 'center', alignItems: 'center', backgroundColor: '#39393a' }]}>
                      <TouchableOpacity onPress={this.GotoShoppingBag.bind(this)} style={[s.buttonContainer, { width: '90%', alignContent: 'center', justifyContent: 'center', alignItems: 'center' }]}>
                        <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc', textAlign: 'center' }}>VIEW SHOPPING BAG</Text>

                      </TouchableOpacity >
                    </View>
                    {/* <View style={[s.container, { width: '48%', marginLeft: 1, marginRight: 3, alignContent: 'center', justifyContent: 'center', alignItems: 'center', backgroundColor: '#39393a' }]}>
                      <TouchableOpacity onPress={this.CheckOut} style={[s.buttonContainer, { width: '90%', alignContent: 'center', justifyContent: 'center', alignItems: 'center' }]}>
                        <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc', textAlign: 'center' }}>PROCEED TO PURCHASE</Text>

                      </TouchableOpacity >
                    </View> */}
                  </View>



                </View>


              </View>

            </Animated.View>)}
          </ View>

        )}
      >
        <View style={{ backgroundColor: '#fff' }}>
          <GluxHome ref='GluxHome' ></GluxHome>
          <SearchHome></SearchHome>
          <Dialog
            visible={this.state.dialogVisible}
            title="SIGN IN"
            onTouchOutside={() => this.setState({ dialogVisible: false })} >
            <View>
              <View style={s.container2}>
                <TouchableOpacity onPress={this.onPressRegister} style={s.buttonContainer3}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2 }}>REGISTER</Text>

                </TouchableOpacity >
                <TouchableOpacity onPress={this.onPressLogin} style={s.buttonContainer2}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc' }}>SIGN IN</Text>

                </TouchableOpacity >

              </View>
            </View>
          </Dialog>
          <ProductDeatailsMain ref='ProductDetailsMain'></ProductDeatailsMain>
          <DropdownAlert
            ref={ref => this.dropdown = ref}
            showCancel={true}
            onClose={data => this.onClose(data)}
            onCancel={data => this.onCancel(data)}
            infoColor='gray'
          />
          <Loading />
          <Loading type={"type"} loadingStyle={{ backgroundColor: "#0b61ea" }} />
          {/* <Modal animationInTiming={3000}  isVisible={this.state.modalVisible}>
          <View style={{marginBottom:1,backgroundColor:'#fff',height:'50%' }}>
            <Text>Hello!</Text>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Hide me!</Text>
            </TouchableOpacity>
          </View>
        </Modal> */}
        </View>
      </StickyHeaderFooterScrollView>
    )
  }
}


const s = StyleSheet.create({
  buttonContainer: {

    backgroundColor: '#39393a',
    padding: 3,
    paddingTop: 10,
    paddingBottom: 10,


    shadowColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1,
    borderColor: '#414142',

    width: '100%',

    alignItems: "center"
  }, containerButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 20
  }, container: {
    backgroundColor: '#e2e2e2',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',


  },
  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 15,

    paddingBottom: 15,

  },
  buttonContainer3: {
    backgroundColor: '#fff',
    borderRadius: 2,
    padding: 10,
    shadowColor: '#f9fafc',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: '#414142',
    marginRight: 3,
    width: 110,
    alignItems: "center"

  },
  buttonContainer2: {
    backgroundColor: '#39393a',
    borderRadius: 2,
    padding: 10,
    shadowColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: '#414142',
    marginLeft: 3,
    width: 110,
    alignItems: "center"
  },
});