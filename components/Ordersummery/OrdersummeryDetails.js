import React from 'react';
import { Divider } from 'react-native-elements'
import { WebView, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Carousel from 'react-native-banner-carousel';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';
import ModalDropdown from 'react-native-modal-dropdown';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import CheckOUt1 from '../CheckOut/CheckOUT1'
import { Row } from 'native-base';


const Icon = createIconSetFromFontello(fontelloConfig);
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowhalfwidthForIcon = ((windowwidth - 10) / 2) / 3;
const windowhalfwidthForLine = ((windowwidth - 10) / 2) / 2;

export default class OrderSummeryDetails extends React.Component {


    constructor(props) {
        super(props);
        this.state = {

            isLoading: true,
            product: '',
            BillingInfo: '',
            DeliveryInfo: '',
            Id: '',
            DateString: '',
            status: '',
            OrderSummeryTab: false,
            shippingTab: false,
            PAYMENTTab: false

        };


    }

    async componentWillMount() {
        this.setState({ product: this.props.navigation.state.params.product })
        this.setState({ BillingInfo: this.props.navigation.state.params.BillingInfo })
        this.setState({ DeliveryInfo: this.props.navigation.state.params.DeliveryInfo })
        this.setState({ Id: this.props.navigation.state.params.Id })
        this.setState({ DateString: this.props.navigation.state.params.DateString })
        this.setState({ status: this.props.navigation.state.params.status })


        // console.log("this.props.navigation.state.params.product", this.props.navigation.state.params.product)



    }


    clickBack() {

        Settings.nav.navigate('OrderSummery')

    }
    ClickShipping() {
        if (this.state.shippingTab == false) {
            this.setState({ PAYMENTTab: false })
            this.setState({ OrderSummeryTab: false })
            this.setState({ shippingTab: true })
        } else {
            this.setState({ shippingTab: false })
        }
    }


    ClickPAYMENT() {
        if (this.state.PAYMENTTab == false) {

            this.setState({ OrderSummeryTab: false })
            this.setState({ shippingTab: false })
            this.setState({ PAYMENTTab: true })
        } else {
            this.setState({ PAYMENTTab: false })
        }
    }


    ClickOrderSummery() {


        if (this.state.OrderSummeryTab == false) {
            this.setState({ PAYMENTTab: false })
            this.setState({ shippingTab: false })
            this.setState({ OrderSummeryTab: true })
        } else {

            this.setState({ OrderSummeryTab: false })
        }


    }
    render3(item) {

        return (
            <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column', justifyContent: "space-between" }}>
                <View style={{ flexDirection: 'column', padding: 2, paddingTop: 0 }}>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helvetica', width: '100%',
                        textAlign: 'left',
                        paddingTop: 2,
                        paddingBottom: 2,
                        alignSelf: 'flex-start',
                        fontWeight: 'bold'
                    }}>Payment Status     : <Text style={{
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontFamily: 'Helveticaregular',
                    }}>{this.state.status} </Text> </Text>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helvetica', width: '100%',
                        textAlign: 'left',
                        paddingTop: 2,
                        paddingBottom: 2,
                        alignSelf: 'flex-start',
                        fontWeight: 'bold'
                    }}>Payment Method  : <Text style={{
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontFamily: 'Helveticaregular',
                    }}> </Text> </Text>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helvetica', width: '100%',
                        textAlign: 'left',
                        paddingTop: 2,
                        paddingBottom: 15,
                        alignSelf: 'flex-start',
                        fontWeight: 'bold'
                    }}>Billing Address      : <Text style={{
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontFamily: 'Helveticaregular',
                    }}>{item.B_Fullname}{"\n"}                                   {item.B_Address1}{"\n"}                                   {item.B_Address2}{"\n"}                                   {item.B_Postalcode}{"\n"}                                   {item.B_Country} </Text> </Text>
                </View>
            </View>)
    }


    render2(item) {

        return (
            <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column', justifyContent: "space-between" }}>
                <View style={{ flexDirection: 'column', padding: 2, paddingTop: 0 }}>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helvetica', width: '100%',
                        textAlign: 'left',
                        paddingTop: 2,
                        paddingBottom: 2,
                        alignSelf: 'flex-start',
                        fontWeight: 'bold'
                    }}>Contact Number    : <Text style={{
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontFamily: 'Helveticaregular',
                    }}>{item.S_ContactNo} </Text> </Text>

                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helvetica', width: '100%',
                        textAlign: 'left',
                        paddingTop: 2,
                        paddingBottom: 15,
                        alignSelf: 'flex-start',
                        fontWeight: 'bold'
                    }}>Shipping Address : <Text style={{
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontFamily: 'Helveticaregular',
                    }}>{item.S_Fullname}{"\n"}                                   {item.S_Address1}{"\n"}                                   {item.S_Address2}{"\n"}                                   {item.S_Postalcode}{"\n"}                                   {item.S_Country} </Text> </Text>
                </View>
            </View>)
    }


    render1(item, index) {

        return (
            <View key={index} style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column', justifyContent: "space-between" }}>
                <View style={{ flexDirection: 'row', padding: 2, paddingTop: 0 }}>

                    <Image
                        source={{ uri: "http://buyerservices.glux.sg/uploads/user/undefined/products/" + item.product._id + "/" + item.product.brief.images[0] }}
                        style={{ height: (windowheight / 3), width: '50%', alignItems: 'flex-start', justifyContent: 'flex-start', borderWidth: 1, borderColor: '#fff' }} />

                    <View style={{ flexDirection: 'column', width: '50%' }}>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'left',
                            paddingTop: 20,
                            alignSelf: 'flex-start',
                        }}>{item.product.detail.product_brand}</Text>

                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'left',
                            paddingTop: 1,
                            alignSelf: 'flex-start',
                        }}>{item.product.brief.name}</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'left',
                            paddingTop: 1,
                            fontSize: 14,
                            fontFamily: 'Helvetica',
                            fontWeight: 'bold',
                            color: '#999797'

                        }} >Quantity:{item.variants[0].order_qty}</Text>
                        {(item.product.pricing.discount_price2 != item.product.brief.price) && (item.product.pricing.discount_price2 != 0) && (
                            <Text style={{
                                width: '100%',
                                textAlign: 'left',
                                paddingTop: 1,
                                fontSize: 16,

                                fontFamily: 'Helvetica',
                                fontWeight: 'bold',
                                color: '#c6c6c6',
                                textDecorationLine: "line-through"
                            }} >{currencyFormatter.FormateCurrency(item.product.brief.price)}</Text>)}
                        {(item.product.pricing.discount_price2 != item.product.brief.price) && (item.product.pricing.discount_price2 != 0) && (
                            <Text style={{
                                width: '100%',
                                textAlign: 'left',
                                paddingTop: 1,
                                fontSize: 12,

                                fontFamily: 'Helvetica',
                                fontWeight: 'bold',
                                color: 'green',

                            }} >saved {currencyFormatter.FormateCurrency(item.product.brief.price - item.product.pricing.discount_price2)}</Text>)}
                        <Text style={{
                            width: '100%',
                            textAlign: 'left',
                            paddingTop: 1,
                            fontSize: 16,
                            fontFamily: 10,
                            fontFamily: 'Helvetica',
                            fontWeight: 'bold'

                        }} >{currencyFormatter.FormateCurrency(item.product.pricing.discount_price2)}</Text>

                        {/* <Text style={{
                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'left',

                            alignSelf: 'flex-start',
                        }}>EARN <Icon name="g-points" size={18} style={{ fontWeight: 5, marginRight: 20 }} /> 2 points</Text> */}

                    </View>

                </View>
            </View>)
    }


    render() {
        return (
            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>ORDER DETAILS</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                )}

            >
                <View style={{ alignItems: 'center',marginBottom:30 }}>
                    <View style={{ borderBttomColor: '#323233', borderBottomWidth: 2, width: windowwidth - 20, marginTop: 10 }}>
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{
                                fontSize: 20,
                                fontFamily: 'Helvetica', width: '100%',
                                textAlign: 'left',
                                paddingTop: 15,
                                alignSelf: 'flex-start',
                                color: '#d87136'
                            }}>#{this.state.Id}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helvetica',
                                textAlign: 'left',
                                paddingTop: 2,
                                paddingBottom: 15,
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}>DATE: <Text style={{
                                fontSize: 16,
                                fontWeight: 'normal',
                                fontFamily: 'Helveticaregular',
                            }}>{this.state.DateString}</Text> </Text>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helvetica',
                                textAlign: 'left',
                                paddingTop: 2,
                                paddingBottom: 15,
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}>STATUS: <Text style={{
                                fontSize: 16,
                                fontWeight: 'normal',
                                fontFamily: 'Helveticaregular',
                            }}>{this.state.status} </Text> </Text>
                        </View>
                    </View>
                    <View>
                        <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 15 }}>
                            <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                                <TouchableOpacity onPress={this.ClickOrderSummery.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                    <View></View>
                                    <Text style={{
                                        fontSize: 18,
                                        fontFamily: 'Helvetica',
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        fontWeight: 'bold'
                                    }}>ITEM SUMMARY</Text>
                                    <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                        {!this.state.OrderSummeryTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                            }} />)}

                                        {this.state.OrderSummeryTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                            }} />
                                        )}
                                    </View>

                                </TouchableOpacity>
                            </View>
                            {this.state.OrderSummeryTab && (<View>


                                {this.state.product.map((image, index) => this.render1(image, index))}
                                <View  style={{ margin: 5, flexDirection: 'column', justifyContent: "space-between" }}>
                                    <View style={{ flexDirection: 'row', padding: 2, paddingTop: 0, justifyContent: "space-between" }}>
                                        <Text style={{
                                            fontSize: 14,
                                            fontFamily: 'Helvetica', width: '70%',
                                            textAlign: 'left',
                                            paddingTop: 15,
                                            alignSelf: 'flex-start',
                                          
                                        }}>TOTAL G-POINTS EARNED </Text>
                                        <Text style={{
                                            fontSize: 14,
                                            fontFamily: 'Helveticaregular', width: '30%',
                                            textAlign: 'right',
                                            paddingTop: 15,
                                            alignSelf: 'flex-end',
                                        }}> <Icon name="g-points" size={18} style={{ fontWeight: 5, marginRight: 20 }} /> {this.state.BillingInfo.Gpoints}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 2, paddingTop: 0, justifyContent: "space-between" }}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontFamily: 'Helvetica', width: '55%',
                                            textAlign: 'left',
                                            paddingTop: 15,
                                            alignSelf: 'flex-start',
                                            fontWeight:'bold'
                                        }}>TOTAL </Text>
                                        <Text style={{
                                            fontSize: 16,
                                            fontFamily: 'Helveticaregular', width: '45%',
                                            textAlign: 'right',
                                            paddingTop: 15,
                                            alignSelf: 'flex-end',
                                            fontWeight:'bold'
                                        }}> {currencyFormatter.FormateCurrency(this.state.BillingInfo.NetAmount)}</Text>
                                    </View>
                                </View>
                            </View>
                            )}

                        </View>







                        <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 15 }}>
                            <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                                <TouchableOpacity onPress={this.ClickShipping.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                    <View></View>
                                    <Text style={{
                                        fontSize: 18,
                                        fontFamily: 'Helvetica',
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        fontWeight: 'bold'
                                    }}>SHIPPING</Text>
                                    <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                        {!this.state.shippingTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                            }} />)}

                                        {this.state.shippingTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                            }} />
                                        )}
                                    </View>

                                </TouchableOpacity>
                            </View>
                            {this.state.shippingTab && (<View>


                                {this.render2(this.state.DeliveryInfo)}

                            </View>)}

                        </View>








                        <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 15 }}>
                            <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                                <TouchableOpacity onPress={this.ClickPAYMENT.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                    <View></View>
                                    <Text style={{
                                        fontSize: 18,
                                        fontFamily: 'Helvetica',
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        fontWeight: 'bold'
                                    }}>PAYMENT</Text>
                                    <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                        {!this.state.PAYMENTTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                            }} />)}

                                        {this.state.PAYMENTTab && (
                                            <Icon name="left-open" size={22} style={{
                                                marginRight: 15, textAlign: 'center',
                                                alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                            }} />
                                        )}
                                    </View>

                                </TouchableOpacity>
                            </View>
                            {this.state.PAYMENTTab && (<View>


                                {this.render3(this.state.DeliveryInfo)}

                            </View>)}

                        </View>





                    </View>
                </View>

            </StickyHeaderFooterScrollView>
        )
    }

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});