import * as React from 'react';
import { View, Animated, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { Constants } from 'expo';
import Womens from "../GluxHome/Womens";
import Men from "../GluxHome/Men";


const windowhalfwidth = Dimensions.get('window').width / 2;
const FirstRoute = () => (
  // <View key="womenContainer" style={[styles.container, { backgroundColor: 'transparent' }]} />
  <Womens style={[styles.container, { backgroundColor: 'transparent' }]}></Womens>
);
const SecondRoute = () => (
  <Men style={[styles.container, { backgroundColor: 'transparent' }]}></Men>
);

export default class PageTab extends React.Component {


  state = {
    loading: true,
    index: 0,
    routes: [
      { key: 'WOMEN', title: 'WOMEN' },
      { key: 'MEN', title: 'MEN' },


    ],
  };



  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View key="SuperMainTabContaier">
        <View key="MainTabContaier" style={styles.tabBar}>
          {props.navigationState.routes.map((route, i) => {
            const color = props.position.interpolate({
              inputRange,
              outputRange: inputRange.map(
                inputIndex => (inputIndex === i ? '#f7f7f7' : '#a5a5a5')
              ),

            });
            return (
              <TouchableOpacity key={route.title}
                style={styles.tabItem}
                onPress={() => this.setState({ index: i })}>
                <Animated.Text style={{ color, fontFamily: 'Helveticaregular' }}>{route.title}</Animated.Text>
              </TouchableOpacity >

            );
          })}

        </View >
        <View key="MainSelectionContaier" style={{ flexDirection: 'row' }}>
          <View key="WomenSelectionContaier" style={this.state.index == 1 ? styles.selectionstyle2 : styles.selectionstyle1} >
          </View>
          <View key="MenSelectionContaier" style={this.state.index == 1 ? styles.selectionstyle1 : styles.selectionstyle2} >
          </View>
        </View>
      </View>
    );
  };

  _renderScene = SceneMap({
    WOMEN: FirstRoute,
    MEN: SecondRoute,

  });

  render() {

    return (
      <TabView style={{ overflow: "visible", height: 10000 }}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBar: {
    flexDirection: 'row',
    paddingTop: 2,
    backgroundColor: 'black',

  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10

  },
  selectionstyle1: {
    width: windowhalfwidth,
    borderBottomColor: 'gray',
    borderBottomWidth: 3
  },
  selectionstyle2: {

    width: windowhalfwidth,

    borderBottomColor: 'black',
    borderBottomWidth: 3
  }
});
