import React from "react";
import { TouchableHighlight, TouchableOpacity, Platform, ScrollView, Text, StyleSheet, View, Dimensions } from "react-native";
import GluxHome from "../GluxHome/Home";
import ProductDetails from "./ProductDeatails";
import SearchHome from "../GluxHome/search";
import commonsettings from '../../constants/commonsettings';
import TabProductDetails from './tabDetails';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Settings from "../../constants/Settings"
export default class ProductDeatailsMain extends React.Component {
    static navigationOptions = {
        header: null,
    };
    
    constructor(props) {

        super(props);
        this.state = {
            loading: false,
            producttab: false
        }
        this.componentDidMount = this.componentDidMount.bind(this);
        this.handler = this.handler.bind(this);
    }

    handler(item) {

        this.refs.SearchHome.bindmsg(item);
    }

    async componentWillMount() {
        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopallStack';

    }
    async componentDidMount(id) {
        if(id!=null && id != undefined)
        {
        this.refs.ProductDetails.binditem(id);
        this.refs.TabProductDetails.binditem(id);
        }
        // this.refs.ProductDetails.binditem(this.props.navigation.state.params.id);
        // this.refs.TabProductDetails.binditem(this.props.navigation.state.params.id);
    }

    render() {

        return (

            <View style={{ marginBottom: 40, backgroundColor: '#fff' }}>

            
                    <ProductDetails ref="ProductDetails"></ProductDetails>

                    <TabProductDetails ref="TabProductDetails" action1={this.handler}></TabProductDetails>
               


            </View >

        )
    }
}

