import Api from "../../utils/Api/AccountDetails";
import React, { Component } from 'react';
import { Alert, Modal, TouchableOpacity, Text, View, Button, Switch, StyleSheet, Dimensions, TextInput, ScrollView, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
const windowwidth = Dimensions.get('window').width;
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";

import DropdownAlert from 'react-native-dropdownalert';
import TimerMixin from 'react-timer-mixin';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';

const Icon = createIconSetFromFontello(fontelloConfig);

export default class Address extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Name: '',
            Address: '',
            Address2: '',
            Address3: '',
            PostalCode: '',
            PhoneNumber: '',
            PreferedShipping: true,
            PreferedBilling: true,
            NameState: false,
            Address1State: false,
            Address2State: false,
            PostalCodeState: false,
            PhoneNumberState: false,
            Tumbcolor2: "#808080",
            Tumbcolor: "#808080",
            progressVisible: false,
            dialogVisible: false
        }
    }
    Alert(item) {
       
        switch (item.type) {
            case 'close':
                this.forceClose();
                break;
            default:
                const random = Math.floor(Math.random() * 1000 + 1);
                const title = item.type + ' #' + random;
                this.dropdown.alertWithType(item.type, item.title, item.message);
        }
    }
    onPressLearnMore() {
        var savingstate = true

        if (this.state.Name == '' || this.state.Name == undefined || this.state.Name == null) {
            this.setState({ NameState: true })
            savingstate = false
        }
        if (this.state.Address == '' || this.state.Address == undefined || this.state.Address == null) {
            this.setState({ Address1State: true })
            savingstate = false
        }
        if (this.state.Address2 == '' || this.state.Address2 == undefined || this.state.Address2 == null) {
            this.setState({ Address2State: true })
            savingstate = false
        }
        if (this.state.PostalCode == '' || this.state.PostalCode == undefined || this.state.PostalCode == null) {
            this.setState({ PostalCodeState: true })
            savingstate = false
        }
        if (this.state.PhoneNumber == '' || this.state.PhoneNumber == undefined || this.state.PhoneNumber == null) {
            this.setState({ PhoneNumberState: true })
            savingstate = false
        }

        if (savingstate == true) {
            this.setState({ progressVisible: true })

            if (this.state.PreferedBilling == true) {

                Api.BillingAddress(this.state.Name,
                    this.state.Address,
                    this.state.Address2,
                    this.state.Address3,
                    'SINGAPORE',
                    this.state.PostalCode,
                    this.state.PhoneNumber)
                    .then((responseJson) => {
                        if (this.state.PreferedShipping !== true) {
                            this.setState({ dialogVisible: true })
                            this.setState({ progressVisible: false })
                            this.interval = setInterval(() => {

                                clearInterval(this.interval);
                                this.setState({ dialogVisible: false })

                            }, 1000);
                        }
                    })
                    .catch((error) => {
                        if (this.state.PreferedShipping !== true) {
                            this.setState({ dialogVisible: true })
                            this.setState({ progressVisible: false })
                            this.interval = setInterval(() => {

                                clearInterval(this.interval);
                                this.setState({ dialogVisible: false })

                            }, 1000);
                        }
                        console.log("EERRRR", error.message)



                    });
            }
            if (this.state.PreferedShipping == true) {
                savingOk = false
                Api.ShippingAddress(this.state.Name,
                    this.state.Address,
                    this.state.Address2,
                    this.state.Address3,
                    'SINGAPORE',
                    this.state.PostalCode,
                    this.state.PhoneNumber)
                    .then((responseJson) => {
                        this.setState({ dialogVisible: true })
                        // savingOk=true
                        this.interval = setInterval(() => {
                            this.setState({ progressVisible: false })
                            clearInterval(this.interval);
                            this.setState({ dialogVisible: false })

                        }, 1000);

                    })
                    .catch((error) => {
                        // savingOk=true
                        this.setState({ dialogVisible: true })
                        this.interval = setInterval(() => {
                            this.setState({ progressVisible: false })
                            clearInterval(this.interval);
                            this.setState({ dialogVisible: false })

                        }, 1000);
                        // this.bindmsg({ type: 'info', title: 'Stock', message: error.message });



                    });
            }
            if (savingOk == true) {

                this.setState({ progressVisible: false })
            }
        }








    }


    handleInputChangeName = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ NameState: true })
            this.setState({ Name: text.trim() })
        }
        else {
            this.setState({ NameState: false })
            this.setState({ Name: text })
        }


    }
    handleInputChangeAddress1 = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ Address1State: true })
            this.setState({ Address: text.trim() })
        }
        else {
            this.setState({ Address1State: false })
            this.setState({ Address: text })
        }


    }

    handleInputChangeAddress2 = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ Address2State: true })
            this.setState({ Address2: text.trim() })
        }
        else {
            this.setState({ Address2State: false })
            this.setState({ Address2: text })
        }


    }
    handleInputChangeAddress3 = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {

            this.setState({ Address3: text.trim() })
        }
        else {

            this.setState({ Address3: text })
        }


    }
    handleInputChangePostalCode = (text) => {

        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PostalCodeState: true })
            this.setState({ PostalCode: text.trim() })
        }
        else {
            this.setState({ PostalCodeState: false })
            this.setState({ PostalCode: text.trim() })
        }


    }

    handleInputChangePhoneNumber = (text) => {

        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PhoneNumberState: true })
            this.setState({ PhoneNumber: text.trim() })
        }
        else {
            this.setState({ PhoneNumberState: false })
            this.setState({ PhoneNumber: text.trim() })
        }


    }

    handleSwitchShippingAdress = (status) => {

        this.setState({ PreferedShipping: status })
        if (status == false) {
            this.setState({ PreferedBilling: true })
            this.setState({ Tumbcolor: "#bfbfc0" })
            this.setState({ Tumbcolor2: "#808080" })

        }
        else { this.setState({ Tumbcolor: "#808080" }) }


    }

    handleSwitchBillingAdress = (status) => {

        this.setState({ PreferedBilling: status })
        if (status == false) {
            this.setState({ PreferedShipping: true })
            this.setState({ Tumbcolor2: "#bfbfc0" })
            this.setState({ Tumbcolor: "#808080" })

        }
        else { this.setState({ Tumbcolor2: "#808080" }) }

    }



    render() {
        return (
            <View style={{ height: '100%', backgroundColor: '#fff' }}>
                {/* <ProgressDialog

                    message=""
                    visible={this.state.progressVisible}
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: 'transparent'
                        }
                    }
                /> */}
                {/* Loading Model */}
                <Modal onRequestClose={() => null} visible={this.state.progressVisible} transparent={true}>
                    <View style={{ flex: 1, backgroundColor: '#00000000', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ borderRadius: 10, backgroundColor: '#00000000', padding: 25 }}>

                            <ActivityIndicator size="large" color='gray' />
                        </View>
                    </View>
                </Modal>


                {/* Mesage Model */}
                <Dialog
                    visible={this.state.dialogVisible}

                    onTouchOutside={() => this.setState({ dialogVisible: false })} >
                    <View>
                        <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon name="check" size={18} style={{ fontWeight: 5 }} ></Icon> Address Updated </Text>
                    </View>
                </Dialog>

                <ScrollView>
                    <KeyboardAvoidingView

                        behavior="padding"
                    >


                        <View style={{ width: windowwidth - 12, paddingBottom: 10, margin: 6, borderWidth: 1, borderColor: 'gray', marginRight: 10 }}>

                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 15 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Name</Text>
                                {!!this.state.NameState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter full name.</Text>
                                )}
                                <TextInput
                                    placeholder="Full Name"
                                    onChangeText={this.handleInputChangeName}
                                    value={this.state.Name}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 2, marginRight: 2, marginBottom: 2 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Address</Text>
                                {!!this.state.Address1State && (
                                    <Text style={{ marginLeft: 6, color: 'red' }}>Please enter Address1</Text>
                                )}
                                <TextInput
                                    placeholder="Address1"
                                    onChangeText={this.handleInputChangeAddress1}
                                    value={this.state.Address}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />
                                {!!this.state.Address2State && (
                                    <Text style={{ marginLeft: 6, color: 'red' }}>Please enter Address2</Text>
                                )}
                                <TextInput
                                    placeholder="Address2"
                                    onChangeText={this.handleInputChangeAddress2}
                                    value={this.state.Address2}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                                <TextInput
                                    placeholder="Address3"
                                    onChangeText={this.handleInputChangeAddress3}
                                    value={this.state.Address3}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 2, marginRight: 2, marginBottom: 2 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#bfbfbf', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Country</Text>
                                <Text style={{ color: '#bfbfbf', fontSize: 17, marginLeft: 6, borderWidth: 1, paddingLeft: 5, marginRight: 6, paddingTop: 5, paddingBottom: 5, borderColor: '#bfbfbf' }}>SINGAPORE</Text>
                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 2, marginRight: 2, marginBottom: 2 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Postal Code</Text>
                                {!!this.state.PostalCodeState && (
                                    <Text style={{ marginLeft: 6, color: 'red' }}>Enter Postal code</Text>
                                )}
                                <TextInput
                                    placeholder="Postal Code"
                                    keyboardType='numeric'
                                    onChangeText={this.handleInputChangePostalCode}
                                    value={this.state.PostalCode}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                            </View>

                            <View style={{ flexDirection: 'column', marginLeft: 2, marginRight: 2, marginBottom: 2 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Phone Number</Text>
                                {!!this.state.PhoneNumberState && (
                                    <Text style={{ marginLeft: 6, color: 'red' }}>Enter Phone Number</Text>
                                )}
                                <TextInput
                                    placeholder="Phone Number"
                                    keyboardType='phone-pad'
                                    onChangeText={this.handleInputChangePhoneNumber}
                                    value={this.state.PhoneNumber}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 8, marginRight: 8, marginBottom: 2 }}>
                                <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#595959' }}>You will be contacted by phone if there is any issues with your order</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginLeft: 8, marginRight: 8, marginBottom: 2, marginTop: 10, justifyContent: 'space-between', alignContent: "space-between" }}>
                                <View style={{ flexDirection: 'row', width: '80%' }}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#595959', fontWeight: "bold" }}>Preffered as Shipping Address</Text>
                                </View>

                                <Switch value={this.state.PreferedShipping} onValueChange={this.handleSwitchShippingAdress} onTintColor="#e6e6e6"
                                    thumbTintColor={this.state.Tumbcolor}>
                                </Switch>

                            </View>
                            <View style={{ flexDirection: 'row', marginLeft: 8, marginRight: 8, marginBottom: 2, borderTopWidth: 1, borderColor: 'gray', justifyContent: 'space-between', alignContent: "space-between" }}>

                                <View style={{ flexDirection: 'row', width: '80%' }}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#595959', fontWeight: "bold" }}>Preffered as Billing Address</Text>
                                </View>

                                <Switch
                                    onValueChange={this.handleSwitchBillingAdress}
                                    value={this.state.PreferedBilling}
                                    onTintColor="#e6e6e6"
                                    thumbTintColor={this.state.Tumbcolor2}

                                >
                                </Switch>
                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 8, marginRight: 8, marginBottom: 2 }}>
                                <View style={styles.container}>
                                    <TouchableOpacity onPress={this.onPressLearnMore.bind(this)} style={styles.buttonContainer}>
                                        <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc' }}>SAVE ADDRESS</Text>

                                    </TouchableOpacity >
                                </View>
                            </View>


                        </View>

                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: '#39393a',
        borderRadius: 2,

        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1, borderColor: '#414142',
        width: '100%',
        paddingBottom: 10,
        paddingTop: 10,

        alignItems: "center"
    }, container: {
        backgroundColor: '#e2e2e2',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }

})