
import React from 'react';
import { Dimensions,TouchableOpacity, FlatList, ActivityIndicator, View, Image, StyleSheet, Platform, Text, Alert, YellowBox } from 'react-native';
import Api from "../../utils/Api/HomeApis"
import Settings from "../../constants/Settings"

const pictureheight = Dimensions.get('window').height/2;
export default class Womens extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      page: 7,
      loading: false,
      dataSource: []
    }
  }

  GetItem(name) {
    Settings.Designer_filter=name;
    Settings.nav.navigate("ShopallStack")
  }

  webCall = () => {
    Api.getTopWomenItems({
      results: 6,
      page: this.state.page
    }
    )
      .then((responseJson) => {
        this.setState(state => ({

          dataSource: [...this.state.dataSource, ...responseJson.results]
        }));
      })
      .catch((error) => {
        console.error(error);
      });

  }

  async componentWillMount() {

    this.webCall();
    this.setState({
      isLoading: false,
    });
  }

  handleEnd = () => {
 
    this.setState(state => ({ page: state.page + 1 }), () => this.webCall())
  }

  ProductDetails(item) {
    // alert(item)
    Settings.nav.navigate("ShopallStack")

  }

  render() {



    if (this.state.isLoading) {
      return (

        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

          <ActivityIndicator size="large" />

        </View>

      );

    }

    return (

      <View style={styles.MainContainer}>

                {/* <FlatList

                    data={this.state.dataSource}

                    onEndReachedThreshold={0} */}

                {/* renderItem={({ item }) => */}
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "LOUIS VUITTON")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="cover" source={{ uri: "http://glux.sg/assets/image/Hot%20Faves.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >HOT FAVES</Text>
                      
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            fontSize: 16,
                            paddingBottom:15,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "GUCCI")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="cover" source={{ uri:"http://glux.sg/assets/image/Gucci.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >GUCCI</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            paddingBottom:15,
                            fontSize: 16,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "PRADA")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="cover" source={{ uri:"http://glux.sg/assets/image/Prada.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >PRADA</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            paddingBottom:15,
                            fontSize:16,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "FENDI")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="stretch" resizeMethod="resize" source={{ uri:"http://glux.sg/assets/image/Fendi.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >FENDI</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            paddingBottom:15,
                            fontSize: 16,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "TORY BURCH")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="cover" source={{ uri:"http://glux.sg/assets/image/Tory%20Burch.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >TORY BURCH</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            paddingBottom:15,
                            fontSize: 16,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginBottom:20}} onPress={this.GetItem.bind(this, "LOVE MOSCHINO")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image resizeMode="cover" source={{ uri:"http://glux.sg/assets/image/Love%20Moschino.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 6,
                            fontSize: 22,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >LOVE MOSCHINO</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 0,
                            paddingBottom:8,
                            fontSize: 16,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
            </View >
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {

    justifyContent: 'center',
    flex: 1,
    flexGrow: 1,
    overflow: "visible",
    marginBottom: 25,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,

  },

  imageView: {

    width: '80%',
    height: pictureheight,
    marginRight: 7,
    alignItems: "center",
    margin: 7
    

  },

  textView: {

    width: '65%',
    textAlignVertical: 'center',
    padding: 10,
    color: '#000'

  }

});