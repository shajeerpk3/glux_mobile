import React from 'react';
import { Platform, Linking, KeyboardAvoidingView, Switch, TextInput, Dimensions, TouchableOpacity, ScrollView, View, StyleSheet, Text } from 'react-native';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import PhoneInput from 'react-native-phone-input'
import CheckBox from 'react-native-check-box';
import Api from "../../utils/Api/starter";
import { Dialog } from "react-native-simple-dialogs";
import Settings from '../../constants/Settings';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class RegisterUser extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            SwichValue: true,
            Tumbcolor: "#808080",
            AgreeTerms: false,
            NameState: false,
            Name: '',
            EmailState: false,
            Email: '',
            EmailMsg: '*Required',
            PhoneNumberState: false,
            PhoneNumber: '',
            PasswordState: false,
            Password: '',
            ConfirmPasswordState: false,
            ConfirmPassword: '',
            ConfirmPasswordMsg: '*Required',
            dialogVisible: false,
            dialogVisible2: false,
            ResultData: 'saved'

        };
        this.saving = this.saving.bind(this)

    }
    handleInputChangeName = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ NameState: true })
            this.setState({ Name: text.trim() })
        }
        else {
            this.setState({ NameState: false })
            this.setState({ Name: text })
        }
    }
    handleInputChangePassword = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PasswordState: true })
            this.setState({ Password: text.trim() })
        }
        else {
            this.setState({ PasswordState: false })
            this.setState({ Password: text })
        }
    }
    handleInputChangeConfirmPassword = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ ConfirmPasswordState: true })
            this.setState({ ConfirmPassword: text.trim() })
        }
        else {
            this.setState({ ConfirmPasswordState: false })
            this.setState({ ConfirmPassword: text })
        }
    }


    handleInputChangeEmail = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ EmailState: true })
            this.setState({ Email: text.trim() })
            this.setState({ EmailMsg: 'Email is requerd' })
        }
        else {
            this.setState({ EmailState: false })
            this.setState({ Email: text })
        }
    }

    handleInputChangePhoneNumber = (text) => {

        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PhoneNumberState: true })
            this.setState({ PhoneNumber: text.trim() })
        }
        else {
            this.setState({ PhoneNumberState: false })
            this.setState({ PhoneNumber: text.trim() })
        }

    }



    handleSwitch = (status) => {

        this.setState({ SwichValue: status })
        if (status == false) {
            this.setState({ PreferedShipping: true })
            this.setState({ Tumbcolor: "#bfbfc0" })


        }
        else { this.setState({ Tumbcolor: "#808080" }) }

    }


    saving() {
        Api.Regist(this.state.Name, "", this.state.Email, this.state.PhoneNumber,
            this.state.Password, this.state.AgreeTerms, this.state.ConfirmPassword, this.state.SwichValue
        )
            .then((responseJson) => {
                if (responseJson.message == "ok") {
                    this.setState({ ResultData: "Saving Success!, Please verify your email" })
                    this.setState({ dialogVisible: true })
                    this.interval = setInterval(() => {

                        clearInterval(this.interval);
                        this.setState({ dialogVisible: false })
                       this.starter();
                    }, 1000);

                }
                else {
                    this.setState({ ResultData: responseJson.message })
                    this.setState({ dialogVisible: true })
                    this.interval = setInterval(() => {

                        clearInterval(this.interval);
                        this.setState({ dialogVisible: false })

                    }, 1000);
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.setState({ ResultData: error.message })
                this.setState({ dialogVisible2: true })
                this.interval = setInterval(() => {

                    clearInterval(this.interval);
                    this.setState({ dialogVisible2: false })

                }, 1000);
            });
    }
    ClickLogin() {
        Settings.RegisterLogin = "LOGIN";
        Settings.nav.navigate("MainLogin")

    }

    starter() {


        Api.Starter(this.state.Email, this.state.Password)
            .then((responseJson) => {
               
                if (responseJson.status_code == "200") {
                    Settings.authorization = responseJson.token;
                    Settings.userid = responseJson.user_id;
                    Settings.userEmail = responseJson.email;
                    Settings.LoginStatus = true;
                    Settings.nav.navigate(Settings.backfromLogin)
                }
                else {
                    this.setState({ ResultData: responseJson.message })
                    this.setState({ dialogVisible2: true })

                    this.interval = setInterval(() => {

                        clearInterval(this.interval);

                        this.setState({ dialogVisible2: false })

                    }, 1000);
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.setState({ ResultData: error.message })
                this.setState({ dialogVisible2: true })
                this.interval = setInterval(() => {

                    clearInterval(this.interval);

                    this.setState({ dialogVisible2: false })

                }, 1000);
            });
    }


    ClickRegister() {
        var savingstate = true
        if (this.state.AgreeTerms != true) {
            this.setState({ ResultData: "please check privacy policy" })
            this.setState({ dialogVisible2: true })
            this.interval = setInterval(() => {

                clearInterval(this.interval);
                this.setState({ dialogVisible2: false })

            }, 1500);
            var savingstate = false

        }




        if (this.state.Name == '' || this.state.Name == undefined || this.state.Name == null) {
            this.setState({ NameState: true })
            savingstate = false
        }
        if (this.state.Password == '' || this.state.Password == undefined || this.state.Password == null) {
            this.setState({ PasswordState: true })
            savingstate = false
        }
        if (this.state.ConfirmPassword == '' || this.state.ConfirmPassword == undefined || this.state.ConfirmPassword == null) {
            this.setState({ ConfirmPasswordState: true })
            this.setState({ ConfirmPasswordMsg: "Requerd*" })
            savingstate = false
        }
        if (this.state.Email == '' || this.state.Email == undefined || this.state.Email == null) {
            this.setState({ EmailState: true })
            this.setState({ EmailMsg: "Requerd*" })
            savingstate = false
        }
        if (this.state.PhoneNumber == '' || this.state.PhoneNumber == undefined || this.state.PhoneNumber == null) {
            this.setState({ PhoneNumberState: true })
            savingstate = false
        }
        if (savingstate == true) {
            if (this.state.ConfirmPassword !== this.state.Password) {
                this.setState({ ConfirmPasswordState: true })
                this.setState({ ConfirmPasswordMsg: "Password not match" })
                savingstate = false
            }

            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(this.state.Email) === false) {
                this.setState({ EmailMsg: "Must be a valid email address" })
                this.setState({ EmailState: true })
                savingstate = false
            }
            if (savingstate == true) { this.saving() }
        }
    }




    render() {
        return (
            <View style={{ width: windowwidth, backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                <Dialog
                    visible={this.state.dialogVisible}

                    onTouchOutside={() => this.setState({ dialogVisible: false })} >
                    <View>
                        <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon name="check" size={18} style={{ fontWeight: 5 }} ></Icon>   {this.state.ResultData} </Text>
                    </View>
                </Dialog>
                <Dialog
                    visible={this.state.dialogVisible2}

                    onTouchOutside={() => this.setState({ dialogVisible: false })} >
                    <View>
                        <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                    </View>
                </Dialog>



                <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, paddingBottom: 18, width: '95%', justifyContent: 'center', alignItems: 'center' }} >
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 25, marginBottom: 25 }}>
                        <Icon name="glux-logo" size={70} style={{ fontWeight: 5 }} ></Icon>

                    </View>

                    <KeyboardAvoidingView

                        behavior="padding"
                    >

                        <View style={{ margin: 5 }} >
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}> <Icon2 name="Account" size={20} style={{ fontWeight: 5 }} ></Icon2></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>

                                    <TextInput
                                        placeholder="Full Name*"
                                        onChangeText={this.handleInputChangeName}
                                        value={this.state.Name}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.NameState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}> <Icon2 name="Email-address" size={15} style={{ fontWeight: 5 }} ></Icon2></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>
                                    <TextInput
                                        placeholder="Email Address*"
                                        onChangeText={this.handleInputChangeEmail}
                                        value={this.state.Email}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.EmailState && (<Text style={{ marginLeft: 6, color: 'red' }}>{this.state.EmailMsg}</Text>)}
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-between', margin: 2, marginTop: 15, width: '100%' }}>
                                <View style={{ flexDirection: 'row', width: '100%' }}>

                                    <Text style={{
                                        width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                            ios: {
                                                marginTop: 0
                                            },
                                            android: {
                                                marginTop: 3

                                            }

                                        })
                                    }}> <Icon2 name="Mobile-number" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>

                                    <PhoneInput
                                        ref={(ref) => { this.phone = ref; }}
                                        initialCountry={'sg'}
                                        style={{
                                            width: '30%', borderBottomWidth: 1, borderColor: 'gray'
                                        }}
                                    />


                                    <TextInput
                                        placeholder="Mobile Number*"
                                        keyboardType='phone-pad'
                                        onChangeText={this.handleInputChangePhoneNumber}
                                        value={this.state.PhoneNumber}
                                        style={{ width: '48%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, marginBottom: 0, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />

                                </View>
                                {!!this.state.PhoneNumberState && (<View style={{ flexDirection: 'row', width: '100%', marginTop: 6 }}>
                                    <Text style={{
                                        width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                            ios: {
                                                marginTop: 0
                                            },
                                            android: {
                                                marginTop: 3

                                            }

                                        })
                                    }}></Text>
                                    <View style={{ flexDirection: 'column', width: '78%', marginLeft: 6 }}><Text style={{ color: 'red' }}>*Required</Text></View>
                                </View>)}
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}> <Icon2 name="Password" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>
                                    <TextInput
                                        placeholder="Password*"
                                        secureTextEntry
                                        autoCorrect={false}
                                        onChangeText={this.handleInputChangePassword}
                                        value={this.state.Password}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.PasswordState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>
                                    <TextInput
                                        placeholder="Confirm Password*"
                                        secureTextEntry
                                        autoCorrect={false}
                                        onChangeText={this.handleInputChangeConfirmPassword}
                                        value={this.state.ConfirmPassword}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.ConfirmPasswordState && (<Text style={{ marginLeft: 6, color: 'red' }}>{this.state.ConfirmPasswordMsg}</Text>)}
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '91%', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'column', width: '85%' }}>
                                        <Text style={{ padding: 2, color: '#595959' }}>Let’s join us today for exclusive Sale access including the new arrivals & promotions </Text>
                                    </View>

                                    <Switch onTintColor="#e6e6e6"
                                        value={this.state.SwichValue}
                                        onValueChange={this.handleSwitch}
                                        thumbTintColor={this.state.Tumbcolor}>
                                    </Switch>

                                </View>

                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 10, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '91%', justifyContent: 'space-between', alignItems: 'center' }}>

                                    <CheckBox

                                        onClick={() => {
                                            this.setState({
                                                AgreeTerms: !this.state.AgreeTerms
                                            })

                                        }}
                                        Animated={true}
                                        isChecked={this.state.AgreeTerms}
                                        checkBoxColor={'gray'}
                                    />
                                    <View style={{ flexDirection: 'column', width: '85%' }}>
                                        <Text style={{ padding: 2, color: '#595959' }}>I have read and understood the Gstock Account <Text onPress={() => Linking.openURL('http://google.com')} style={{ color: '#d87136', textDecorationLine: "underline" }}>Terms and Conditions</Text> & <Text onPress={() => Linking.openURL('http://google.com')} style={{ color: '#d87136', textDecorationLine: "underline" }}>Privacy Policy</Text> </Text>
                                    </View>



                                </View>

                            </View>




                        </View>
                    </KeyboardAvoidingView>
                    <View style={{ margin: 2, marginTop: 15, width: '95%', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={styles.containerRegister}>
                            <TouchableOpacity onPress={this.ClickRegister.bind(this)} style={styles.buttonContainerRegister}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>REGISTER</Text>

                            </TouchableOpacity >
                        </View>


                        <View style={styles.containerSignin}>
                            <TouchableOpacity onPress={this.ClickLogin} style={styles.buttonContainerSignin}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>SIGN IN</Text>

                            </TouchableOpacity >
                        </View>

                    </View>

                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({

    buttonContainerRegister: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#d6d7d8',
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    }, containerSignin: {
        marginTop: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});