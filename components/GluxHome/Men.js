
import React from 'react';
import { FlatList, ActivityIndicator, View, Image, StyleSheet, Platform, Text, YellowBox } from 'react-native';
import Api from "../../utils/Api/HomeApis"

export default class Men extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            isLoading: true,
            page: 7,
            loading: false,
            dataSource: []
        }

        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

    }

    GetItem(flower_name) {
        Settings.Designer_filter=flower_name;
        this.setState(state => ({ page: state.page + 1 }), () => this.webCall())

    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: .5,
                    width: "100%",
                    backgroundColor: "#000",
                }}
            />
        );
    }

    webCall = () => {

        Api.getTopMenItems({
            results: 100,

            page: this.state.page
        }

        )
            .then((responseJson) => {
                this.setState(state => ({

                    dataSource: [...this.state.dataSource, ...responseJson.results]
                }));
            })
            .catch((error) => {
                console.error(error);
            });

    }

    async componentWillMount() {
        this.webCall();
        this.setState({
            isLoading: false,
        });
    }
    handleEnd = () => {

        this.setState(state => ({ page: state.page + 1 }), () => this.webCall())
    }



    render() {



        if (this.state.isLoading) {
            return (

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                    <ActivityIndicator size="large" />

                </View>

            );

        }

        return (

            <View style={styles.MainContainer}>

                {/* <FlatList

                    data={this.state.dataSource}

                    onEndReachedThreshold={0} */}

                {/* renderItem={({ item }) => */}
                <TouchableOpacity onPress={this.GetItem.bind(this, "HOT FAVES")}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image source={{ uri: "http://glux.sg/assets/image/Hot%20Faves.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 3,
                            fontSize: 12,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >HOT FAVES</Text>
                      
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            fontSize: 10,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.GetItem.bind(this, item.login.uuid)}>

                    < View style={{
                        flex: 1, alignItems: "center", flexGrow: 1,
                        overflow: "visible"
                    }}

                    >

                        <Image source={{ uri:"http://glux.sg/assets/image/Gucci.jpg" }} style={styles.imageView} />

                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 3,
                            fontSize: 12,
                            fontFamily: 'lucidabrightdemiregular'
                        }} >GUCCI</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            fontSize: 10,
                            fontFamily: 'Helvetica',
                            textDecorationLine: "underline",
                            fontWeight: 'bold'
                        }} >SHOP NOW</Text>

                    </View>
                </TouchableOpacity>
            </View >
        );
    }
}

const styles = StyleSheet.create({

    MainContainer: {

        justifyContent: 'center',
        flex: 1,
        flexGrow: 1,
        overflow: "visible",
        marginBottom: 50,
        marginTop: (Platform.OS === 'ios') ? 20 : 0,

    },

    imageView: {

        width: '80%',
        height: 350,
        marginRight: 7,
        alignItems: "center",
        margin: 7

    },

    textView: {

        width: '65%',
        textAlignVertical: 'center',
        padding: 10,
        color: '#000'

    }

});