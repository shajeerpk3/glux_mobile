import React from "react";
import settingss from '../../constants/Settings';
const servername = settingss.serverUrl1;
const endUrl = {
    PaypalSuccess: servername + "/order/success"
}



function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
      
    } catch (e) {
        console.log("not jason")
        bodyIsJson = false;
       
    }

  
    let fullUrl: string = endpoint + constructParams(params);
     console.log("fullUrl2313", fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
   
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}


function PaypalSuccess(order,array) {
    console.log("order",order)
    const body = JSON.stringify({
        order_id: order    
    });
    return callApi(endUrl.PaypalSuccess , "POST", body, null, settingss.authorization)

}

export default {
    PaypalSuccess
   
};