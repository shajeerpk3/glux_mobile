import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
//  import HomeScreen from '../components/ProductDetails.';
import HomeScreen from '../components/GluxHome/main';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AddressScreen from '../components/AccountDetails/Address'
import Designer from '../components/Designer/Designer'
import shopall from '../components/shopall/MainShopall';
import { createIconSetFromFontello,createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../src/config.json';
import icoMoonConfig from '../src/selection.json';
import Account from '../components/AccountDetails/Account'
import AccountDetails from '../utils/Api/AccountDetails';
import { colors } from 'react-native-elements';
const  loading= true ;
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'HOME',
  tabBarIcon: ({ focused }) => (
    <Icon2 name="Glux-icon-1" size={25}  />

  ),
  tabBarOptions: {
  // hide labels
     activeTintColor: '#000000', // active icon color
     labelStyle: {bottom:5,
      marginBottom:5},
    style: {
      borderTopWidth: 5,// TabBar background
      borderTopColor:'#000000',
      height: 65,
      
    }
},
};

const LinksStack = createStackNavigator({
  Links: Designer,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'DESIGNERS',
  tabBarIcon: ({ focused }) => (
    <Icon name="designer" size={25}  />
  ),
  tabBarOptions: {
    // hide labels
       activeTintColor: '#000000', // active icon color
     
      // inactiveTintColor: '#586589',  // inactive icon color
      labelStyle: {bottom:5,
        marginBottom:5},
      style: {
        borderTopWidth: 5,// TabBar background
        borderTopColor:'#000000',
        height: 65,
       
      }
  },
};

const ShopallStack = createStackNavigator({
  Shopall: shopall,
});

ShopallStack.navigationOptions = {
  tabBarLabel: 'SHOP ALL',
  tabBarIcon: ({ focused }) => (
    <Icon name="shopall" size={25} />
  ),
  tabBarOptions: {
    // hide labels
       activeTintColor: '#000000', // active icon color
     
      // inactiveTintColor: '#586589',  // inactive icon color
      labelStyle: {bottom:5,
        marginBottom:5},
      style: {
        borderTopWidth: 5,// TabBar background
        borderTopColor:'#000000',
        height: 65
      }
  },
};


const WishStack = createStackNavigator({
  Wish: shopall,
});

 WishStack.navigationOptions = {
  tabBarLabel: 'WISHLIST',
  tabBarIcon: ({ focused }) => (
    <Icon name="wishlist" size={30}  />
  ),
  tabBarOptions: {
    // hide labels
       activeTintColor: '#000000', // active icon color
     
      // inactiveTintColor: '#586589',  // inactive icon color
      labelStyle: {bottom:5,
        marginBottom:5},
      style: {
        borderTopWidth: 5,// TabBar background
        borderTopColor:'#000000',
        height: 65
      }
  },
};


const AccountsStack = createStackNavigator({
  Account: Account,
});

AccountsStack.navigationOptions = {
  tabBarLabel: 'ACCOUNTS',
  tabBarIcon: ({ focused }) => (
    <Icon name="account" size={25}  />
  ),
  tabBarOptions: {
    // hide labels
       activeTintColor: '#000000', // active icon color
     
      // inactiveTintColor: '#586589',  // inactive icon color
      labelStyle: {bottom:5,
        marginBottom:5},
      style: {
        borderTopWidth: 5,// TabBar background
        borderTopColor:'#000000',
        height: 65
      }
      
  },
};


export default createBottomTabNavigator({
  HomeStack,
  LinksStack,
  ShopallStack,
 
  AccountsStack
});
