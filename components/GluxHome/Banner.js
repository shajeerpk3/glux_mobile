
import React from "react";
import { ImageBackground, StyleSheet, View, Image, Dimensions } from "react-native";
import Carousel from 'react-native-banner-carousel';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = Dimensions.get('window').height / 3;;

const images = [
    "https://glux.sg/assets/image/Launching.jpg",    
];



export default class Banner extends React.Component {
    renderPage(image, index) {
        var aaa=Math.random()
        console.log(aaa)
        return (
            <View key={index}>
                <ImageBackground resizeMode="stretch" source={{ uri: image + "?aa=" + aaa }} style={{ height: BannerHeight, width:BannerWidth }}>
                  
                </ImageBackground>
            </View >
        );
    }

    render() {

        return (
            <View style={styles.container}>

                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={1}
                    pageSize={BannerWidth}
                    removeClippedSubviews={false}
                    enableMomentum
                    initialNumToRender={1}
                    activePageIndicatorStyle={styles.activePageIndicatorStyle}
                    pageIndicatorStyle={styles.pageIndicatorStyle}
                >
                    {images.map((image, index) => this.renderPage(image, index))}
                </Carousel>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center'
    },
    activePageIndicatorStyle: {
        backgroundColor: 'black',

    },
    pageIndicatorStyle: {
        backgroundColor: 'gray',

    }


});