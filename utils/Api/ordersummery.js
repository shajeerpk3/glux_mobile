import settingss from '../../constants/Settings';

const servername = settingss.serverUrl2;
const endUrl = {
    getordersummery: servername + "/buyer/order",
    // getordersummery: servername + "/admin/order",
  
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
    } catch (e) {
        bodyIsJson = false;
    }

    const fullUrl = endpoint + constructParams(params);
    console.log("full monnnnnn",fullUrl)
    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...contentTypeHeader,
        },
        body: body,
    };

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {


            if (!response.ok) {
                return Promise.reject(json);
            }
           
            return json;
        });

}

function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}



function getordersummery(userid) {
   console.log("useridApi",userid)
    return callApi(endUrl.getordersummery + "?user_id=" +userid, "GET", null, null);
   
}


export default {
    getordersummery
};