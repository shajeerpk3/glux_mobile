import React from "react";
import { FlatList, StyleSheet, View, Image, Dimensions, TouchableOpacity, Text } from "react-native";
import Carousel from 'react-native-banner-carousel';
import Api from "../../utils/Api/HomeApis";
import Settings from "../../constants/Settings"

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = Dimensions.get('window').height;




export default class Spotlight extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            page: 4,
            loading: false,
            // dataSource: [],
            // images: []
        }
    }
    async componentWillMount() {
        // this.webCall();
        this.setState({
            isLoading: false,
        });
    }


    webCall = () => {
        // Api.SpotlightHome({
        //     results: 4,         
        //     page: this.state.page
        // }
        // ).then((responseJson) => {
        //     this.setState(state => ({

        //         dataSource: [...this.state.dataSource, ...responseJson.results]
        //     }));
        // }).catch((error) => {
        //     console.error(error);
        // });
    }



    GetItem(name) {
        Settings.Designer_filter = name;
        Settings.nav.navigate("ShopallStack")

    }

    renderPage(images, index, Name, Smalltext) {

        return (
            <View key={index} style={{ alignItems: "center", flex: 1 }}>
                <TouchableOpacity onPress={this.GetItem.bind(this, Name)} style={{
                    width: '100%',
                    height: '100%',
                    flex: 1,
                    alignItems: "center",
                }}>
                    <Image resizeMode="cover" style={{
                        width: '90%',
                        height: (BannerHeight / 2),
                        alignItems: "center",

                    }} source={{ uri: images }} />

                    {/* <TouchableOpacity onPress={this.onPressLearnMore}> */}
                    <Text style={{
                        fontSize: 22,
                        fontFamily: 'lucidabrightdemiregular', width: '80%',
                        textAlign: 'center',
                        paddingTop: 13,
                        alignSelf: 'center', justifyContent: 'center', alignItems: 'center'
                    }}>{Name}</Text>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helveticaregular', width: '80%',
                        textAlign: 'center',
                        paddingTop: 1,
                        alignSelf: 'center', justifyContent: 'center', alignItems: 'center'
                    }}>{Smalltext}</Text>
                    <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        paddingTop: 8,
                        fontSize: 16,
                        fontFamily: 'Helvetica',
                        textDecorationLine: "underline",
                        fontWeight: 'bold'
                    }} >SHOP BRAND</Text>
                </TouchableOpacity >
            </View>
        );
    }
    renderPage1(images, index, Name, Smalltext) {

        return (
            <View key={index} style={{ alignItems: "center", flex: 1 }}>
                <TouchableOpacity onPress={this.GetItem.bind(this, "")} style={{
                    width: '100%',
                    height: '100%',
                    flex: 1,
                    alignItems: "center",
                }}>
                    <Image resizeMode="stretch" resizeMethod="resize" style={{
                        width: '90%',
                        height: (BannerHeight / 2),
                        alignItems: "center",

                    }} source={{ uri: images }} />

                    {/* <TouchableOpacity onPress={this.onPressLearnMore}> */}
                    <Text style={{
                        fontSize: 22,
                        fontFamily: 'lucidabrightdemiregular', width: '80%',
                        textAlign: 'center',
                        paddingTop: 16,

                        alignSelf: 'center', justifyContent: 'center', alignItems: 'center'
                    }}>{Name}</Text>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helveticaregular', width: '80%',
                        textAlign: 'center',
                        paddingTop: 1,
                        alignSelf: 'center', justifyContent: 'center', alignItems: 'center'
                    }}>{Smalltext}</Text>
                    {/* <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        paddingTop: 1,
                        fontSize: 16,
                        fontFamily: 'Helvetica',
                        textDecorationLine: "underline",
                        fontWeight: 'bold'
                    }} >SHOP BRAND</Text> */}
                </TouchableOpacity >
            </View>
        );
    }
    render() {

        return (
            <View style={styles.container1}>
                <Image
                    resizeMode="contain"
                    source={require('../../assets/images/Spotlight.png')}
                    style={styles.SpotlightImg}
                />
                <View style={styles.container}>


                    <Carousel
                        autoplay
                        autoplayTimeout={5000}
                        loop
                        index={1}
                        pageSize={BannerWidth}
                        activePageIndicatorStyle={styles.activePageIndicatorStyle}
                        pageIndicatorStyle={styles.pageIndicatorStyle}
                    >
                        {this.renderPage1("http://glux.sg/assets/image/Institut%20Karite_banner1.jpg", 1, "INSTITUT KARITE PARIS (COMING SOON)", "Recharge, perfect and protect your skin with natural and high quality formula.")}
                        {this.renderPage("http://glux.sg/assets/image/LV.jpg", 2, "LOUIS VUITTON", "Find your favourite and latest arrival of Louis Vuitton")}
                        {this.renderPage1("http://glux.sg/assets/image/Stay%20Connected.jpg", 3, "STAY CONNECTED", "You can connect with us on WhatsApp at 8514 4978 to receive any exclusive member events and more!")}
                        {/* {this.state.dataSource.map((image, index) => this.renderPage(image.picture.large, image.login.uuid, image.name.first.toUpperCase(), image.location.state))} */}
                    </Carousel>


                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container1: {
        alignItems: "center",
        height: (BannerHeight / 2) + 295,
        backgroundColor: '#eff0f2',
        marginBottom: 50,
        paddingBottom:15
    },
    container: {
        justifyContent: 'center',
        flex: 1,
        flexGrow: 1,
        overflow: "visible",
        marginTop: 25
    },
    SpotlightImg: {
        marginTop: 35,
        height: '10%',
        width: '45%',

    },
    activePageIndicatorStyle: {
        backgroundColor: 'black',

    },
    pageIndicatorStyle: {
        backgroundColor: 'gray',

    }



});