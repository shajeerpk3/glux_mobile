import React from 'react';
import { ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text, Alert, YellowBox } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Carousel from 'react-native-banner-carousel';
import TabProductDetails from './tabDetails';
import ModalDropdown from 'react-native-modal-dropdown';
import Settings from '../../constants/Settings'

const Icon = createIconSetFromFontello(fontelloConfig);
const BannerWidth = Dimensions.get('window').width;

var productid = ''
var variantss = ''
var discountstring = ''

export default class ProductDetails extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            product_id: productid,
            isLoading: true,
            images: [],
            dataSource: [],
            discountstringBuilder: ''
        };

        this.webcall = this.webcall.bind(this);
    }


    binditem(id) {
        Api.Itemdetails({
            product_id: id
        }
        )
            .then((responseJson) => {
                this.webcall(responseJson);
               
                if (responseJson.product.pricing.discount_rate != 0) {
                    discountstring = 'SALE ' + responseJson.product.pricing.discount_rate + '% OFF';
                }

                if (responseJson.product.pricing.discount_rate1 != 0 && responseJson.product.pricing.discount_rate == 0) {

                    discountstring =  responseJson.product.pricing.discount_rate1 + '% OFF' ;
                }
                if (responseJson.product.pricing.discount_rate1 != 0 && responseJson.product.pricing.discount_rate != 0) {

                    discountstring = discountstring + ' +  ' + responseJson.product.pricing.discount_rate1 + '% OFF';
                }
                if ((responseJson.product.pricing.discount_rate1 != 0 || responseJson.product.pricing.discount_rate != 0) && responseJson.product.pricing.discount_rate2 != 0) {

                    discountstring = discountstring + ' + ' + responseJson.product.pricing.discount_rate2 + '% OFF';
                }
                if ((responseJson.product.pricing.discount_rate1 == 0 && responseJson.product.pricing.discount_rate == 0) && responseJson.product.pricing.discount_rate2 != 0) {

                    discountstring = responseJson.product.pricing.discount_rate2 + '% OFF';
                }
                this.setState({ discountstringBuilder: discountstring })



            })
            .catch((error) => {
                console.error(error);
            });


        // this.webcall('5b3eca64d5049d000ff1feb8');
    }

    _dropdown_4_onSelect(idx, value) {
        console.log(idx, value);
    }

    webcall(id) {

        this.setState(state => ({
            dataSource: id
        }));
        this.variantss = this.transformVariants(id.variants);
        this.setState({ isLoading: false });
        console.log("id.product.GLux_pnts",id.product.GLux_pnts);
        Settings.GpointOfProductDetails=id.product.GLux_pnts;
                

    }





    addtowishlist() {
        console.log("hello1");
    }


    share() {
        console.log("hello2");
    }

    transformVariants(variants, index = 0) {
        //alhamdu lillah
        if (!variants.length) { return; }
        const options = variants[0]['option_name'].split('/').map((o) => {
            return {
                name: o,
                value: ['Please select']
            };
        });

        for (let i = 0; i < options.length; i++) {
            variants.forEach(e => options[index].value.push(e['option_value'].split('/')[index])),

                options[index].value = Array.from(new Set(options[index].value)),
                index = index + 1
        }

        return options;
    }
    bindTabdetails() {
        // console.log("log");
        //   {this.props.actionMon(productid)}
    }

    renderVariants(imag, index) {
        return (
            <View key={index} style={{ borderColor: "gray", borderWidth: 1, height: 30, flexDirection: "row", marginLeft: 10, marginTop: 5, marginRight: 10 }}>
                <View style={{ flex: 3 }}>
                    <ModalDropdown dropdownStyle={{ width: '100%' }} textStyle={{ color: "#c6c6c6", paddingTop: 3.0, fontSize: 13, padding: 2, fontFamily: "Helvetica" }} animated={true} defaultValue={imag.name} style={{ width: "100%", height: "100%" }} options={imag.value} onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}>

                    </ModalDropdown>
                </View>
                <View style={{ borderLeftWidth: 1, borderColor: "gray", width: 20 }}>
                    <Icon name="left-open" size={12} style={{ transform: [{ rotate: '270deg' }] }} />
                </View>
            </View>
        )
    }


    renderPage(images, index) {


        return (
            <View key={index} style={{ alignItems: "center" }}>

                <Image resizeMode="cover" style={{
                    width: 240,
                    height: 300,
                    alignItems: "center",
                    borderRadius: 5,
                }} source={{ uri: images }} />

            </View>
        );
    }



    render() {

        if (this.state.isLoading) {
            return (

                <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 100 }}>

                    <ActivityIndicator size="large" color="gray" />

                </View>


            );

        }

        return (

            <View style={styles.MainContainer}>
                {/* {console.log("hayoooooo",this.state.dataSource.product)} */}
                <View style={{ flexDirection: "row", marginTop: 10 }} >
                    <View style={{ flex: 2 }}></View>
                    {/* <TouchableOpacity onPress={this.addtowishlist.bind(this)}><Icon name="wishlist" size={20} style={{ fontWeight: 5, marginRight: 11 }} /></TouchableOpacity>
                    <TouchableOpacity onPress={this.share.bind(this)}><Icon name="share-1" size={20} style={{ fontWeight: 5, marginRight: 20 }} /></TouchableOpacity> */}
                </View>
                <View style={styles.container1}>

                    <View style={styles.container}>


                        <Carousel
                            autoplay
                            autoplayTimeout={5000}
                            loop
                            index={0}
                            pageSize={BannerWidth}
                        >
                            {/* {this.state.dataSource.product.brief.images.map((ima, index) => console.log("https://gstock.sg/buyer.gstock/uploads/user/products/" + this.state.dataSource.product._id + "/" + ima, index))} */}
                            {this.state.dataSource.product.brief.images.map((ima, index) => this.renderPage("http://buyerservices.glux.sg/uploads/user/undefined/products/" + this.state.dataSource.product._id + "/" + ima, index))}
                            {/* {this.state.dataSource.product.brief.images.map((ima, index) => this.renderPage("https://gstock.sg/buyer.gstock/uploads/user/" + this.state.dataSource.product.merchant_id._id + "/products/" + this.state.dataSource.product._id + "/" + ima, index))} */}

                        </Carousel>


                    </View>
                </View>
                <View style={{


                    backgroundColor: '#fff',
                    padding: 5,
                    justifyContent: "center",
                    alignItems: "center",


                }}>
                    <Text style={{

                        fontSize: 20,
                        fontFamily: 'lucidabrightdemiregular', width: '100%',
                        textAlign: 'center',
                        alignSelf: 'center',



                    }}>
                        {this.state.dataSource.product.detail.product_brand.toUpperCase()}
                    </Text>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helveticaregular', width: '100%',
                        textAlign: 'center',

                        alignSelf: 'center',
                    }}> {this.state.dataSource.product.brief.name}</Text>
                    <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        paddingTop: 14,
                        fontSize: 25,
                        fontFamily: 10,
                        fontFamily: 'Helveticaregular',

                    }} >{currencyFormatter.FormateCurrency(this.state.dataSource.product.pricing.discount_price2)}</Text>
                    {currencyFormatter.FormateCurrency(this.state.dataSource.product.pricing.discount_price2)
                        != currencyFormatter.FormateCurrency(this.state.dataSource.product.brief.price) && (
                            <View style={{ flexDirection: "column" }}>
                                <Text style={{

                                    textAlign: 'center',
                                    paddingTop: 1, fontSize: 20,
                                    fontFamily: 'Helvetica',
                                    color: '#c6c6c6',
                                    textDecorationLine: "line-through",
                                    fontWeight: 'bold'
                                }} >{currencyFormatter.FormateCurrency(this.state.dataSource.product.brief.price)}</Text>

                                <View style={{ flexDirection: "row" }}>


                                    <Text style={{
                                        textDecorationLine: "none",
                                        color: '#d3586a',
                                        textAlign: 'center',
                                        fontSize: 17
                                    }}>  {this.state.discountstringBuilder} </Text>
                                </View>
                            </View>
                        )}
                    <Text style={{
                        fontSize: 16,
                        fontFamily: 'Helveticaregular', width: '100%',
                        textAlign: 'center',

                        alignSelf: 'center',
                    }}> EARN <Icon name="g-points" size={18} style={{ fontWeight: 5, marginRight: 20 }} /> {this.state.dataSource.product.GLux_pnts} points</Text>
                </View>
                <View style={{ marginTop: 5, alignContent: "center", marginBottom: 5 }}>

                    {this.variantss && (this.variantss.map((ima, index) => this.renderVariants(ima, index)))}

                </View>
                {this.bindTabdetails()}
                {/* {console.log("my name ishhhhdhfhfhfh")} */}
            </View>

        )

    }
}


const styles = StyleSheet.create({

    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 6,

        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',

        width: '100%',
        margin: 10,
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 20

    },


    MainContainer: {

        backgroundColor: '#fff',



    },
    container1: {
        marginTop: 16,
        alignItems: "center",
        height: 300,
        backgroundColor: '#fff',
        marginBottom: 10
    },
    container: {
        justifyContent: 'center',
        flex: 1,
        flexGrow: 1,
        overflow: "visible",
    },
    containerloading: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }


});

