import Api from "../../utils/Api/AccountDetails";
import Api2 from "../../utils/Api/accountUpdates";
import React, { Component } from 'react';
import { Alert, Modal, TouchableOpacity, Text, View, Button, Switch, StyleSheet, Dimensions, TextInput, ScrollView, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
const windowwidth = Dimensions.get('window').width;
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import RNPickerSelect from 'react-native-picker-select';
import DropdownAlert from 'react-native-dropdownalert';
import TimerMixin from 'react-timer-mixin';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from '../../constants/Settings';
import PhoneInput from 'react-native-phone-input';
import DatePicker from 'react-native-datepicker';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';


const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
var radio_props = [
    { label: 'Male  ', value: 'M' },
    { label: 'Female', value: 'F' }
];


export default class UpdateAccountSub extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Name: '',
            Email: '',
            PhoneNumber: '',
            gender: 'M',
            genderSelection:0,
            dob: '',
            Country: 'Singapore',
            Language: 'English',
            PreferedShipping: true,
            PreferedBilling: true,
            NameState: false,
            EmailState: false,
            CountryState: false,
            dobState: false,
            LanguageState: false,
            PhoneNumberState: false,
            Tumbcolor2: "#808080",
            Tumbcolor: "#808080",
            progressVisible: false,
            dialogVisible: false,
            visible:false
        },
        this.onPressLearnMore=this.onPressLearnMore.bind(this);
    }



    onPressLearnMore() {
        var savingstate = true

        if (this.state.Name == '' || this.state.Name == undefined || this.state.Name == null) {
            this.setState({ NameState: true })
            savingstate = false
        }
        if (this.state.Country == '' || this.state.Country == undefined || this.state.Country == null) {
            this.setState({ CountryState: true })
            savingstate = false
        }
        if (this.state.Language == '' || this.state.Language == undefined || this.state.Language == null) {
            this.setState({ LanguageState: true })
            savingstate = false
        }
        if (this.state.PhoneNumber == '' || this.state.PhoneNumber == undefined || this.state.PhoneNumber == null) {
            this.setState({ PhoneNumberState: true })
            savingstate = false
        }
        if (this.state.dob == '' || this.state.dob == undefined || this.state.dob == null) {
            this.setState({ dobState: true })
            savingstate = false
        }

        if (this.state.Email == '' || this.state.Email == undefined || this.state.Email == null) {
            this.setState({ EmailState: true })
            this.setState({ EmailMsg: "Requerd*" })
            savingstate = false
        }
        if (savingstate == true) {

            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(this.state.Email) === false) {
                this.setState({ EmailMsg: "Must be a valid email address" })
                this.setState({ EmailState: true })
                savingstate = false
            }

        }


        if (savingstate == true) {
            // this.setState({ progressVisible: true })

            if (this.state.PreferedBilling == true) {

                Api2.accountdetailsUpdate(this.state.Name, "",
                    this.state.Email,
                    this.state.Language,
                    this.state.Country,
                    this.state.gender,
                    this.state.PhoneNumber,
                    this.state.dob,
                    Settings.userid
                )
                    .then((responseJson) => {
                        this.setState({ progressVisible: false })
                        alert("Account updated");
                        // this.setState({ dialogVisible: true });
                        // this.interval = setInterval(() => {
                        //     this.setState({ progressVisible: false })
                        //     clearInterval(this.interval);
                        //     this.setState({ dialogVisible: false })

                        // }, 1000);

                    })
                    .catch((error) => {

                        console.log("EERRRR", error.message)



                    });
            }

            // if (savingOk == true) {

            //     this.setState({ progressVisible: false })
            // }
        }
    }



    handleInputChangeEmail = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ EmailState: true })
            this.setState({ Email: text.trim() })
        }
        else {
            this.setState({ EmailState: false })
            this.setState({ Email: text })
        }
    }



    handleInputChangeName = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ NameState: true })
            this.setState({ Name: text.trim() })
        }
        else {
            this.setState({ NameState: false })
            this.setState({ Name: text })
        }


    }

    async componentDidMount() {
       
        Api2.accountdetails(Settings.userid)
            .then((responseJson) => {
                console.log("allah", responseJson.docs[0])
                this.setState({

                    Name: responseJson.docs[0].profile.first_name
                });
                this.setState({

                    dob: responseJson.docs[0].profile.DOB
                });
                this.setState({

                    Email: responseJson.docs[0].credential.email
                });
                this.setState({

                    PhoneNumber: responseJson.docs[0].contact.mobile_no
                });
                this.setState({

                    Country: responseJson.docs[0].location.country
                });
                this.setState({

                    Language: responseJson.docs[0].location.Language
                });

                this.setState({
                    loading: false
                });
                if(responseJson.docs[0].profile.gender =="F" )
                {
                    this.setState({

                        genderSelection: 1
                    });
                }
                else
                {
                    this.setState({

                        genderSelection: 0
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });

    }


    handleInputChangePhoneNumber = (text) => {

        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PhoneNumberState: true })
            this.setState({ PhoneNumber: text.trim() })
        }
        else {
            this.setState({ PhoneNumberState: false })
            this.setState({ PhoneNumber: text.trim() })
        }

    }
    clickfulldtailsbind() {

        Settings.nav.navigate("AccountDetails")
    }
    Showpassword(columnnow) {

        console.log("columnnow", columnnow)
    }
    render() {
        return (
            <View style={{ height: '100%', backgroundColor: '#fff' }}>
                <Modal onRequestClose={() => null} visible={this.state.progressVisible} transparent={true}>
                    <View style={{ flex: 1, backgroundColor: '#00000000', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ borderRadius: 10, backgroundColor: '#00000000', padding: 25 }}>

                            <ActivityIndicator size="large" color='gray' />
                        </View>
                    </View>
                </Modal>
                <Dialog
                    visible={this.state.dialogVisible}

                    onTouchOutside={() => this.setState({ dialogVisible: false })} >
                    <View>
                        <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon name="check" size={18} style={{ fontWeight: 5 }} ></Icon> Updated </Text>
                    </View>
                </Dialog>


                <ScrollView>
                    <KeyboardAvoidingView

                        behavior="padding"
                    >
                        <View style={{ width: windowwidth - 33, paddingBottom: 10, margin: 6, borderWidth: 1, borderColor: 'gray', flexDirection: 'row', marginRight: 10, justifyContent: 'space-between' }}>
                            <TouchableOpacity onPress={this.clickfulldtailsbind.bind(this)} style={{ padding: 5, justifyContent: 'space-between', flexDirection: 'row', width: '100%' }} >
                                <Text style={{ fontSize: 20, fontFamily: 'Helvetica', paddingTop: 5, fontWeight: "bold" }}>MY ADDRESS BOOK</Text>
                                <Icon name="left-open" size={20} style={{ transform: [{ rotate: '180deg' }],fontWeight: 5, marginTop: 8, marginRight: 6 }} />
                            </TouchableOpacity>
                        </View>

                        <View style={{ width: windowwidth - 33, paddingBottom: 10, margin: 6, borderWidth: 1, borderColor: 'gray', marginRight: 10 }}>

                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 15 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Name</Text>
                                {!!this.state.NameState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter full name.</Text>
                                )}
                                <TextInput
                                    placeholder="Full Name"
                                    onChangeText={this.handleInputChangeName}
                                    value={this.state.Name}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Email Address</Text>
                                {!!this.state.EmailState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter valied email.</Text>
                                )}
                                <TextInput
                                    placeholder="Email"
                                    onChangeText={this.handleInputChangeEmail}
                                    value={this.state.Email}
                                    style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                    underlineColorAndroid="transparent"
                                />

                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Country</Text>
                                {!!this.state.CountryState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please select Country.</Text>
                                )}
                                <View style={{ borderWidth: 1, borderColor: 'gray', paddingTop: 5, paddingBottom: 5, paddingLeft: 1, margin: 6, marginTop: 0 }}>
                                    <RNPickerSelect
                                        useNativeAndroidPickerStyle={false}
                                        hideIcon={true}
                                        placeholder={{}}
                                        items={Settings.Country}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.Country}
                                        underline={"transparent"}
                                        underlineColorAndroid="transparent"
                                        onValueChange={(Selectvalue, index) => {
                                            console.log("hellloooooo", Selectvalue);
                                            this.setState({ Country: Selectvalue })

                                        }}
                                    >
                                    </RNPickerSelect>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Language</Text>
                                {!!this.state.LanguageState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please select Language.</Text>
                                )}
                                <View style={{ borderWidth: 1, borderColor: 'gray', paddingTop: 5, paddingBottom: 5, paddingLeft: 1, margin: 6, marginTop: 0 }}>
                                    <RNPickerSelect
                                        useNativeAndroidPickerStyle={false}
                                        hideIcon={true}
                                        placeholder={{}}
                                        items={Settings.Language}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.Language}
                                        underline={"transparent"}
                                        underlineColorAndroid="transparent"
                                        onValueChange={(Selectvalue, index) => {
                                            console.log("hellloooooo22222", Selectvalue);
                                            this.setState({ Language: Selectvalue })

                                        }}
                                    >
                                    </RNPickerSelect>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Phone Number</Text>
                                {!!this.state.PhoneNumberState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter Phone Number.</Text>
                                )}
                                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                    <View style={{ borderWidth: 1, borderColor: 'gray', paddingTop: 3, paddingBottom: 3, paddingLeft: 1, margin: 6, marginTop: 0, width: '30%' }}>
                                        <PhoneInput
                                            ref={(ref) => { this.phone = ref; }}
                                            initialCountry={'sg'}
                                            style={{
                                                width: '100%',
                                                paddingBottom: 6,
                                                paddingTop: 6,
                                                paddingLeft: 2
                                            }}
                                        />
                                    </View>
                                    <View style={{ alignItems: "center", borderWidth: 1, borderColor: 'gray', paddingTop: 3, paddingBottom: 3, paddingLeft: 1, margin: 6, marginTop: 0, width: '62%' }}>
                                        <TextInput
                                            placeholder="Mobile Number*"
                                            keyboardType='phone-pad'
                                            onChangeText={this.handleInputChangePhoneNumber}
                                            value={this.state.PhoneNumber}
                                            style={{ width: '100%', padding: 3, marginBottom: 0, alignSelf: 'center' }}
                                            underlineColorAndroid="transparent"
                                        />

                                    </View>
                                </View>
                                <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                    <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Date of Birth</Text>
                                    {!!this.state.dobState && (
                                        <Text style={{ color: 'red', marginLeft: 6 }}>Please select DOB.</Text>
                                    )}

                                    <View style={{ paddingTop: 3, paddingBottom: 3, paddingLeft: 1, margin: 6, marginTop: 0 }}>
                                        <DatePicker
                                            style={{ width: '100%' }}
                                            date={this.state.dob}
                                            mode="date"
                                            placeholder="select date"
                                            format="YYYY-MM-DD"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36
                                                }
                                                // ... You can check the source to find the other keys.
                                            }}
                                            onDateChange={(date) => { this.setState({ dob: date }) }}
                                        />
                                    </View>

                                </View>
                                <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                    <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Gender</Text>


                                    <View style={{ paddingTop: 3, paddingBottom: 3, paddingLeft: 1, margin: 6, marginTop: 0 }}>
                                        <RadioForm
                                            radio_props={radio_props}
                                            initial={this.state.genderSelection}
                                            onPress={(value) => { this.setState({ gender: value }) }}
                                            buttonColor={'#77797c'}
                                            buttonWrapStyle={{ color: '#77797c' }}
                                            buttonSize={7}
                                            buttonOuterSize={17}
                                            selectedButtonColor={'#77797c'}
                                            formHorizontal={true}
                                            labelHorizontal={true}
                                            
                                        />
                                    </View>

                                </View>


                            </View>

                            <View>
                                <View style={styles.containerButton}>
                                    <TouchableOpacity style={styles.buttonContainer} onPress={this.onPressLearnMore.bind(this)}>
                                        <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>SAVE</Text>
                                    </TouchableOpacity >
                                </View>

                            </ View>
                        </View>
                      {this.state.visible &&(  <View style={{ width: windowwidth - 33, paddingBottom: 10, margin: 6, borderWidth: 1, borderColor: 'gray', flexDirection: 'column', marginRight: 10, justifyContent: 'space-between' }}>

                            <Text style={{ marginLeft:4,fontSize: 20, fontFamily: 'Helvetica', paddingTop: 5, fontWeight: "bold" }}>CHANGE PASSWORD</Text>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Old Password</Text>
                                {!!this.state.EmailState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter valied email.</Text>
                                )}
                                <View style={{ borderWidth: 1, borderColor: 'gray', margin: 6, marginTop: 0, flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <TextInput
                                          placeholder="Password*"
                                          secureTextEntry
                                        onChangeText={this.handleInputChangeEmail}
                                        value={this.state.OldPassword}
                                        style={{ padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <TouchableOpacity onPress={this.Showpassword.bind(this, "oldpassword")}>
                                        <Icon2 name="Show-password" size={13} style={{ fontWeight: 5, marginTop: 12, marginRight: 6 }} />
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>New Password</Text>
                                {!!this.state.EmailState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter valied email.</Text>
                                )}
                                <View style={{ borderWidth: 1, borderColor: 'gray', margin: 6, marginTop: 0, flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <TextInput
                                          placeholder="Password*"
                                          secureTextEntry={false}
                                        onChangeText={this.handleInputChangeEmail}
                                        value={this.state.newPassword}
                                        style={{ padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <TouchableOpacity onPress={this.Showpassword.bind(this, "newpassword")}>
                                        <Icon2 name="Show-password" size={13} style={{ fontWeight: 5, marginTop: 12, marginRight: 6 }} />
                                    </TouchableOpacity>
                                </View>


                            </View>
                            <View style={{ flexDirection: 'column', margin: 2, marginTop: 5 }}>
                                <Text style={{ fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6 }}>Confirm Password</Text>
                                {!!this.state.EmailState && (
                                    <Text style={{ color: 'red', marginLeft: 6 }}>Please enter valied email.</Text>
                                )}
                                <View style={{ borderWidth: 1, borderColor: 'gray', margin: 6, marginTop: 0, flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <TextInput
                                      placeholder="Password*"
                                      secureTextEntry={true}
                                        onChangeText={this.handleInputChangeEmail}
                                        value={this.state.cofirmPassword}
                                        style={{ padding: 3, paddingLeft: 6, margin: 6, marginTop: 0 }}
                                        underlineColorAndroid="transparent"
                                    >

                                    </TextInput>
                                    <TouchableOpacity onPress={this.Showpassword.bind(this, "confirm")}>
                                        <Icon2 name="Show-password" size={13} style={{ fontWeight: 5, marginTop: 12, marginRight: 6 }} />
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <View>
                                <View style={styles.containerButton}>
                                    <TouchableOpacity style={styles.buttonContainer} onPress={this.onPressLearnMore.bind(this)}>
                                        <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>SAVE</Text>
                                    </TouchableOpacity >
                                </View>

                            </ View>

                        </View>)}
                    </KeyboardAvoidingView>
                </ScrollView>

            </View >
        )
    }

}

const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: '#39393a',
        borderRadius: 2,

        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1, borderColor: '#414142',
        width: '100%',
        paddingBottom: 10,
        paddingTop: 10,

        alignItems: "center"
    }, container: {
        backgroundColor: '#e2e2e2',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }

})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {

        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        backgroundColor: 'red'
    }



});