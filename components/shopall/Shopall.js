
import React from 'react';
import { ImageBackground, TouchableOpacity, Dimensions, ActivityIndicator, View, Image, StyleSheet, Text } from 'react-native';
import Api from "../../utils/Api/ShopallApis";
import currencyFormatter from '../../constants/currncy_DateFormate';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from "../../constants/Settings"
import commonsettings from '../../constants/commonsettings';
import Designer from '../../utils/Api/Designer';
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const width = (Dimensions.get('window').width / 2);
const Screenwidth = (Dimensions.get('window').width);
const Screenheight = (Dimensions.get('window').height);
var numbercount = 1;
var imagesSecond = '';
var NameSecond = '';
var SmalltextSecond = '';
var discountSecond = '';
var priceSecond = '';
var idSecond = '';
var stockSecond = '';
var designerfilter = "";
var MerchentidSecond = '';
var category = Settings.category_filter;
var sortby = Settings.sortby_filter;
var name = '';
export default class Shopall extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isLoading1: false,
            page: 1,
            dialogVisible: true,
            loading: true,
            dataSource: [],
        };



    }

    GetItem1() {
        this.setState({
            isLoading: true,
        });
        this.setState(state => ({ page: 1 }), () => this.handleSearch())

    }

    GetItem() {
        this.setState({
            isLoading1: true,
        });
        this.setState(state => ({ page: state.page + 1 }), () => this.webCall())

    }

    handleClick(item) {
        category = Settings.category_filter;

        Settings.sortby_filter = item;
        designerfilter = Settings.Designer_filter;
        this.setState({ page: 1 })
        sortby = item
        Api.getShopall({
            limit: 40,
            select: 'brief/pricing/detail',
            page: 1,
            category: category,
            sortby: sortby,
            name: name,
            brand: designerfilter
        }
        )
            .then((responseJson) => {


                this.setState(state => ({

                    dataSource: responseJson.docs
                }));
                this.setState({
                    isLoading1: false,
                });
                this.setState({
                    isLoading: false,
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    handleSearch = () => {
        numbercount = 1
        this.setState({ page: 1 })

        Api.getShopall({
            limit: 40,
            select: 'brief/pricing/detail',
            page: 1,
            category: category,
            sortby: sortby,
            name: Settings.AllPrtoductFilterName
        }
        )
            .then((responseJson) => {

                // console.log("responseJson.docs",responseJson.docs)
                this.setState(state => ({

                    dataSource: responseJson.docs
                }));
                this.setState({
                    isLoading1: false,
                });
                this.setState({
                    isLoading: false,
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    webCall = () => {
        Api.getShopall({
            limit: 40,
            select: 'brief/pricing/detail',
            page: this.state.page,
            category: Settings.category_filter,
            sortby: Settings.sortby_filter,
            name: Settings.AllPrtoductFilterName,
            brand: Settings.Designer_filter
        }
        )
            .then((responseJson) => {

                this.setState(state => ({

                    dataSource: [...this.state.dataSource, ...responseJson.docs]
                }));
                this.setState({
                    isLoading1: false,
                });
                this.setState({
                    isLoading: false,
                });
                Settings.AllPrtoductFilterName = "";

            })
            .catch((error) => {
                console.error(error);
            });

    }


    ProductDetails(item, id, Merchentid, ImageUrl, smallText, Price, stock) {
        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopallStack';
        Settings.nav.navigate("AllproductPage", {
            name: item, id: id, Merchentid: Merchentid
            , ImageUrl: ImageUrl, smallText: smallText, Price: Price, stock: stock
        })
        // props.navigation.navigate("buttontesting", { name: item, id: id })

    }
    async componentWillMount() {

        this.webCall();

    }


    handleEnd = () => {

        this.setState(state => ({ page: state.page + 1 }), () => this.webCall())
    }

    renderLastOneItem() {

        numbercount = 1;

        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', overflow: "visible" }}>

                <TouchableOpacity onPress={this.ProductDetails.bind(this, NameSecond, idSecond, MerchentidSecond, imagesSecond, SmalltextSecond, discountSecond, stockSecond)} style={{ width: width - 5, alignItems: "center", borderBottomWidth: 1, borderRightWidth: 1, borderColor: '#e2e2e2', padding: 5, paddingBottom: 14, paddingTop: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        {/* <Image resizeMode="cover" style={{
                                width: (Screenwidth / 2) - 20,
                                marginTop: 14,
                                height: (Screenheight / 3),
                                marginRight: 7,
                                alignItems: "center",
                                margin: 7,


                            }} source={{ uri: imagesSecond }} /> */}
                        {stockSecond == 0 && (<ImageBackground opacity={0.3} resizeMode="cover" source={{ uri: imagesSecond }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>

                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-start', marginBottom: 55, marginLeft: 30 }}>
                                <Image
                                    opacity={1}
                                    source={require('../../assets/images/Sold_Out.png')}
                                    style={{ width: (Screenwidth / 3) - 20, height: 20, alignItems: 'center', justifyContent: 'center' }} />

                            </View>

                        </ImageBackground>)}

                        {stockSecond != 0 && (<ImageBackground resizeMode="cover" source={{ uri: imagesSecond }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>
                        </ImageBackground>)}

                        {/* <Icon name="wishlist" size={18} /> */}
                    </View>
                    {stockSecond == 0 && (<View opacity={0.3} style={{ width: '100%' }}>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 50,
                            alignSelf: 'center',
                        }}>{NameSecond}</Text>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            alignSelf: 'center',
                        }}>{SmalltextSecond}</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            fontSize: 16,
                            fontFamily: 10,
                            fontFamily: 'Helvetica',
                            fontWeight: 'bold'

                        }} >{discountSecond}</Text>


                        {discountSecond != priceSecond && (
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                fontSize: 16,

                                fontFamily: 'Helvetica',
                                fontWeight: 'bold',
                                color: '#c6c6c6',
                                textDecorationLine: "line-through"
                            }} >{priceSecond}</Text>)}
                    </View>)}
                    {stockSecond != 0 && (<View style={{ width: '100%' }}>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 50,
                            alignSelf: 'center',
                        }}>{NameSecond}</Text>
                        <Text style={{
                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            alignSelf: 'center',
                        }}>{SmalltextSecond}</Text>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            paddingTop: 1,
                            fontSize: 16,
                            fontFamily: 10,
                            fontFamily: 'Helvetica',
                            fontWeight: 'bold'

                        }} >{discountSecond}</Text>


                        {discountSecond != priceSecond && (
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                fontSize: 16,

                                fontFamily: 'Helvetica',
                                fontWeight: 'bold',
                                color: '#c6c6c6',
                                textDecorationLine: "line-through"
                            }} >{priceSecond}</Text>)}
                    </View>)}
                </TouchableOpacity>
            </View>
        )

    }
    renderPage(imagesUrl, index, Name, Smalltext, discount, price, id, Merchentid, stock) {

        if (numbercount == 1) {

            numbercount = 2; imagesSecond = imagesUrl; NameSecond = Name; SmalltextSecond = Smalltext; discountSecond = discount, priceSecond = price, idSecond = id, MerchentidSecond = Merchentid;
            stockSecond = stock;
            return null;
        }
        else {
            numbercount = 1;
            return (
                <View key={index} style={{ flexDirection: 'row', overflow: "visible" }}>

                    <TouchableOpacity onPress={this.ProductDetails.bind(this, NameSecond, idSecond, MerchentidSecond, imagesSecond, SmalltextSecond, discountSecond, stockSecond)} style={{ width: width - 5, alignItems: "center", borderBottomWidth: 1, borderRightWidth: 1, borderColor: '#e2e2e2', padding: 5, paddingBottom: 14, paddingTop: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            {/* <Image resizeMode="cover" style={{
                                width: (Screenwidth / 2) - 20,
                                marginTop: 14,
                                height: (Screenheight / 3),
                                marginRight: 7,
                                alignItems: "center",
                                margin: 7

                            }} source={{ uri: imagesSecond }} /> */}
                            {stockSecond == 0 && (
                            <ImageBackground opacity={0.3} resizeMode="cover" source={{ uri: imagesSecond }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>
                             
                                <View style={{ flex: 1,justifyContent: 'flex-end', alignItems: 'flex-start', marginBottom: 55, marginLeft: 30 }}>
                                    <Image
                                        opacity={1}
                                        source={require('../../assets/images/Sold_Out.png')}
                                        style={{ width: (Screenwidth / 3) - 20, height: 20, alignItems: 'center', justifyContent: 'center' }} />
                                </View>

                            </ImageBackground>)}

                            {stockSecond != 0 && (<ImageBackground resizeMode="cover" source={{ uri: imagesSecond }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>
                            </ImageBackground>)}

                            {/* <Icon2 name="Sold-Out" size={23} style={{ fontWeight: 5 }} ></Icon2> */}
                            {/* <Icon name="wishlist" size={18} /> */}
                        </View>
                        {stockSecond != 0 && (<View style={{ width: '100%' }}>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'lucidabrightdemiregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 50,
                                alignSelf: 'center',
                            }}>{NameSecond}</Text>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                alignSelf: 'center',
                            }}>{SmalltextSecond}</Text>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                fontSize: 16,
                                fontFamily: 10,
                                fontFamily: 'Helvetica',
                                fontWeight: 'bold'

                            }} >{discountSecond}</Text>
                            {discountSecond != priceSecond && (
                                <Text resizeMode="cover" style={{
                                    width: Screenwidth / 2,
                                    textAlign: 'center',
                                    paddingTop: 1,
                                    fontSize: 16,
                                    fontFamily: 'Helvetica',
                                    fontWeight: 'bold',
                                    color: '#c6c6c6',
                                    textDecorationLine: "line-through"
                                }} >{priceSecond}</Text>)}
                        </View>)}


                        {stockSecond == 0 && (<View opacity={0.3} style={{ width: '100%' }}>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'lucidabrightdemiregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 50,
                                alignSelf: 'center',
                            }}>{NameSecond}</Text>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                alignSelf: 'center',
                            }}>{SmalltextSecond}</Text>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                fontSize: 16,
                                fontFamily: 10,
                                fontFamily: 'Helvetica',
                                fontWeight: 'bold'

                            }} >{discountSecond}</Text>
                            {discountSecond != priceSecond && (
                                <Text resizeMode="cover" style={{
                                    width: Screenwidth / 2,
                                    textAlign: 'center',
                                    paddingTop: 1,
                                    fontSize: 16,
                                    fontFamily: 'Helvetica',
                                    fontWeight: 'bold',
                                    color: '#c6c6c6',
                                    textDecorationLine: "line-through"
                                }} >{priceSecond}</Text>)}
                        </View>)}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.ProductDetails.bind(this, Name, id, Merchentid, imagesUrl, Smalltext, discount, stock)} style={{ width: width - 5, alignItems: "center", borderBottomWidth: 1, borderColor: '#e2e2e2', padding: 5, paddingBottom: 14, paddingTop: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            {/* <Image resizeMode="cover" style={{
                                width: (Screenwidth / 2) - 20,
                                height: (Screenheight / 3),

                                marginTop: 14,
                                alignItems: "center",
                                margin: 7,
                                justifyContent: 'center'

                            }} source={{ uri: imagesUrl }} /> */}

                            {stock == 0 && (
                            <ImageBackground opacity={0.3} resizeMode="cover" source={{ uri: imagesUrl }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>

                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-start', marginBottom: 55, marginLeft: 30 }}>
                                    <Image
                                        opacity={1}
                                        source={require('../../assets/images/Sold_Out.png')}
                                        style={{ width: (Screenwidth / 3) - 20, height: 20, alignItems: 'center', justifyContent: 'center' }} />
                                </View>

                            </ImageBackground>)}

                            {stock != 0 && (<ImageBackground source={{ uri: imagesUrl }} style={{ height: (Screenheight / 3), width: (Screenwidth / 2) - 20 }}>
                            </ImageBackground>)}
                            {/* <Icon name="wishlist" size={18} /> */}
                        </View>

                        {stock == 0 && (<View opacity={0.3} style={{ width: '100%' }}>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'lucidabrightdemiregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 50,
                                alignSelf: 'center',
                            }}>{Name}</Text>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                alignSelf: 'center',
                            }}>{Smalltext}</Text>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1, fontSize: 16,
                                fontFamily: 'Helvetica',
                                fontWeight: 'bold'

                            }} >

                                {discount}</Text>

                            {discount != price && (
                                <Text style={{
                                    width: '100%',
                                    textAlign: 'center',
                                    paddingTop: 1, fontSize: 16,
                                    fontFamily: 'Helvetica',
                                    color: '#c6c6c6',
                                    textDecorationLine: "line-through",
                                    fontWeight: 'bold'
                                }} >{price}</Text>)}
                        </View>)}

                        {stock != 0 && (<View style={{ width: '100%' }}>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'lucidabrightdemiregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 50,
                                alignSelf: 'center',
                            }}>{Name}</Text>
                            <Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular', width: '100%',
                                textAlign: 'center',
                                paddingTop: 1,
                                alignSelf: 'center',
                            }}>{Smalltext}</Text>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                paddingTop: 1, fontSize: 16,
                                fontFamily: 'Helvetica',
                                fontWeight: 'bold'

                            }} >

                                {discount}</Text>

                            {discount != price && (
                                <Text style={{
                                    width: '100%',
                                    textAlign: 'center',
                                    paddingTop: 1, fontSize: 16,
                                    fontFamily: 'Helvetica',
                                    color: '#c6c6c6',
                                    textDecorationLine: "line-through",
                                    fontWeight: 'bold'
                                }} >{price}</Text>)}
                        </View>)}

                    </TouchableOpacity>
                </View>
            );
        }



    }


    render() {



        if (this.state.isLoading) {
            return (

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100 }}>

                    <ActivityIndicator size="large" color="gray" />

                </View>


            );

        }

        return (
            // <View  parentReference = {this.handleClick.bind(this)} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <View style={styles.MainContainer}>
                    {this.state.dataSource.map((image, index) => this.renderPage("http://buyerservices.glux.sg/uploads/user/undefined/products/" + image._id + "/" + image.brief.images[0], index, image.detail.product_brand, image.brief.name.toUpperCase(), currencyFormatter.FormateCurrency(image.pricing.discount_price2), currencyFormatter.FormateCurrency(image.brief.price), image._id, 0, image.brief.stock))}
                    {/* {this.state.dataSource.map((image, index) => this.renderPage("https://gstock.sg/buyer.gstock/uploads/user/" + image.merchant_id._id + "/products/" + image._id + "/" + image.brief.images[0], index, image.brief.name.toUpperCase(), image.brief.name, currencyFormatter.FormateCurrency(image.pricing.discount_price), currencyFormatter.FormateCurrency(image.brief.price), image._id, image.merchant_id._id))} */}
                    {numbercount == 2 && (this.renderLastOneItem())}
                    {this.state.isLoading1 && (<View style={{ flex: 1, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="small" color="black" />
                    </View>)}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    MainContainer: {


        width: (width * 2) -10,

        overflow: "visible",
        marginBottom: 50,
        marginTop: 30,

    },

    imageView: {

        width: '50%',
        height: 100,
        marginRight: 7,
        alignItems: "center",
        margin: 7

    },

    textView: {

        width: '65%',
        textAlignVertical: 'center',
        padding: 10,
        color: '#000'

    },

});