
import React from 'react';
import {TouchableOpacity,ActivityIndicator, Dimensions, View,  StyleSheet, Text } from 'react-native';
import commonsettings from '../../constants/commonsettings';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Settings from '../../constants/Settings';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Register from './RegisterUser'


const Icon = createIconSetFromFontello(fontelloConfig);
const windowwidth = Dimensions.get('window').width;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;



export default class MainLoginRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: true,
           
         
        };


    }

   


    clickBack() {

        Settings.nav.navigate(Settings.backfromLogin)

    }

   



    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                          <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} color={"gray"} /></TouchableOpacity>
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>{Settings.RegisterLogin}</Text>
                        </View>
                        
                    </View>

                )}
               
            >


                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>

                     <Register></Register>
                    
                </View> 
            </StickyHeaderFooterScrollView>
        )
    }
  

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
