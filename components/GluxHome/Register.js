import React from "react";
import { Platform, AppRegistry, TouchableHighlight, TouchableNativeFeedback, TouchableOpacity, Text, StyleSheet, Dimensions, View, Image } from "react-native";
import { Dialog } from 'react-native-simple-dialogs';
import Expo from "expo";
import Settings from '../../constants/Settings';
const windowheight = 180;
const left = Dimensions.get('window').width;
export default class Register extends React.Component {


  constructor(props) {

    super(props);

  }


  onPressRegister() {
    Settings.backfromLogin="HomeStack";
    Settings.RegisterLogin="REGISTER";
    Settings.nav.navigate("MainLoginRegister")
  }
  onPressLogin() {
    Settings.backfromLogin="HomeStack";
    Settings.RegisterLogin="LOGIN";
    
     Settings.nav.navigate("MainLogin")
  }




  render() {
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
      TouchableElement = TouchableNativeFeedback;
    }

    return (

      <View style={styles.header}>
       
        <TouchableOpacity onPress={this.props.action1} style={styles.Closebutton}><Image source={require('../../assets/images/Close.png')} />

        </TouchableOpacity >
        <View style={styles.topBox,[{marginTop:15,alignItems:"center"}]}>
          <Text style={{ fontFamily: 'Helvetica', fontSize: 24, fontWeight: 'bold' }}> LET'S GET EXCLUSIVE</Text>
        </View>
        <View style={styles.topBox}>
          <Text style={{ fontFamily: 'Helveticaregular', color: 'gray', }} > Sign in for an exclusive shopping experience</Text>
        </View>
        <View style={styles.container}>
          <TouchableOpacity onPress={this.onPressRegister} style={styles.buttonContainer}>
            <Text style={{ fontFamily: 'Helveticaregular', padding: 2,fontSize:20 }}>REGISTER</Text>

          </TouchableOpacity >
          <TouchableOpacity onPress={this.onPressLogin} style={styles.buttonContainer2}>
            <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc',fontSize:20 }}>SIGN IN</Text>

          </TouchableOpacity >

        </View>

      </View>



    );
  }
}

const styles = StyleSheet.create({
  header: {


    height: windowheight,
    width: left,
  },

  Closebutton: {
    right: 10,
    top: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end'

  },

  topBox: {

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    margin:10,
    marginTop:25,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',

  },
  buttonContainer: {
    backgroundColor: '#fff',
    padding: 10,
    shadowColor: '#f9fafc',
    shadowOffset: {
      width: 0,
      height: 3
    },
   
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: '#414142',
    marginRight: 6,
    width: 150,
    alignItems: "center"

  },
  buttonContainer2: {
    backgroundColor: 'black',
   
    padding: 10,
    shadowColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 3
    },
  
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: 'black',
    marginLeft: 6,
    width: 150,
    alignItems: "center"
  }

});
AppRegistry.registerComponent('Register', () => Register)