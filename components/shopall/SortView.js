import * as React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity, Text } from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';


const Icon = createIconSetFromFontello(fontelloConfig);

export default class SortView extends React.Component {
    constructor(props) {
        super(props);

        this.renderPage = this.renderPage.bind(this);
        this.state = {
            sortitem: 'Popular Items',
            Popularity: true,
            whats: false,
            lowtohigh: false,
            hightolow: false


        }
    }

    renderPage(item, name) {
        this.props.action1(item);
        switch (name) {
            case "pop":


                return (this.setState({
                    Popularity: true,
                    whats: false,
                    lowtohigh: false,
                    hightolow: false
                })

                );
            case "date":

                return (
                    this.setState({
                        Popularity: false,
                        whats: true,
                        lowtohigh: false,
                        hightolow: false
                    }));

            case "price":

                return (
                    this.setState({
                        Popularity: false,
                        whats: false,
                        lowtohigh: true,
                        hightolow: false
                    }));

            case "lowprice":

                return (
                    this.setState({
                        Popularity: false,
                        whats: false,
                        lowtohigh: false,
                        hightolow: true
                    }));

        }


    }

    async componentWillMount() {

        this.setState({
            Popularity: true,
            whats: false,
            lowtohigh: false,
            hightolow: false
        });
    }
    render() {

        return (
            <View>

                <View>

                    <View style={styles.container}>
                        {this.state.Popularity && (<TouchableOpacity onPress={() => this.renderPage('', 'pop')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#414142', flex: 3 , fontWeight: 'bold'}}>Popularity</Text>
                            <View>
                                <Icon name="tick" size={20} width={20} style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flex: 1 }} />
                            </View>
                        </TouchableOpacity >)}
                        {!this.state.Popularity && (<TouchableOpacity onPress={() => this.renderPage('', 'pop')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#414142', flex: 3 }}>Popularity</Text>

                        </TouchableOpacity >)}
                    </View>
                    <View style={styles.container}>
                        {this.state.whats && (<TouchableOpacity onPress={() => this.renderPage('-date', 'date')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#414142', flex: 3, fontWeight: 'bold' }}>What's new</Text>
                            <View>
                                <Icon name="tick" size={20} width={20} style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flex: 1 }} />
                            </View>
                        </TouchableOpacity >)}
                        {!this.state.whats && (<TouchableOpacity onPress={() => this.renderPage('-date', 'date')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#414142', flex: 3 }}>What's new</Text>

                        </TouchableOpacity >)}
                    </View>

                    <View style={styles.container}>
                        {this.state.lowtohigh && (<TouchableOpacity onPress={() => this.renderPage('price', 'price')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#414142', flex: 3, fontWeight: 'bold' }}>Price low to high</Text>
                            <View>
                                <Icon name="tick" size={20} width={20} style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flex: 1 }} />
                            </View>
                        </TouchableOpacity >)}
                        {!this.state.lowtohigh && (<TouchableOpacity onPress={() => this.renderPage('price', 'price')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#414142', flex: 3 }}>Price low to high</Text>

                        </TouchableOpacity >)}
                    </View>
                    <View style={styles.container}>
                        {this.state.hightolow && (<TouchableOpacity onPress={() => this.renderPage('-price', 'lowprice')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#414142', flex: 3 , fontWeight: 'bold'}}>Price high to low</Text>
                            <View>
                                <Icon name="tick" size={20} width={20} style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flex: 1 }} />
                            </View>
                        </TouchableOpacity >)}
                        {!this.state.hightolow && (<TouchableOpacity onPress={() => this.renderPage('-price', 'lowprice')} style={styles.buttonContainer}>
                            <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#414142', flex: 3 }}>Price high to low</Text>

                        </TouchableOpacity >)}
                    </View>
                </View>


            </View>

        )
    }
}

const styles = StyleSheet.create({

    buttonContainer: {
        backgroundColor: '#00000000',
        flexDirection: 'row',
        paddingTop: 20,

        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderBottomWidth: 1, borderColor: 'gray',

        width: '80%',

    }, container: {
        backgroundColor: '#00000000',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});