import settingss from '../../constants/Settings';

const servername = settingss.serverUrl1;
const endUrl = {
    Regist: servername + "/user/register",
    Starter:servername + "/user/login",
   
}
function callApi(endpoint, method, body, params, authToken,lastaddid=null) {
   
    let bodyIsJson = true;
    try {
        JSON.parse(body);
        // if(body==null)
        // {
        //     bodyIsJson = false;
        // }
    } catch (e) {
        bodyIsJson = false;
      //  console.log("mindulaaa",bodyIsJson)
    }

   // console.log("mindano")

    let fullUrl:string = endpoint + constructParams(params);

    // if(lastaddid!=null){
        fullUrl=fullUrl + DeleteParam(lastaddid);
    // }
  
// console.log("fullUrl",fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    //console.log("hello",fetchOptions)
    // if (type == "op") {
    //     console.log("fullUrl")
    //     return fetch(fullUrl, fetchOptions)
    //         .then((response) => console.log("rerererer",response))

    // }
    // if (type != "op") {

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }
  
    return ("/"+ params)
}







function Regist(first_name, last_name, email,mobile_no,password,Terms,confirm_password,promotions) {

    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        first_name: first_name,
        last_name: last_name,
        Terms:true,
        email: email, 
        mobile_no:mobile_no,
        password:password,
        confirm_password:confirm_password,
        country:'singapore',
        promotions:promotions,
        user_group:'buyer'
    });

    return callApi(endUrl.Regist, "POST", body, null, null)
}

function Starter(email,password) {

    const body = JSON.stringify({      
        email: email,     
        password:password,       
        user_group:'buyer'
    });

    return callApi(endUrl.Starter, "POST", body, null, null)
}



export default {
    Regist,
    Starter
    
};













