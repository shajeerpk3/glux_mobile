import React from 'react';
import { Divider } from 'react-native-elements'
import { WebView, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Carousel from 'react-native-banner-carousel';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';
import ModalDropdown from 'react-native-modal-dropdown';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import CheckOUt2 from '../CheckOut/CheckOut2'
import { Row } from 'native-base';

const Icon = createIconSetFromFontello(fontelloConfig);
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowhalfwidthForIcon = ((windowwidth - 10) / 2) / 3;
const windowhalfwidthForLine = ((windowwidth - 10) / 2) / 2;


export default class CheckOut2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: true,
            images: [],
            dataSource: [],
            showDialogInfo: false
        };


    }

    Checkout1() {
       
        if (Settings.BillingAddressExistance == true) { Settings.nav.navigate("CheckOut3")} 
        else {
            this.setState({ showDialogInfo: true })

        }
    }


    clickBack() {
        // navigation goBack
        // Settings.fillingfrom=true;
        Settings.nav.navigate("CheckOut")

    }

    showdialog(showorhide) {
        this.setState({ showDialogInfo: showorhide })
    }



    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                            {commonsettings.backbuttonstatus && (<TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>)}
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>CHECKOUT</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                )}
                renderStickyFooter={() => (
                    <View>
                        <View style={s.containerButton}>
                            <TouchableOpacity style={s.buttonContainer} onPress={this.Checkout1.bind(this)}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>CONFIRM AND PAY ({Settings.BagItemCOunt} items) </Text>
                            </TouchableOpacity >
                        </View>

                    </ View>

                )}
            >





                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>


                    <Dialog
                        title="Please add Billing Address"
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                                backgroundColor: '#fff'
                            }
                        }
                        onTouchOutside={() => this.showdialog(false)}
                        visible={this.state.showDialogInfo}
                    >
                        <View>

                            <View style={{ flexDirection: 'row', alignItems:'center', justifyContent: "center",marginTop:10 }}>

                                <TouchableOpacity onPress={() => this.showdialog(false)} style={[s.buttonContainer1, { marginLeft: 2 }]}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Close </Text>
                                </TouchableOpacity >
                            </View>
                        </View>



                    </Dialog>

                    <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                    }}>


                        <View style={{ width: 270, height: 22, marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: '#262626'


                            }}>
                                SHIPPING
                            </Text>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                              color: '#262626'


                            }}>
                                PAYMENT
                            </Text>
                            <Text style={{

                                fontSize: 12,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: '#c6c6c6'


                            }}>
                                CONFIRMATION
                            </Text>

                        </View>
                    </View>
                    <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                    }}>


                        <View style={{ width: 250, height: 22, marginTop: 5, flexDirection: 'row' }}>
                            {/* <Image
                                resizeMode="contain"
                                source={require('../../assets/images/cHECKoUT11.png')}
                                style={{ flex: 3, height: undefined, width: undefined }}
                            /> */}
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} />
                            </View>
                            <View style={{ width: 93, height: 22, flexDirection: 'row' }}>
                                <View style={{ width: 93, height: 13, borderColor: '#262626', borderBottomWidth: 2, flexDirection: 'row' }}></View>
                            </View>
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} />
                            </View>
                            <View style={{ width: 93, height: 22, flexDirection: 'row' }}>
                                <View style={{ width: 93, height: 13, borderColor: '#c6c6c6', borderBottomWidth: 2, flexDirection: 'row' }}></View>
                            </View>
                            <View style={{ width: 20, height: 22, flexDirection: 'row' }}>
                                <Icon name="g-points" size={22} color={'#c6c6c6'} />
                            </View>

                        </View>
                    </View>
                    <View style={{


                        alignItems: 'center',
                        marginTop: 15

                    }}>
                        <CheckOUt2 ></CheckOUt2>
                    </View>
                </View>
            </StickyHeaderFooterScrollView>
        )
    }
    //     return (
    //         <View style={s.header}>
    //         <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
    //             {commonsettings.backbuttonstatus && (<TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>)}
    //         </View>
    //         <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
    //             <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>CHECKOUT</Text>
    //         </View>
    //         <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
    //             <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
    //                 <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
    //             </TouchableOpacity>
    //         </View>
    //     </View>
    //     )
    // }

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
