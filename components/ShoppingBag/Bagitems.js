import React, { Component } from 'react';
import { ActivityIndicator, Picker, Alert, Image, Animated, TouchableOpacity, WebView, Text, View, Button, StyleSheet, ScrollView, Dimensions, Platform, TextInput } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import BagitemMain from './BagitemMain'
const halfwidth = Dimensions.get('window').width / 8;
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').height;


export default class AllproductPage extends Component {

    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            BagCount: false,
        }
        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopallStack';
    }


    handler() {

        if (commonsettings.bagCount != 0) {
            this.setState({
                BagCount: false,
            })
        }
        else {
            this.setState({
                BagCount: true,
            })
        }

    }

    clickBack() {
        // navigation goBack
        console.log("jijiji backkkk")
        Settings.fillingfrom = true;
        Settings.nav.navigate(commonsettings.backButtonPage)
    }

    CheckOUt() {
        console.log("jijijhihii checkout")
        Settings.fillingfrom = false;
        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopingBag';
        Settings.nav.navigate("CheckOut")
    }



    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                            {commonsettings.backbuttonstatus && (<TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>)}
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>SHOPPING BAG</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                )}
                renderStickyFooter={() => (
                    <View>
                        {!this.state.BagCount && (<View style={s.containerButton}>
                            <TouchableOpacity onPress={this.CheckOUt.bind(this)} style={s.buttonContainer}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>PROCEED TO CHECKOUT </Text>
                            </TouchableOpacity >
                        </View>)}
                        {this.state.BagCount && (<View style={s.containerButton}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={s.buttonContainer}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>START SHOPPING </Text>
                            </TouchableOpacity >
                        </View>)}
                    </ View>

                )}
            >

                <View style={{ borderWidth: 3, borderBottomWidth: 3, borderColor: '#c6c6c6', margin: 4, borderRadius: 6 }}>
                    <BagitemMain action1={this.handler.bind(this)} ></BagitemMain>
                </View>
            </StickyHeaderFooterScrollView>
        )
    }

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
