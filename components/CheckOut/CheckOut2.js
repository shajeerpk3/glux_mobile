import React from 'react';
import { View } from 'react-native';
import Bagitems from '../CheckOut/ItemSummery'
import ShippingDetails from '../CheckOut/ShippingDetails'
import BillingAddress from '../CheckOut/billingaddress'
import PaymentMethod from '../CheckOut/PaymentMethod'


export default class CheckOut2 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
    }


    render() {



        return (

            <View style={{ margin: 4, flexDirection: 'column', }}>

                <BillingAddress></BillingAddress>
                <Bagitems></Bagitems>

                <PaymentMethod></PaymentMethod>
               
            </View >
        )
    }
}