import * as React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Sortview from './SortView'
import Filterby from './Filterby'
import Settings from "../../constants/Settings"
const width = (Dimensions.get('window').width / 2) - 30;
const Windowheight = (Dimensions.get('window').height) / 2;
const widthPannel = (Dimensions.get('window').width) - 46;
const Icon = createIconSetFromFontello(fontelloConfig);


export default class SortbyFilerBy extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            height: 0,
            backcolor: '#00000000',
            forcolor: '#414142',
            bord: 0,
            bordtop: 0,
            height1: 0,
            backcolor1: '#00000000',
            forcolor1: '#414142',
            bord1: 0,
            bordtop1: 0,
            showhide: false,
            showhide1: false,
            sortby: ''
        }
        this.onPressLearnMore = this.onPressLearnMore.bind(this);
        this.onPressLearnMore1 = this.onPressLearnMore1.bind(this);
        this.handler = this.handler.bind(this);
    }


    handler(item) {
       
        this.props.action2(item);
        this.setState({showhide1:false})
    }

    clear(item) {
       
       Settings.Gender_filter="";
       Settings.category_filter="";
       Settings.Designer_filter="";
       this.props.action2(item);
       this.setState({showhide1:false})

    }

    onPressLearnMore() {
        this.setState(() => ({ showhide: this.state.showhide === false ? true : false }));
        this.setState(() => ({ forcolor: this.state.showhide === false ? '#ffffff' : '#414142' }));
        this.setState(() => ({ backcolor: this.state.showhide === false ? '#414142' : '#00000000' }));
        this.setState(() => ({ bordtop: this.state.showhide === false ? 5 : 0 }));
        this.setState(() => ({ height: this.state.showhide === false ? 190 : 0 }));
        this.setState({ showhide1: false });
        this.setState({ backcolor1: '#00000000' });
        this.setState({ forcolor1: '#414142' });
        this.setState({ bord1: 0 });
        this.setState({ bordtop1: 0 });
    }

    onPressLearnMore1() {
        this.setState(() => ({ showhide1: this.state.showhide1 === false ? true : false }));
        this.setState(() => ({ forcolor1: this.state.showhide1 === false ? '#ffffff' : '#414142' }));
        this.setState(() => ({ backcolor1: this.state.showhide1 === false ? '#414142' : '#00000000' }));
        this.setState(() => ({ bordtop1: this.state.showhide1 === false ? 5 : 0 }));
        this.setState(() => ({ height1: this.state.showhide1 === false ? 190 : 0 }));
        this.setState({ showhide: false });
        this.setState({ backcolor: '#00000000' });
        this.setState({ forcolor: '#414142' });
        this.setState({ bord: 0 });
        this.setState({ bordtop: 0 });
    }

    
    render() {

        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>


                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 15 }}>
                    <View style={styles.container1}>
                        <TouchableOpacity style={[styles.buttonContainer, { backgroundColor: this.state.backcolor }]} onPress={() => this.onPressLearnMore()}>
                            <Icon name="sort-by" size={16} color={this.state.forcolor} />
                            <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor }}>SORT BY</Text>

                        </TouchableOpacity >
                    </View>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => this.onPressLearnMore1()} style={[styles.buttonContainer, { backgroundColor: this.state.backcolor1 }]}>
                            <Icon name="filter-by" size={16} color={this.state.forcolor1} />
                            <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, color: this.state.forcolor1 }}>FILTER BY</Text>

                        </TouchableOpacity >
                    </View>


                </View>
                <View style={[styles.header, { borderWidth: this.state.bord, borderTopWidth: this.state.bordtop }]}>

                    {this.state.showhide && (<View style={{ height: 190, borderColor: '#414142', borderWidth: 1, borderTopWidth: 0 }}>
                        <Sortview action1={this.handler}></Sortview>
                    </View>)}
                </View>
                <View style={[styles.header, { borderWidth: this.state.bord1, borderTopWidth: this.state.bordtop1 }]}>

                    <View >
                        {this.state.showhide1 && (<View style={{ height: Windowheight + 60, borderColor: '#414142', borderWidth: 1, borderTopWidth: 0 }}>
                            <View style={{ height: Windowheight + 30 }}>
                                <Filterby action2={this.handler}></Filterby>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={[styles.buttonApplay, { backgroundColor: '#00000000' }]}>
                                    <TouchableOpacity onPress={this.clear.bind(this, Settings.sortby_filter)}>
                                        <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', padding: 2, color: '#414142' }}>CLEAR</Text>
                                    </TouchableOpacity >
                                </View>
                                <View style={[styles.buttonApplay, { backgroundColor: '#414142' }]}>
                                    <TouchableOpacity onPress={this.handler.bind(this, Settings.sortby_filter)}>
                                        <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', padding: 2, color: '#ffffff' }}>APPLY</Text>
                                    </TouchableOpacity >
                                </View>
                            </View>


                        </View>)}

                    </View>

                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({

    buttonContainer: {
        backgroundColor: '#fff',
        padding: 8,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: width,
        flexDirection: 'row',
        justifyContent: 'center'
    }, container: {
        marginLeft: 7,
        flexDirection: 'row',
    },
    header: {

        borderColor: '#414142',
        zIndex: 2,
        width: widthPannel,

    },

    container1: {
        marginRight: 7,
        flexDirection: 'row',
    },
    buttonApplay: {
        height: 30,
        width: (widthPannel / 2),
        borderColor: '#414142',
        borderTopWidth: 1,
        alignItems: 'center', justifyContent: 'center'

    }


});