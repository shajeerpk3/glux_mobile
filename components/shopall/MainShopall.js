import React from "react";
import GluxHome from "../GluxHome/Home";
import SearchHome from "../GluxHome/searchby";
import SortbyFilterby from "./SortbyFilerBy"
import Shopall from "./Shopall";
import Settings from "../../constants/Settings";
import commonsettings from '../../constants/commonsettings';
import { NavigationEvents } from "react-navigation";
import { Text, StyleSheet, View, Dimensions, ScrollView } from "react-native";
import ProductDetails from '../ProductDetails/ProductDeatails'
import Drawer from 'react-native-drawer'
import ControlPanel from './SearchingSidebar'
const windowWidth = Dimensions.get('window').width;
const windowheight = Dimensions.get('window').height;
export default class main extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {

    super(props);

    this.state = {

      status: true,
      page: 1,
      sort: '',
      drowerStatus: false

    };
    this.handler4 = this.handler4.bind(this)
    this.handler3 = this.handler3.bind(this)
    this.handler = this.handler.bind(this);

  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  async componentWillMount() {
    commonsettings.backbuttonstatus = false;
    if (Settings.DroverStatus == true) {
      this.setState({ drowerStatus: true })
      Settings.DroverStatus == false
    }
  }


  handler(item) {
    this.refs.Shopall.handleClick(item);
  }
  handler3() {
    this.setState({ drowerStatus: true })
  }
  handler4() {
    this.setState({ drowerStatus: false })
    this.refs.Shopall.GetItem1();

  }



  render() {
    const drawerStyles = {
      drawer: { shadowColor: '#000000', backgroundColor: '#fff', shadowOpacity: 0.8, shadowRadius: 3 },
      main: { paddingLeft: 0 },
    }
    return (
      <Drawer
        type="overlay"
        open={this.state.drowerStatus}
        tapToClose={true}
        openDrawerOffset={0.0} // 20% gap on the right side of drawer
        panCloseMask={0.0}
        closedDrawerOffset={-3}
        content={<ControlPanel action4={this.handler4} />}

        styles={drawerStyles}
        tweenHandler={(ratio) => ({
          ControlPanel: { opacity: (2 - ratio) / 2 }
        })}
      >
        <View style={{ backgroundColor: '#fff', height: '100%' }}>
          <NavigationEvents
            onWillFocus={payload => {
              this.handler(Settings.sortby_filter);
            }}
          />
          <GluxHome></GluxHome>
          <SearchHome action3={this.handler3}></SearchHome>
          <SortbyFilterby action2={this.handler}></SortbyFilterby>
          {/* <SlidingExample></SlidingExample>  */}
          <ScrollView
            scrollEventThrottle={1000}
            onScroll={({ nativeEvent }) => {
              if (this.isCloseToBottom(nativeEvent)) {
                this.refs.Shopall.GetItem();
              }
            }}>


            <Shopall ref='Shopall'></Shopall>

            {/* <SlidingExample></SlidingExample> */}

          </ScrollView>
        </View>
      </Drawer>

    );
  }
}

const styles = StyleSheet.create({
  header: {

    backgroundColor: '#f8f9f7',
    height: windowheight

  },



});