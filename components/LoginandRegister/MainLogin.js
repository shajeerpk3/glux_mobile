
import React from 'react';
import { Platform, TouchableOpacity, KeyboardAvoidingView, ActivityIndicator, Dimensions, View, StyleSheet, Text, TextInput } from 'react-native';
import commonsettings from '../../constants/commonsettings';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Settings from '../../constants/Settings';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Api from "../../utils/Api/starter";
import { Dialog } from "react-native-simple-dialogs";
import Api2 from "../../utils/Api/accountUpdates";


const Icon = createIconSetFromFontello(fontelloConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default class MainLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: true,
            EmailState: false,
            Email: '',
            EmailMsg: '*Required',
            PasswordState: false,
            Password: '',
            dialogVisible2: false,


        };

        this.starter = this.starter.bind(this)
    }

    handleInputChangePassword = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PasswordState: true })
            this.setState({ Password: text.trim() })
        }
        else {
            this.setState({ PasswordState: false })
            this.setState({ Password: text })
        }
    }
    handleInputChangeEmail = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ EmailState: true })
            this.setState({ Email: text.trim() })
            this.setState({ EmailMsg: 'Email is requerd' })
        }
        else {
            this.setState({ EmailState: false })
            this.setState({ Email: text })
        }
    }
    ClickRegister() {
        Settings.RegisterLogin = "REGISTER";
        Settings.nav.navigate("MainLoginRegister")
    }
    Clickstarter() {
        var savingstate = true
        if (this.state.Password == '' || this.state.Password == undefined || this.state.Password == null) {
            this.setState({ PasswordState: true })
            savingstate = false
        }
        if (this.state.Email == '' || this.state.Email == undefined || this.state.Email == null) {
            this.setState({ EmailState: true })
            this.setState({ EmailMsg: "Requerd*" })
            savingstate = false
        }
        if (savingstate == true) {

            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(this.state.Email) === false) {
                this.setState({ EmailMsg: "Must be a valid email address" })
                this.setState({ EmailState: true })
                savingstate = false
            }
            if (savingstate == true) { this.starter() }
        }
    }


    setUseName(){
        Api2.accountdetails(Settings.userid)
        .then((responseJson) => {    
            console.log("responseJson.docs[0].profile.first_name",responseJson.docs[0].profile.first_name)    
                Settings.userName= responseJson.docs[0].profile.first_name;
                Settings.nav.navigate(Settings.backfromLogin)
        })
        .catch((error) => {
            console.error(error);
        });
    }


    starter() {


        Api.Starter(this.state.Email, this.state.Password)
            .then((responseJson) => {
                console.log("responseJson", responseJson);
                if (responseJson.status_code == "200") {
                    Settings.authorization = responseJson.token;
                    // console.log("Auth,",  Settings.authorization )
                    Settings.userid = responseJson.user_id;
                    Settings.userEmail = responseJson.email;
                    Settings.LoginStatus =true;
                    this.setUseName();
                   
                 

                }
                else {
                    this.setState({ ResultData: responseJson.message })
                    this.setState({ dialogVisible2: true })

                    this.interval = setInterval(() => {

                        clearInterval(this.interval);
                      
                        this.setState({ dialogVisible2: false })

                    }, 2000);
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.setState({ ResultData: error.message })
                this.setState({ dialogVisible2: true })
                this.interval = setInterval(() => {

                    clearInterval(this.interval);
                    
                    this.setState({ dialogVisible2: false })

                }, 2000);
            });
    }


    clickBack() {

        Settings.nav.navigate(Settings.backfromLogin)

    }





    render() {
        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} color={"gray"} /></TouchableOpacity>
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>{Settings.RegisterLogin}</Text>
                        </View>

                    </View>

                )}
                renderStickyFooter={() => (
                    <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ margin: 2, marginTop: 15, width: '95%', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10 }}>
                            <View style={s.containerRegister}>
                                <TouchableOpacity onPress={this.Clickstarter.bind(this)} style={s.buttonContainerRegister}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20, color: '#f9f9fc' }}>SIGN IN</Text>

                                </TouchableOpacity >
                            </View>


                            <View style={s.containerSignin}>
                                <TouchableOpacity onPress={this.ClickRegister} style={s.buttonContainerSignin}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>REGISTER</Text>

                                </TouchableOpacity >
                            </View>

                        </View>


                    </ View>

                )}

            >


                <View style={{ width: windowwidth, backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Dialog
                        visible={this.state.dialogVisible2}

                        onTouchOutside={() => this.setState({ dialogVisible2: false })} >
                        <View>
                            <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                        </View>
                    </Dialog>
                    <KeyboardAvoidingView

                        behavior="padding"
                    >

                        <View style={{ margin: 5, paddingBottom: 18, width: '95%', justifyContent: 'center', alignItems: 'center', height: windowHeight - 200 }} >
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 25, marginBottom: 25 }}>
                                <Icon name="glux-logo" size={70} style={{ fontWeight: 5 }} ></Icon>

                            </View>


                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}> <Icon2 name="Email-address" size={15} style={{ fontWeight: 5 }} ></Icon2></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>
                                    <TextInput
                                        placeholder="Email Address*"
                                        onChangeText={this.handleInputChangeEmail}
                                        value={this.state.Email}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.EmailState && (<Text style={{ marginLeft: 6, color: 'red' }}>{this.state.EmailMsg}</Text>)}
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                <Text style={{
                                    width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                        ios: {
                                            marginTop: 0
                                        },
                                        android: {
                                            marginTop: 3

                                        }

                                    })
                                }}> <Icon2 name="Password" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                <View style={{ flexDirection: 'column', width: '78%' }}>
                                    <TextInput
                                        placeholder="Password*"
                                        secureTextEntry
                                        autoCorrect={false}
                                        onChangeText={this.handleInputChangePassword}
                                        value={this.state.Password}
                                        style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    {!!this.state.PasswordState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                </View>
                            </View>

                        </View>
                    </KeyboardAvoidingView>
                </View>
            </StickyHeaderFooterScrollView>
        )
    }


}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },
    buttonContainerRegister: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#39393a',
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    }, containerSignin: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }

});
