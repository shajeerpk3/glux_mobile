import * as React from 'react';
import { Linking,WebView, SafeAreaView, Text, View, Animated, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import commonsettings from '../../constants/commonsettings';
import Api from "../../utils/Api/ProductDetailsApis";
import AutoResizeHeightWebView from 'react-native-autoreheight-webview';
import GluxHome from "../GluxHome/Home";
import SearchHome from "../GluxHome/search";
import ProductDetails from './ProductDeatails';
import Webview from './Webview';
import Moment from "moment";

// import DropdownAlert from 'react-native-dropdownalert';
var ScrollableTabView = require('react-native-scrollable-tab-view');
const windowhalfwidth = Dimensions.get('window').width / 2;
var datavalue = '';


export default class TabProductDetails extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            loading: true,
            index: 0,
            datestring:'',
            month:new Date().get,
            webviewHeight: 500,
            routes: [
                { key: 'WOMEN', title: 'PRODUCT DETAILS' },
                { key: 'MEN', title: 'SHIPPING AND RETURNS' },


            ],
            dataSource: []
        };




        this.webcall = this.webcall.bind(this);

    }







    webcall(dataobj) {
        let Threedays = new Date();
        Threedays = Moment(Threedays).add(3, 'day').format('YYYY-MM-DD'); // for specific format
        console.log("Threedays",Threedays)
        var date = new Date(Threedays).getDate()
        var month = new Date(Threedays).getMonth() + 1; //Current Month
        var year = new Date(Threedays).getFullYear()
        month=commonsettings.monthNames[month]
        this.setState({datestring:month + ' ' + date + ', ' + year})
        datavalue = dataobj;
        this.setState({ isLoading: false })

    }

    


    _handleIndexChange = index => this.setState({ index });


    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);

        return (
            <View key="SuperMainTabContaier">
                <View key="MainTabContaier" style={styles.tabBar}>
                    {props.navigationState.routes.map((route, i) => {
                        const color = props.position.interpolate({
                            inputRange,
                            outputRange: inputRange.map(
                                inputIndex => (inputIndex === i ? '#464647' : '#bcbcbc')
                            ),

                        });
                        return (
                            <TouchableOpacity key={route.title}
                                style={styles.tabItem}
                                onPress={() => this.setState({ index: i })}>
                                <Animated.Text style={{ color, fontFamily: 'Helvetica',  fontWeight: 'bold'}}>{route.title}</Animated.Text>
                            </TouchableOpacity >

                        );
                    })}

                </View >
                <View key="MainSelectionContaier" style={{ flexDirection: 'row' }}>
                    <View key="WomenSelectionContaier" style={this.state.index == 1 ? styles.selectionstyle2 : styles.selectionstyle1} >
                    </View>
                    <View key="MenSelectionContaier" style={this.state.index == 1 ? styles.selectionstyle1 : styles.selectionstyle2} >
                    </View>
                </View>
            </View>
        );
    };




    handler11(heighto) {

        this.setState({
            webviewHeight: heighto,
        });
    }





    PassingtoWebview(Webview) {
      
           
            try {
                Webview.binditem11(datavalue.product.detail.long_description)
             }
             catch (e) {
               
             }
    }




    binditem(dataobj) {

        Api.Itemdetails({
            product_id: dataobj
        }
        )
            .then((responseJson) => {
                this.webcall(responseJson);

            })
            .catch((error) => {
                console.error(error);
            });
        
    }

    FirstRoute = () => (
      
            <View style={{ flex: 1, backgroundColor: "#fff" }}>

                <Webview ref={(Webview) => { this.PassingtoWebview(Webview) }} action11={this.handler11.bind(this)} />
            </View>
        
    );

    SecondRoute = () => (

        <View style={{ flex: 1, backgroundColor: "#fff" }}>


            <View style={{ flexDirection: 'row', borderColor: "gray", borderWidth: 1, marginLeft: 5, height: 50, marginRight: 5, marginTop: 10 }}>
                <Text style={{ flex: 3, padding: 5, paddingTop: 15, fontFamily: 'Helvetica' , fontWeight: 'bold'}}>Estimated Delivery</Text><Text style={{ padding: 5, paddingTop: 15, fontFamily: 'Helveticaregular' }}>{this.state.datestring  }</Text>
            </View>
            <View style={{ flexDirection: 'row', borderColor: "gray", borderWidth: 1, marginLeft: 5, height: 50, marginRight: 5, marginTop: 3 }}>
                <Text onPress={() => Linking.openURL('http://terms.glux.sg/returnsandexchanges.component.html')} style={{ flex: 3, padding: 5, paddingTop: 15, fontFamily: 'Helvetica', color: '#d87136', textDecorationLine: "underline", fontWeight: 'bold' }}>Return Policy</Text><Text style={{ padding: 5, paddingTop: 15, fontFamily: 'Helveticaregular' }}>Returnable</Text>
            </View>
            <View style={{ marginLeft: 8, marginTop: 50 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontFamily: 'Helveticaregular' }}>Find out</Text><Text onPress={() => Linking.openURL('http://terms.glux.sg/returnsandexchanges.component.html')} style={{ fontFamily: 'Helvetica', color: '#d87136', textDecorationLine: "underline", fontWeight: 'bold' }}> Return Policy</Text>
                    <Text style={{ fontFamily: 'Helveticaregular' }}> about our shipping option,</Text>
                </View>

                <Text style={{ fontFamily: 'Helveticaregular' }}> including Self-Collect and Same Day delivery</Text>
            </View>
            
        </View>
    );

   



    _renderScene = SceneMap({
        WOMEN: this.FirstRoute,
        MEN: this.SecondRoute,

    });

    render() {

        if (this.state.isLoading) {
            return (
                <View style={{ backgroundColor: '#fff' }}></View>
               
            );

        }
        return (

            <View>
              

                <TabView style={{ overflow: "visible", height: this.state.webviewHeight }}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderTabBar={this._renderTabBar}
                    onIndexChange={this._handleIndexChange}
                />


            </View>


        );
    }
}
const styles = StyleSheet.create({
    containermsg: {
        flex: 1,
        backgroundColor: 'white',
    },
    container: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 20,
        backgroundColor: '#fff',
        height: 50,

    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 10

    },
    selectionstyle1: {
        width: windowhalfwidth,
        borderBottomColor: '#464647',
        borderBottomWidth: 6,

    },
    selectionstyle2: {

        width: windowhalfwidth,
        borderBottomColor: 'gray',
        borderBottomWidth: 4
    }
});
