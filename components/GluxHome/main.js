import React from "react";
import GluxHome from "../GluxHome/Home";
import Banner from "../GluxHome/Banner";
import SearchHome from "../GluxHome/search";
import Register from "../GluxHome/Register";
import MenuButton from "../GluxHome/MenuButton";
import PageTab from "../GluxHome/PageTab";
import { Text, StyleSheet, View, Dimensions, ScrollView } from "react-native";
import Womens from "../GluxHome/Womens";
import Bottombutton from "../GluxHome/Bottombutton"
import Spotlight from "../GluxHome/Spotlight";
import Settings from '../../constants/Settings';
import Api from "../../utils/Api/HomeApis"
import { NavigationEvents } from "react-navigation";


const windowheight = Dimensions.get('window').height;
export default class main extends React.Component {


  static navigationOptions = ({ navigation }) => {

    Settings.nav = navigation;
    // console.log("navigation",navigation)
    return {
      header: null
    };
  }


  constructor(props) {

    super(props);
    this.handler = this.handler.bind(this);
    this.state = {

      status: !Settings.LoginStatus,
      page: 1,


    }

  }
  // This method will be sent to the child component
  handler() {
    // console.log("sleepyy");
    this.setState({
      status: false
    });
  }

  handleRefresh() {
  Settings.category_filter='';
  Settings.sortby_filter='';
  Settings.Designer_filter='';
  Settings.Gender_filter='';
  Settings.AllPrtoductFilterName='';
  }


  async componentDidMount() {

    Api.AllProductSearchingada({
      limit: 1000000000,
      page: 1
    }

    )
      .then((responseJson) => {


        Settings.SerachData = responseJson.docs;
        // console.error(" Settings.SerachData", Settings.SerachData);
      })
      .catch((error) => {
        console.error(error);
      });
  }
  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  render() {

    return (
      <View style={styles.header}>
        <NavigationEvents
          onWillFocus={payload => {
            this.handleRefresh();
          }}
        />
        <GluxHome></GluxHome>

        <SearchHome ></SearchHome>
        <ScrollView


          // onScroll={({ nativeEvent }) => {
          //   if (this.isCloseToBottom(nativeEvent)) {
          //     this.refs.womens.GetItem();
          //   }
          // }}
          scrollEnabled>
          {
            this.state.status ? <Register action1={this.handler}></Register> : null
          }
          <MenuButton></MenuButton>
          <Banner ></Banner>
          <Bottombutton></Bottombutton>
          {/* <Womens ref='womens'></Womens> */}
          {/* <PageTab  ref='womens'></PageTab> */}
          <Womens></Womens>
          <Spotlight></Spotlight>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {

    backgroundColor: '#f8f9f7',
    height: windowheight

  },



});