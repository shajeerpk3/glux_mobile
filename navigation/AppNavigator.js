import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';
import AllproductPage from '../components/ProductDetails/AllproductPage'
import ShopingBag from '../components/ShoppingBag/Bagitems'
import CheckOut from '../components/CheckOut/CheckOutMain1'
import CheckOut2 from '../components/CheckOut/CheckOutMain2'
import CheckOut3 from '../components/CheckOut/CheckOutMain3'
import CheckOutAddress from '../components/CheckOut/AddressFromShipping'
import MainRegister from '../components/LoginandRegister/MainRegister'
import MainLogin from '../components/LoginandRegister/MainLogin'
import Paypal from '../components/CheckOut/Paypal'
import WebviewForCondtions from '../components/AccountDetails/WebVidew'
import AccountDetails from '../components/AccountDetails/Accountdetails'
import ShippingDetails from '../components/AccountDetails/ShippingDetails'
import Approvalscreen from '../components/CheckOut/apporove'
import OrderSummery from '../components/Ordersummery/Ordersummery'
import OrderSummeryDetails from '../components/Ordersummery/OrdersummeryDetails'
import AccountUpdate from '../components/AccountDetails/UpdateAccount'
export default createSwitchNavigator({

  Main: MainTabNavigator,
  AllproductPage: AllproductPage,
  ShopingBag: ShopingBag,
  CheckOut: CheckOut,
  CheckOutAddress: CheckOutAddress,
  CheckOut2:CheckOut2,
  CheckOut3:CheckOut3,
  MainLoginRegister:MainRegister,
  MainLogin:MainLogin,
  Paypal:Paypal,
  WebviewForCondtions:WebviewForCondtions,
  AccountDetails:AccountDetails,
  ShippingDetails:ShippingDetails,
  Approvalscreen:Approvalscreen,
  OrderSummery:OrderSummery,
  OrderSummeryDetails:OrderSummeryDetails,
  AccountUpdate:AccountUpdate
});