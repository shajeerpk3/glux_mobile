import React from 'react';
import { Image, TouchableOpacity, Dimensions, View, StyleSheet, Text } from 'react-native';
import commonsettings from '../../constants/commonsettings';
import icoMoonConfig from '../../src/selection.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Settings from '../../constants/Settings';
// import S from '../../assets/images/Card';

const Icon = createIconSetFromFontello(fontelloConfig);

const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;

export default class PaymentMethod extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            PaymentMethodTab: true,
            selectCard: false,
            border1: 1,
            border2: 3,
            border3: 1
        };
    }



    ClickPaymentMethodTab() {


        if (this.state.PaymentMethodTab == false) {


            this.setState({ PaymentMethodTab: true })
        } else {

            this.setState({ PaymentMethodTab: false })
        }


    }

    ClickPaymentMainItem(item) {
        if (item == "card") {
            this.setState({ selectCard: true });
            this.setState({ border1: 3 }); this.setState({ border2: 1 }); this.setState({ border3: 1 })
            Settings.selectedPaymentMode=1
        }
        if (item == "PayPal") {
            this.setState({ selectCard: false });
            this.setState({ border1: 1 }); this.setState({ border2: 3 }); this.setState({ border3: 1 })
            Settings.selectedPaymentMode=2
        }
        if (item == "installment") {
            this.setState({ selectCard: false });
            this.setState({ border1: 1 }); this.setState({ border2: 1 }); this.setState({ border3: 3 })
            Settings.selectedPaymentMode=3
        }


    }




    render() {
        return (
            <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 5 }}>

                <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                    <TouchableOpacity onPress={this.ClickPaymentMethodTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                        <View></View>
                        <Text style={{
                            fontSize: 18,
                            fontFamily: 'Helvetica',
                            textAlign: 'center',
                            alignSelf: 'center',
                            fontWeight: 'bold'
                        }}>PAYMENT METHOD
                        </Text>
                        <View style={{ justifyContent: 'center', alignItems: "center" }}>
                            {!this.state.PaymentMethodTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                }} />)}

                            {this.state.PaymentMethodTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                }} />
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                {this.state.PaymentMethodTab && (<View style={{ flexDirection: 'column' }}>
                    <View style={{ margin: 10, marginBottom: 5, borderColor: '#c6c6c6', borderWidth: this.state.border1, flexDirection: 'column' }}>


                        {this.state.border1 == 3 && (<View style={{ backgroundColor: '#f8f8f8', justifyContent: 'flex-end', alignItems: "flex-end", width: '100%' }}><Text style={{
                            width: 30,
                            height: 30,
                            borderRadius: 15,
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: "center",
                            fontFamily: 'Helvetica',
                            backgroundColor: 'green',
                            paddingTop: 5,
                            paddingLeft:2,
                        }}>
                            <Icon name="tick" size={14} color={"#ff"} style={{
                                textAlign: 'center', color: '#fff',
                                alignSelf: 'center'
                            }} /> </Text></View>)}




                        <View style={{ backgroundColor: '#f8f8f8', borderColor: '#c6c6c6', borderBottomWidth: 1, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.ClickPaymentMainItem.bind(this, "card")} style={{ width: '100%', flexDirection: 'row' }}>

                                <Image
                                    resizeMode="contain"
                                    source={require('../../assets/images/Card/CreditDebitCard.png')}
                                    style={styles.CardHead}
                                />

                                <Text style={{
                                    fontSize: 18,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                }}>Credit/Debit Card </Text>


                            </TouchableOpacity>
                        </View>
                        {this.state.selectCard && (<View>
                            <View style={{ height: 50, flexDirection: 'row' }}>
                                <View style={{ width: 110, flexDirection: 'row' }}>
                                    <Image
                                        resizeMode="contain"
                                        source={require('../../assets/images/Card/Visa.png')}
                                        style={styles.CardChiled}
                                    />

                                    <Image
                                        resizeMode="contain"
                                        source={require('../../assets/images/Card/MasterCard.png')}
                                        style={styles.CardChiled1}
                                    />
                                </View>
                                <Text style={{
                                    fontSize: 18,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'center',
                                    alignSelf: 'center',

                                }}>Visa/ MasterCard
                            </Text>
                            </View>
                            <View style={{ borderColor: '#c6c6c6', borderTopWidth: 1, height: 50, flexDirection: 'row' }}>
                                <View style={{ width: 110, flexDirection: 'row' }}>
                                    <Image
                                        resizeMode="contain"
                                        source={require('../../assets/images/Card/UnioNPay.png')}
                                        style={styles.CardChiled}
                                    />


                                </View>
                                <Text style={{
                                    fontSize: 18,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'center',
                                    alignSelf: 'center',

                                }}>UnionPay
                            </Text>
                            </View>
                        </View>)}

                    </View>



                    <View style={{ margin: 10, marginBottom: 5, borderColor: '#c6c6c6', borderWidth: this.state.border2, flexDirection: 'column' }}>
                        {this.state.border2 == 3 && (<View style={{ backgroundColor: '#f8f8f8', justifyContent: 'flex-end', alignItems: "flex-end", width: '100%' }}><Text style={{
                            width: 30,
                            height: 30,
                            borderRadius: 15,
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: "center",
                            fontFamily: 'Helvetica',
                            backgroundColor: 'green',
                            paddingTop: 5,
                            paddingLeft:2,
                        }}>
                            <Icon name="tick" size={14} color={"#ff"} style={{
                                textAlign: 'center', color: '#fff',
                                alignSelf: 'center'
                            }} /> </Text></View>)}
                        <TouchableOpacity onPress={this.ClickPaymentMainItem.bind(this, "PayPal")} style={{ width: '100%' }}>
                            <View style={{ backgroundColor: '#f8f8f8', flexDirection: 'row' }}>
                                <Image
                                    resizeMode="contain"
                                    source={require('../../assets/images/Card/PayPal.png')}
                                    style={styles.CardHeadPaypall}
                                />

                                <Text style={{
                                    fontSize: 18,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'center',
                                    alignSelf: 'center',

                                }}>Paypal
                            </Text>
                            </View>
                        </TouchableOpacity>

                    </View>





                    <View style={{ margin: 10, borderColor: '#c6c6c6', borderWidth: this.state.border3, flexDirection: 'column' }}>
                        {this.state.border3 == 3 && (<View style={{ backgroundColor: '#f8f8f8', justifyContent: 'flex-end', alignItems: "flex-end", width: '100%' }}><Text style={{
                            width: 30,
                            height: 30,
                            borderRadius: 15,
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: "center",
                            fontFamily: 'Helvetica',
                            backgroundColor: 'green',
                            paddingTop: 5,
                            paddingLeft:2,
                        }}>
                            <Icon name="tick" size={14} color={"#ff"} style={{
                                textAlign: 'center', color: '#fff',
                                alignSelf: 'center'
                            }} /> </Text></View>)}
                        <TouchableOpacity onPress={this.ClickPaymentMainItem.bind(this, "installment")} style={{ width: '100%' }}>
                            <View style={{ backgroundColor: '#f8f8f8', flexDirection: 'row' }}>
                                <Image
                                    resizeMode="contain"
                                    source={require('../../assets/images/Card/Installment.jpg')}
                                    style={styles.CardHead}
                                />

                                <Text style={{
                                    fontSize: 18,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'center',
                                    alignSelf: 'center',

                                }}>Instalment Plan
                            </Text>
                            </View>

                        </TouchableOpacity>
                    </View>




                </View>)}



            </View>
        )
    }

}
const styles = StyleSheet.create({

    CardHead: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,

        resizeMode: 'contain'
    },
    CardHeadPaypall: {

        overflow: 'visible',
        height: 60,
        width: 50,
        margin: 15,
        marginTop: 1,
        marginBottom: 1,
        resizeMode: 'contain'
    },
    CardChiled: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,
        marginLeft: 25,
        marginRight: 2,
        resizeMode: 'contain'
    },
    CardChiled1: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,
        marginLeft: 3,
        resizeMode: 'contain'
    },


});