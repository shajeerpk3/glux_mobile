import React from 'react';
import { View } from 'react-native';
import Bagitems from '../CheckOut/ItemSummery';
import ShippingDetails from '../CheckOut/ShippingDetails';
import BillingAddress from '../CheckOut/billingaddress';
import PaymentMethod from '../CheckOut/PaymentSelectedItem';



export default class CheckOut3 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
    }


    render() {



        return (

            <View style={{ margin: 4, flexDirection: 'column', }}>
                <ShippingDetails></ShippingDetails>
                <Bagitems></Bagitems>
                <BillingAddress></BillingAddress>
                <PaymentMethod></PaymentMethod>

            </View >
        )
    }
}