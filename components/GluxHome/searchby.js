import React from "react";
import { TouchableOpacity,StyleSheet, View, Dimensions } from "react-native";
import SearchInput from 'react-native-search-filter';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../src/selection.json';
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class searchBy extends React.Component {
    constructor(props) {
        super(props);
    }

    onPressLearnMore() {
        this.props.action3;
    }

    render() {
        return (
            <View style={styles.header}>
                {/* <SearchInput      
        onChangeText={(term) => {   this.props.action3();}} 
        style={styles.searchInput}
        placeholder="Search"
        /> */}
                <TouchableOpacity onPress={(term) => {this.props.action3(true);}}  style={styles.buttonContainer}>
                    <View style={styles.searchInput}>
                    <Icon2 name="Search" size={16} style={{ fontWeight: 5}} ></Icon2>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    searchInput: {
        padding: 5,
        borderColor: '#918e8e',
        borderWidth: 1,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 2,
        marginTop: 2,
        height: 30,
        borderRadius: 10,


        backgroundColor: '#ffffff',
    },
    header: {
        backgroundColor: '#fff',
    }


});