import React from "react";
import {Text, TouchableOpacity,StyleSheet, View, Dimensions } from "react-native";
import SearchInput from 'react-native-search-filter';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../src/selection.json';
import Settings from "../../constants/Settings";
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class search extends React.Component {
    constructor(props) {
        super(props);
    }

    onPressLearnMore() {
       Settings.DroverStatus=true
       Settings.nav.navigate("ShopallStack")
    }

    render() {
        return (
            <View style={styles.header}>
                {/* <SearchInput      
        onChangeText={(term) => {   this.props.action3();}} 
        style={styles.searchInput}
        placeholder="Search"
        /> */}
                <TouchableOpacity onPress={(term) => {this.onPressLearnMore();}}  style={styles.buttonContainer}>
                    <View style={styles.searchInput}>
                    <View style={{flexDirection:'row'}}>
                    <Icon2 name="Search" size={16} style={{ fontWeight: 5}} ></Icon2>
                    <Text style={{  
                            fontFamily: 'Helvetica', width: '100%',
                            paddingLeft:3,color:'gray'
                        }}>Search</Text>
                    </View>
                
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    searchInput: {
        padding: 5,
        borderColor: '#d8d8d8',
        borderWidth: 4,    
        height: 40,       
        backgroundColor: '#ffffff',
    }, 
    header: {
        backgroundColor: '#fff',
    }


});