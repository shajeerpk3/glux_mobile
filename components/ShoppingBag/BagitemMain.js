import React, { Component } from 'react';
import { ActivityIndicator, Picker, Alert, Image, Animated, TouchableOpacity, WebView, Text, View, Button, StyleSheet, ScrollView, Dimensions, Platform, TextInput } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import { Loading, EasyLoading } from 'react-native-easy-loading';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import DropdownAlert from 'react-native-dropdownalert';
import commonsettings from '../../constants/commonsettings';
import CheckBox from 'react-native-check-box';
import Settings from '../../constants/Settings';
import RNPickerSelect from 'react-native-picker-select';
import icoMoonConfig from '../../src/selection.json';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";

const halfwidth = Dimensions.get('window').width / 8;
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').height;
const { State: TextInputState } = TextInput;
let totalGpoint: number = 0;
let total: number = 0;
let total2: number = 0;
let subtotal: number = 0;
let netTotal: number = 0;
let shippingcharge: number = 0;
var radio_props = [
    { label: 'Use maximum Credit Wallet available SGD 500 ', value: 0 },
    { label: 'Choose a different amount', value: 1 }
];
var radio_props2 = [
    { label: 'Use maximum Credit Wallet available SGD 500 ', value: 0 },
    { label: 'Enter G-Points', value: 1 }
];
export default class AllproductPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            data: [],//for cart data
            isLoading: true,
            BagCount: false,
            favColor: '',
            total: 0,
            subtotal: 0,
            netTotal: 0,
            isChecked: false,
            creditvalet: 0,
            Gpoint: 0,
            counting: 0,
            PromoApplyStatus: false,
            PromoAmount: 0,
            GpointUsing: 0,
            Creditvalletusing: 0,
            shippingdata: Settings.ShippingCharge[0].value,
            shippingcharevisile: false,
            GpointWidth: 278,
            creditWalletWidth: 278,

            items: [
                {
                    label: '1',
                    value: '1',
                },
             
            ],


        };

        this.fillcart = this.fillcart.bind(this);
        shippingcharge = 0;
    }





    static navigationOptions = {
        header: null,
    };

    showdialog(showorhide) {
        this.setState({ showDialog: showorhide })
    }

    clickBack() {
        // navigation goBack
        Settings.nav.navigate(commonsettings.backButtonPage)
    }



    //******************************* **/update array on the basis of  quantity  change************************************************///////// 
    ValueChange(value, qty, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex) {
        //  total = 0;
        var newarry = [];
        if (varianceFlag === true) {
            var changeproduct = this.state.data[merchantIndex].products[ProductIndex].variants[varianceindex]

        }


        newarry = this.state.data.map((items, index) =>
            this.ProductMap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex))
        newarry = this.remove(newarry)
        this.setState({ data: newarry })
        Settings.data = newarry;


    }


    remove(array) {
        var result = [];

        array.forEach(function (item) {
            if (Array.isArray(item) && item.length != 0) {
                // Item is a nested array, go one level deeper recursively
                result.push(remove(item));
            }
            else if (typeof item !== 'undefined') {
                result.push(item);
            }
        });

        return result;
    };


    ProductMap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex) {
        var newProductarry = [];
        if (index !== merchantIndex) {
            // console.log("helloShajeer")
            newProductarry.push(items)
            return (newProductarry[0])
        }
        else {
            newProductarry.push(items);

            newProductarry[0].products.map((items, index) =>
                this.varuianceOrProductmap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex))
            newProductarry = this.remove(newProductarry)
            return (newProductarry[0])
        }

        //    console.log("jooooooooooo",newarry)

    }
    varuianceOrProductmap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex) {
        // console.log("itemmmm",items)
        var newVarianceArry = [];
        if (index !== ProductIndex) {
            // console.log("helloShajeer")
            newVarianceArry.push(items)
            return (newVarianceArry[0])
        }
        else {
            if (varianceFlag !== true) {
                items.qty = value
                newVarianceArry.push(items)

                return (newVarianceArry[0])
            }
            else {
                newVarianceArry.push(items);
                newVarianceArry[0].variants.map((items, index) =>
                    this.varinaceMap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex))
                newVarianceArry = this.remove(newVarianceArry)
                return (newVarianceArry[0])
            }

        }
    }

    varinaceMap(items, index, value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex) {
        var newVarianceArry = [];
        if (index !== varianceindex) {
            // console.log("helloShajeer")
            newVarianceArry.push(items)
            return (newVarianceArry[0])
        }
        else {

            items.qty = value
            newVarianceArry.push(items)

            return (newVarianceArry[0])
        }
    }


    DeleteCartItem(cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex) {


        Api.DeleteCartItem(
            cartid

        )


            .then((responseJson) => {

                // total=0
                // this.setState({netTotal:0})
                this.fillcart();



            })
            .catch((error) => {

                console.log(error.message)

            });
        //this.creditvaluecheckchange()
        // this.gpointcheckchange()
    }


    //********************************** */alert masg//************************************************* */

    Alert(item) {

        switch (item.type) {
            case 'close':
                this.forceClose();
                break;
            default:
                const random = Math.floor(Math.random() * 1000 + 1);
                const title = item.type + ' #' + random;
                this.dropdown.alertWithType(item.type, item.title, item.message);
        }
    }




    async componentDidMount() {

        commonsettings.backbuttonstatus = true;
        commonsettings.backButtonPage = 'ShopallStack';//set goback page to common settings
        Settings.SHippingChageValue = Settings.ShippingCharge[0].value;
        Settings.SHippingChageLabel = Settings.ShippingCharge[0].label;
        Settings.SHippingChageLabel1 = Settings.ShippingCharge[0].label1;
        //api for Cartitem
        if (Settings.fillingfrom == true) { this.fillcart(); }
        else {
            let counting: Number = 0;
            this.setState({
                data: Settings.data
            });
            Settings.data.map(function (product, i) {
                counting = counting + product.products.length;
            })

            this.setState({ GpointValue: (this.state.Gpoint * 0.01) })
            radio_props[0].label = 'Use maximum Credit Wallet available ' + Settings.currency + ' ' + this.state.creditvalet
            radio_props2[0].label = 'Use maximum G-Point available ' + Settings.currency + ' ' + this.state.GpointValue
            this.setState({ counting: counting })
            Settings.BagItemCOunt = counting
            if (counting == 0) {
                commonsettings.bagCount = counting; this.props.action1();
                this.setState({ BagCount: true })
            }


            this.setState(state => ({

                isLoading: false
            }));


            this.setState(state => ({

                PromoAmount: Settings.promocode

            }));

            this.setState(state => ({

                PromoApplyStatus: Settings.procodestatus

            }));


            this.setState({
                GpointStatus: !Settings.GpointStatus
            })

            this.setState({
                value: 0
            })

            this.setState({
                GpointUsing: Settings.gpoint
            })

            if (Settings.GpointStatus != true) {
                this.setState({
                    GpointWidth: 180
                })
            }

            // console.log("hihihnnnunununu1", Settings.data)
            // console.log("hihihnnnunununu", this.state.data);

        }


    }

    fillcart() {
        let counting: Number = 0;
        // total = 0
        Api.getGpoint()

            .then((responseJson) => {
                console.log("responseJson.message[0]",responseJson.message)
                 if(responseJson.message[0]==undefined || responseJson.message[0]==null || responseJson.message[0]==0){
                    this.setState({ Gpoint: 0 })
                 }
                 else{
                    this.setState({ Gpoint: responseJson.message[0].Gpts})
                 }
                Api.CartItem()

                    .then((responseJson) => {
                        // console.log("responseJson.cart",responseJson.cart)
                        this.setState(state => ({

                            data: responseJson.cart

                        }));
                        // console.log("kkkppppp", responseJson.cart)
                        Settings.data = responseJson.cart;

                        this.state.data.map(function (product, i) {
                            counting = counting + product.products.length;
                        })

                        this.setState({ GpointValue: (this.state.Gpoint * 0.01) })
                        radio_props[0].label = 'Use maximum Credit Wallet available ' + Settings.currency + ' ' + this.state.creditvalet
                        radio_props2[0].label = 'Use maximum G-Point available ' + Settings.currency + ' ' + this.state.GpointValue
                        this.setState({ counting: counting })
                        Settings.BagItemCOunt = counting;
                        if (counting == 0) {
                            commonsettings.bagCount = counting; this.props.action1();
                            this.setState({ BagCount: true })
                        }


                        this.setState(state => ({

                            isLoading: false
                        }));

                        // this.setState(state => ({

                        //     data1: this.state.data.filter((x,i) => i == 1 )
                        // }));



                    })
                    .catch((error) => {

                        console.log(error.message)
                        // this.bindmsg({ type: 'info', title: 'Stock', message: error.message });



                    });
            })
            .catch((error) => {

                console.log(error.message)
                // this.bindmsg({ type: 'info', title: 'Stock', message: error.message });



            });
    }

    //create page
    // renderEachitems(img,name,cartid,desc,qty,price ,stock,index) {
    renderEachitems(img, name, variant, cartid, desc, qty, price, index, stock, merchantIndex, varianceFlag, ProductIndex, varianceindex, gpoint, brand) {

        // console.log("price", price)

        total = total + price
        totalGpoint = totalGpoint + gpoint
        Settings.TotalGpointErn = totalGpoint
        Settings.ShoppingBagSubTotal = total
        return (

            < View key={index} style={{
                shadowColor: '#c6c6c6',
                borderColor: '#c6c6c6',
                borderWidth: 1,
                marginTop: 10,
                marginLeft: 10,
                marginRight: 10,
                paddingBottom: 10
                // borderTopLeftRadius: ,
                // borderTopRightRadius: 5,
                // borderBottomRightRadius: 1,
                // borderBottomLeftRadius: 1,
                // elevation: 2,

            }}>
                <View style={{ padding: 5, width: '100%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <TouchableOpacity style={{ width: '25%', flexDirection: 'row', justifyContent: 'flex-end' }} onPress={this.DeleteCartItem.bind(this, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)}>
                        <Icon name="cancel-1" color="#c6c6c6" size={18} style={{ fontWeight: 5, paddingRight: 5, paddingTop: 4 }} ></Icon>
                    </TouchableOpacity>

                </View>
                <View style={{ flexDirection: "row" }}>
                    <View style={{ width: 135 }}>

                        <Image style={{
                            height: 200,
                            margin: 7,
                            marginTop: 1,
                            alignItems: "center",

                        }} source={{ uri: img }} />
                        <View style={{ flexDirection: "column", padding: 5 }}>
                            <TouchableOpacity style={{ flexDirection: "row", margin: 7, }}>
                                {/* <Icon name="wishlist" size={15} style={{ textAlignVertical: 'center', marginRight: 4 }}> Wishlist</Icon> */}
                                {/* <View style={{ flexDirection: 'column', overflow: 'hidden' }}><Text>Move to Wishlist</Text> </View>*/}

                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flexDirection: "column", flex: 3, paddingLeft: 4, paddingTop: 50 }}>
                        <Text style={{

                            fontSize: 16,
                            fontFamily: 'Helvetica', width: '100%',
                            textAlign: 'left',
                            alignSelf: 'flex-start',
                            fontWeight: 'bold'


                        }}>
                            {brand.toUpperCase()}
                        </Text>
                        <Text style={{
                            marginTop: 10,
                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'left',
                            alignSelf: 'flex-start',
                            // fontWeight: 'bold'


                        }}>
                            {name}
                        </Text>

                        <Text style={{

                            fontSize: 16,
                            fontFamily: 'Helveticaregular', width: '100%',
                            textAlign: 'left',
                            alignSelf: 'flex-start',
                            marginTop: 8
                            // fontWeight: 'bold'


                        }}><Icon name="g-points" size={16} marginRight={5} style={{ fontWeight: 5, marginRight: 22 }} /> {gpoint}</Text>
                        <Text style={{

                            textAlign: 'left',
                            marginTop: 8, fontSize: 16,
                            fontFamily: 'Helvetica',
                            fontWeight: 'bold'

                        }} >{currencyFormatter.FormateCurrency(price)}</Text>
                        <View style={{ justifyContent: "flex-start", alignContent: "flex-start", alignItems: "flex-start", paddingTop: 10, paddingBottom: 5 }}>
                            <View style={{ justifyContent: "flex-start", alignContent: "flex-start", alignItems: "flex-start", borderWidth: 1, borderColor: '#c6c6c6', flexDirection: "row", paddingRight: 5 }}>
                                <View style={{
                                    justifyContent: "center", alignContent: "center", alignItems: "center",
                                    paddingTop: 8, paddingBottom: 5


                                }}>
                                    <Text style={{
                                        color: 'gray',
                                        fontFamily: 'Helvetica',

                                        fontWeight: 'bold',
                                        textAlign: "center",
                                        paddingRight: 3

                                    }}> QTY</Text>
                                </View>
                                <View style={{
                                    justifyContent: "center", alignContent: "center", alignItems: "center", ...Platform.select({
                                        ios: {
                                            paddingTop: 8, paddingBottom: 5
                                        },
                                        android: {
                                            paddingTop: 3,
                                        }

                                    }),
                                }}>
                                    <RNPickerSelect
                                        useNativeAndroidPickerStyle={false}
                                        hideIcon={true}
                                        value={qty}
                                        placeholder={{}}
                                        items={this.state.items}
                                        style={{ ...pickerSelectStyles }}
                                        underlineColorAndroid="transparent"
                                        onValueChange={(Selectvalue) => {
                                            // console.log("helloooo")
                                            this.ValueChange(Selectvalue, qty, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)
                                        }}

                                    // onValueChange= {(value)=>{this.ValueChange.bind(this,value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)}}
                                    >

                                    </RNPickerSelect>

                                </View>
                                <View style={{
                                    ...Platform.select({
                                        ios: {
                                            paddingTop: 10
                                        },
                                        android: {
                                            paddingTop: 12,
                                        }

                                    }),
                                }}>
                                    <Icon name="left-open" size={14} style={{ fontWeight: 10, transform: [{ rotate: '270deg' }] }} />
                                </View>



                            </View>
                        </View>

                    </View>

                </View>


            </View >
        )
    }


    //    second category item binding
    renderitemsandcheckvariance(merchant, items, index, merchantIndex) {

      


        let itemimg: string = items.product_id.brief.images[0];
        let productId: string = items.product_id._id;
        let discountrate: string = 0;
        let name: string = items.product_id.brief.name
        let brand: string = items.product_id.detail.product_brand

        let productindex: number = index

        try {

            discountrate = items.product_id.pricing.discount_rate

        }
        catch (error) {
            discountrate = 0

        }



        return (<View key={index}>{

            items.variants != undefined && (
                items.variants.map((variance, index) =>
                    // (this.renderEachitems("https://gstock.sg/buyer.gstock/uploads/user/" + merchant + "/products/" + items.product_id._id + "/" + items.product_id.brief.images[0], name, variance.variant_id.option_value, variance.cart_id, items.product_id.brief.short_description, variance.qty, (variance.variant_id.price * variance.qty) - ((variance.variant_id.price * variance.qty) * discountrate) / 100, index, items.product_id.brief.stock, merchantIndex, true, productindex, index, items.GLux_Pnts))
                    (this.renderEachitems("http://buyerservices.glux.sg/uploads/user/undefined/products/" + items.product_id._id + "/" + items.product_id.brief.images[0], name, variance.variant_id.option_value, variance.cart_id, items.product_id.brief.short_description, variance.qty, (variance.variant_id.price * variance.qty) - ((variance.variant_id.price * variance.qty) * discountrate) / 100, index, items.product_id.brief.stock, merchantIndex, true, productindex, index, items.GLux_Pnts, brand))
                )
            )
        }

            {

                items.variants == undefined && (this.renderEachitems("http://buyerservices.glux.sg/uploads/user/undefined/products/" + items.product_id._id + "/" + items.product_id.brief.images[0], name, '', items.cart_id, items.product_id.brief.short_description, items.qty, (items.product_id.pricing.discount_price2 * items.qty), index, items.product_id.brief.stock, merchantIndex, false, productindex, 0, items.GLux_Pnts, brand))
                // items.variants == undefined && (this.renderEachitems("https://gstock.sg/buyer.gstock/uploads/user/" + merchant + "/products/" + items.product_id._id + "/" + items.product_id.brief.images[0], name, '', items.cart_id, items.product_id.brief.short_description, items.qty, (items.product_id.pricing.discount_price * items.qty), index, items.product_id.brief.stock, merchantIndex, false, productindex, 0, items.GLux_Pnts))

            }</View>)


    }
    handlePromoTextChange = (text) => {
        this.setState({ nameError: "" })
        this.setState({ promocode: text.trim() })

    }
    handleInputChange = (text) => {

        this.setState({
            text: text.replace(/[^(((\d)+(\.)\d)|((\d)+))]/g, ''),
        });

        let usingamount: number = 0


        if (((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount > this.state.creditvalet) {
            usingamount = this.state.creditvalet
        }
        else {
            usingamount = ((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount
        }





        if (usingamount > text) {
            this.state.Creditvalletusing = text

        }
        else {
            this.setState({
                text: (this.state.Creditvalletusing).toString()
            });
        }




    }

    handleInputChange1 = (text) => {

        this.setState({
            text1: text.replace(/[^(((\d)+(\.)\d)|((\d)+))]/g, ''),
        });

        let usingamount: number = 0


        if (((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount > this.state.GpointValue) {
            usingamount = this.state.GpointValue
        }
        else {
            usingamount = ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount
        }



        let GpointtoSgd = text * 0.01

        if (usingamount > GpointtoSgd) {
            this.setState({ GpointUsing: GpointtoSgd })
            Settings.gpoint = GpointtoSgd;
        }
        else {
            this.setState({
                text1: (this.state.GpointUsing / 0.01).toString()
            });
        }




    }
    ApplayPromo() {

        if (this.state.promocode == "" || this.state.promocode == undefined || this.state.promocode == null) {
            this.setState({ nameError: "Enter Promocode" })
        }
        else {
            this.setState({ nameError: "" })


            Api.PromocodeVerify({ promo_code: this.state.promocode })
                .then((responseJson) => {
                    // console.log("hhh", responseJson.value)
                    this.setState(state => ({

                        PromoAmount: responseJson.value

                    }));
                    Settings.promocode = responseJson.value;
                    Settings.promocodecode = this.state.promocode
                    this.setState(state => ({
                        PromoApplyStatus: true
                    }));
                    Settings.procodestatus = true;
                    this.showdialog(false)

                })

                .catch((error) => {
                    this.setState(state => ({

                        PromoAmount: "0"

                    }));
                    Settings.procodestatus = false;
                    Settings.promocode = 0;
                    this.setState(state => ({

                        PromoApplyStatus: false

                    }));
                    this.setState({ nameError: error.message })
                    console.log(error.message)

                });


        }

    }
    // clickBack() {
    //     // props.navigation.goBack(commonsettings.backButtonPage)
    //     props.navigation.navigate('ShopallStack')
    // }
    setSubTotal() {
        total2 = total
        // this.setState({total:total2})
        // console.log("totallllllllllllllllll",total2)
    }

    renderitems(items, index) {
      
        let merchant: string = items.merchant._id
        let merchantIndex: number = index
        return (
            items.products.map((products, index) =>
                // console.log(items.merchant._id,products)
                this.renderitemsandcheckvariance(merchant, products, index, merchantIndex)
            )
        )
    }
    creditvaluecheckchange() {
        if (!this.state.creditvaletStatus) {
            this.setState({
                creditWalletWidth: 180
            })

            if (((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount > this.state.creditvalet) {
                this.state.Creditvalletusing = this.state.creditvalet
            }
            else {
                this.state.Creditvalletusing = ((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount
            }
        }
        else {

            this.setState({
                text: ''
            });
            this.state.Creditvalletusing = 0

            this.setState({
                creditWalletWidth: 278
            })
            this.setState({
                creditRadiostatus: false
            })
            if (((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount > this.state.GpointValue) {
                this.setState({ GpointUsing: this.state.GpointValue })
                Settings.gpoint = this.state.GpointValue;
                // this.state.GpointUsing = this.state.GpointValue
            }
            else {
                this.setState({ GpointUsing: ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount })
                Settings.gpoint = ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount;
                // this.state.GpointUsing = 
            }
        }
    }
    gpointcheckchange() {
        if (!this.state.GpointStatus) {
            this.setState({
                GpointWidth: 180
            })
            if (((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount > this.state.GpointValue) {

                this.setState({ GpointUsing: this.state.GpointValue })
                Settings.gpoint = this.state.GpointValue;
            }
            else {
                this.setState({ GpointUsing: ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount })
                Settings.gpoint = ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount;

            }
        }
        else {
            this.setState({
                text1: ''
            });
            this.setState({
                GpointUsing: 0
            })
            Settings.gpoint = 0
            this.setState({
                GpointWidth: 278
            })
            this.setState({
                GpointRadiostatus: false
            })
            if (((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount > this.state.creditvalet) {
                this.state.Creditvalletusing = this.state.creditvalet
            }
            else {
                this.state.Creditvalletusing = ((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount
            }
        }
    }




    render() {


        if (this.state.isLoading) {
            return (
                <View style={{ backgroundColor: '#fff', height: '100%', flexDirection: 'column', height: windowheight }}>

                    <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="gray" />
                    </View>
                </View>
            );

        }
        return (

            <View style={{ backgroundColor: '#fff', marginBottom: 10 }}>

                <Dialog
                    title="Enter Promos Code"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: '#fff'
                        }
                    }
                    onTouchOutside={() => this.showdialog(false)}
                    visible={this.state.showDialog}
                >
                    <View>
                        <View style={{ marginBottom: 4 }}>
                            {!!this.state.nameError && (
                                <Text style={{ color: 'red', marginBottom: 2 }}>{this.state.nameError}</Text>
                            )}
                            <TextInput

                                onChangeText={this.handlePromoTextChange}
                                value={this.state.promocode}
                                style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, width: 216 }}
                                underlineColorAndroid="transparent"
                            />

                        </View>
                        <View style={{ flexDirection: 'row', width: 220 }}>
                            <TouchableOpacity onPress={this.ApplayPromo.bind(this)} style={[s.buttonContainer1, { marginRight: 2 }]}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Apply </Text>
                            </TouchableOpacity >
                            <TouchableOpacity onPress={() => this.showdialog(false)} style={[s.buttonContainer1, { marginLeft: 2 }]}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Cancel </Text>
                            </TouchableOpacity >
                        </View>
                    </View>



                </Dialog>


                {!this.state.BagCount && (<View>
                    <View style={{ paddingBottom: 5, borderWidth: 1, borderColor: '#c6c6c6', margin: 5 }}>
                        <View style={{ borderBottomWidth: 1, borderColor: '#c6c6c6', height: 60, alignContent: 'center', alignItems: 'center', padding: 5, paddingRight: 10 }}>
                            <Icon name="glux-logo" size={45} style={{ fontWeight: 5 }} ></Icon>
                        </View>
                        {/* binding each item */}
                        <View >
                            {total = 0,totalGpoint=0,
                            this.state.data.map((items, index) =>
                                this.renderitems(items, index)
                            )}

                            </View>
                        <View style={{ flexDirection: "column" }}>
                            <View style={{ margin: 10, marginBottom: 0, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between" }}>
                                <TouchableOpacity onPress={() => this.showdialog(true)}>
                                    <Text style={{ fontSize: 15.5, fontFamily: 'Helvetica', color: '#d87136', textDecorationLine: "underline", fontWeight: 'bold', margin: 7 }}>Promos Codes </Text>
                                </TouchableOpacity>
                                {this.state.PromoApplyStatus && (<View>
                                    <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', color: 'green', margin: 7 }}><Icon name="check" size={18} style={{ fontWeight: 5 }} ></Icon> {currencyFormatter.FormateCurrency(this.state.PromoAmount)} </Text>
                                </View>)}

                            </View>
                            {this.state.shippingcharevisile && (<View style={{ marginLeft: 10, marginTop: 3, marginRight: 12, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between" }}>
                                {/* {/* <View> <Text style={{ fontSize: 18, fontFamily: 'Helvetica', fontWeight: 'bold', margin: 7 }}>SHIPPING FEE  </Text></View> */}
                                {/* <View> <Text style={{ fontSize: 18, fontFamily: 'Helvetica', fontWeight: 'bold', margin: 7 }}>SHIPPING FEE  </Text></View> */}
                                <View style={{ flexDirection: "column", width: 220 }}>
                                    <Text style={{ fontSize: 14.5, fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 7, }}>SHIPPING FEE  </Text>
                                    <Text style={{ marginLeft: 7, color: 'gray' }}>{this.state.shippingdata}</Text>
                                </View>
                                <View style={{ borderWidth: 1, borderColor: '#c6c6c6', marginRight: 1, alignContent: "center", alignItems: 'center', justifyContent: 'center' }}>
                                    <RNPickerSelect
                                        useNativeAndroidPickerStyle={false}
                                        hideIcon={true}
                                        placeholder={{}}
                                        items={Settings.ShippingCharge}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.setState.shippingdata}
                                        underline={"transparent"}
                                        underlineColorAndroid="transparent"
                                        onValueChange={(Selectvalue, index) => {
                                            // console.log("helloooo")
                                            total = 0
                                            shippingcharge = Settings.ShippingCharge[index].cash
                                            this.setState({ shippingcharge: Settings.ShippingCharge[index].cash })
                                            this.setState({ shippingdata: Selectvalue })
                                            Settings.SHippingChageValue = Settings.ShippingCharge[index].value;
                                            Settings.SHippingChageLabel = Settings.ShippingCharge[index].label;
                                            Settings.SHippingChageLabel1 = Settings.ShippingCharge[index].label1;


                                        }}
                                    // onValueChange= {(value)=>{this.ValueChange.bind(this,value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)}}
                                    >
                                    </RNPickerSelect>

                                </View>

                            </View>)}
                            <View style={{ marginLeft: 10, marginTop: 12, flexDirection: "row", flex: 1, marginRight: 15, alignContent: "space-between", justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold', marginLeft: 4 }}> SUBTOTAL  </Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}> {currencyFormatter.FormateCurrency((total + shippingcharge) - this.state.PromoAmount)} </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginLeft: 5, marginRight: 8, justifyContent: 'space-between', alignContent: "space-between" }}>
                        {(2 + 1) == 0 && (<View style={{ margin: 10, marginTop: 3, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between" }}>
                            <View style={{ paddingLeft: 7 }}>
                                <CheckBox

                                    onClick={() => {
                                        this.setState({
                                            creditvaletStatus: !this.state.creditvaletStatus
                                        })
                                        this.creditvaluecheckchange();
                                    }}
                                    Animated={true}
                                    isChecked={this.state.creditvaletStatus}
                                    checkBoxColor={'gray'}
                                />
                            </View>
                            <View style={{ flexDirection: "column", width: this.state.creditWalletWidth }}>
                                <Text style={{ fontSize: 16, fontFamily: 'Helveticaregular' }}>Use Credit Wallet <Text style={{ fontSize: 15, fontFamily: 'Helveticaregular', fontWeight: 'bold' }}>({Settings.currency} {this.state.creditvalet})</Text> </Text>

                            </View>
                            {this.state.creditvaletStatus && (<View style={{ flexDirection: "column", marginRight: 6 }}>

                                <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}>-{currencyFormatter.FormateCurrency(this.state.Creditvalletusing)} </Text>
                            </View>)}
                        </View>)}



                        {this.state.creditvaletStatus && (<View style={{ marginLeft: 46, marginRight: 38, margin: 10, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between", }}>

                            <View style={{ flexDirection: "column" }}>
                                <View style={{ flexDirection: "column", width: '100%' }}>
                                    <RadioForm
                                        radio_props={radio_props}
                                        initial={0}
                                        buttonColor={'#77797c'}
                                        buttonSize={7}
                                        buttonOuterSize={17}
                                        buttonWrapStyle={{ color: '#77797c' }}

                                        onPress={(value) => {
                                            this.setState({ value: value })
                                            if (value) {
                                                this.state.Creditvalletusing = 0
                                                this.setState({
                                                    creditRadiostatus: true

                                                })
                                            }
                                            else {

                                                if (((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount > this.state.creditvalet) {
                                                    this.state.Creditvalletusing = this.state.creditvalet
                                                }
                                                else {
                                                    this.state.Creditvalletusing = ((total + shippingcharge) - this.state.GpointUsing) - this.state.PromoAmount
                                                }
                                                this.setState({
                                                    creditRadiostatus: false
                                                })
                                            }
                                        }}
                                        selectedButtonColor={'#77797c'}
                                    />
                                </View>
                                {(this.state.creditRadiostatus && <View style={{ flexDirection: "row", marginTop: 2, padding: 2, width: '100%' }}>
                                    <Text style={{ fontSize: 15, fontFamily: 'Helveticaregular', paddingTop: 5 }}>{Settings.currency} </Text>
                                    <TextInput
                                        keyboardType='numeric'
                                        onChangeText={this.handleInputChange}
                                        value={this.state.text}
                                        style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, width: 80 }}
                                        underlineColorAndroid="transparent"
                                    />
                                </View>)}
                            </View>
                        </View>)}












                        <View style={{ margin: 10, marginTop: 20, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between" }}>
                            <View style={{ paddingLeft: 7 }}>
                                <CheckBox

                                    onClick={() => {
                                        this.setState({
                                            GpointStatus: !this.state.GpointStatus
                                        })
                                        Settings.GpointStatus = this.state.GpointStatus;
                                        this.gpointcheckchange();
                                    }}
                                    Animated={true}
                                    isChecked={this.state.GpointStatus}
                                    checkBoxColor={'gray'}
                                />
                            </View>
                            <View style={{ flexDirection: "column", width: this.state.GpointWidth }}>
                                <Text style={{ fontSize: 16, fontFamily: 'Helveticaregular', }}>Use G-Points (<Icon name="g-points" size={15} marginRight={5} style={{ fontWeight: 5, marginRight: 22 }} /><Text style={{ fontSize: 15, fontFamily: 'Helvetica', }}> {this.state.Gpoint}</Text> points available) </Text>

                            </View>
                            {this.state.GpointStatus && (<View style={{ flexDirection: "column", marginRight: 7 }}>

                                <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}>-{currencyFormatter.FormateCurrency(this.state.GpointUsing)} </Text>
                            </View>)}
                        </View>



                        {this.state.GpointStatus && (<View style={{ marginLeft: 46, marginRight: 38, margin: 10, marginTop: 3, flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between", }}>

                            <View style={{ flexDirection: "column" }}>
                                <View style={{ flexDirection: "column", marginTop: 2, padding: 2, width: '100%', }}>
                                    <RadioForm
                                        radio_props={radio_props2}
                                        initial={0}
                                        buttonColor={'#77797c'}
                                        buttonSize={7}
                                        buttonOuterSize={17}
                                        buttonWrapStyle={{ color: '#77797c' }}

                                        onPress={(value) => {
                                            this.setState({ GpointUsing: 0 })
                                            Settings.gpoint = 0
                                            this.setState({ value: value })

                                            if (value) {
                                                this.setState({
                                                    GpointRadiostatus: true
                                                })
                                                Settings.gpointradiostatus = true;
                                            }
                                            else {
                                                Settings.gpointradiostatus = false;
                                                if (((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount > this.state.GpointValue) {
                                                    this.setState({ GpointUsing: this.state.GpointValue })
                                                }
                                                else {
                                                    this.setState({ GpointUsing: ((total + shippingcharge) - this.state.Creditvalletusing) - this.state.PromoAmount })

                                                }
                                                this.setState({
                                                    GpointRadiostatus: false
                                                })
                                            }

                                        }}
                                        selectedButtonColor={'#77797c'}

                                    />
                                </View>
                                {this.state.GpointRadiostatus && (<View style={{ flexDirection: "row", marginTop: 2, padding: 2, width: '100%' }}>
                                    <Text style={{ fontSize: 15, fontFamily: 'Helveticaregular', paddingTop: 5 }}><Icon name="g-points" size={15} marginRight={5} style={{ fontWeight: 5, marginRight: 22 }} /> </Text>
                                    <TextInput
                                        keyboardType='numeric'
                                        onChangeText={this.handleInputChange1}
                                        value={this.state.text1}
                                        style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, width: 80 }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <Text style={{ fontSize: 15, fontFamily: 'Helvetica', paddingTop: 5 }}> ( {currencyFormatter.FormateCurrency(this.state.GpointUsing)} ) </Text>
                                </View>)}


                            </View>
                        </View>)}
                        <View style={{ margin: 5 }}>
                            <View style={{ margin: 10, marginTop: 3, flexDirection: "column", justifyContent: 'space-between', alignContent: "space-between" }}>
                                <View style={{ flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between", }}>
                                    <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold', width: windowwidth / 3 }}>TOTAL G-POINTS EARNED   </Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Helvetica', fontWeight: 'bold' }}><Icon name="g-points" size={15} marginRight={5} style={{ fontWeight: 5 }} /> {totalGpoint} </Text>
                                </View>
                                <View style={{ flexDirection: "row", justifyContent: 'space-between', alignContent: "space-between", paddingTop: 10 }}>
                                    <Text style={{ fontSize: 17, fontFamily: 'Helvetica', fontWeight: 'bold', width: windowwidth / 3 }}>TOTAL <Text style={{ fontSize: 17, fontFamily: 'Helveticaregular', fontWeight: 'normal' }}>({this.state.counting} items)</Text> </Text>
                                    <Text style={{ fontSize: 17, fontFamily: 'Helvetica', fontWeight: 'bold' }}> {currencyFormatter.FormateCurrency(((((total + shippingcharge) - this.state.Creditvalletusing) - this.state.GpointUsing) - this.state.PromoAmount))} </Text>
                                </View>
                                <View style={{ flexDirection: "column", justifyContent: 'flex-start', alignContent: "flex-start", paddingTop: 10 }}>
                                    <Text style={{ fontSize: 13, fontFamily: 'Helveticaregular', color: '#babbbc' }}>G-Points will be added into your account at the end of the return period of your purchase  </Text>

                                </View>
                            </View>
                        </View>
                    </View>

                </View>)}

                {this.state.BagCount && (<View style={{ height: windowheight, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Icon2 name="Shopping-bag-empty" size={70} style={{ fontWeight: 5 }} ></Icon2>
                        <Text style={{
                            fontSize: 20,
                            fontFamily: 'Helveticaregular',
                            marginTop: 5,
                            textAlign: "center",
                            paddingRight: 3
                        }}> Your bag is empty</Text>
                    </View>
                </View>)}
            </View>

        )
    }
}


const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {

        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        backgroundColor: 'red'
    }



});