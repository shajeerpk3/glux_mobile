import React from "react";
import settingss from '../../constants/Settings';

const servername = settingss.serverUrl1;
const LocalHost = settingss.loacalServer;

const endUrl = {
    getGpoint:servername + "/gpoint/GPoints",
    Itemdetails: servername + "/product/product",
    AddToCart: servername + "/buyer/cart",
    PromocodeVerify:servername + "/product/promo/verify"
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params, authToken,lastaddid=null) {
   
    let bodyIsJson = true;
    try {
        JSON.parse(body);
        // if(body==null)
        // {
        //     bodyIsJson = false;
        // }
    } catch (e) {
        bodyIsJson = false;
      //  console.log("mindulaaa",bodyIsJson)
    }

   // console.log("mindano")

    let fullUrl:string = endpoint + constructParams(params);

    // if(lastaddid!=null){
        fullUrl=fullUrl + DeleteParam(lastaddid);
    // }
   console.log("fullUrl",fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    //console.log("hello",fetchOptions)
    // if (type == "op") {
    //     console.log("fullUrl")
    //     return fetch(fullUrl, fetchOptions)
    //         .then((response) => console.log("rerererer",response))

    // }
    // if (type != "op") {

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }
  
    return ("/"+ params)
}


function AddToCart(ProductId, merchant_id, userid,GLux_Pnts) {
// console.log("GLux_Pnts:",GLux_Pnts)
    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        product_id: ProductId,
        merchant_id: merchant_id,
        // merchant_id: null,
        qty: userid,
        GLux_Pnts:GLux_Pnts
    });

    return callApi(endUrl.AddToCart, "POST", body, null, settingss.authorization)
}


function CartItem() {


    return callApi(endUrl.AddToCart, "GET", null, null, settingss.authorization)
}

function getGpoint() {


    return callApi(endUrl.getGpoint +"?user_id=" + settingss.userid, "GET", null, null, settingss.authorization)
}


function PromocodeVerify(param) {


    return callApi(endUrl.PromocodeVerify, "GET", null, param, settingss.authorization)
}


function DeleteCartItem(params) {


   return callApi(endUrl.AddToCart, "DELETE", null, null, settingss.authorization,params)
}

function Itemdetails(params) {

    return callApi(endUrl.Itemdetails, "GET", null, params);
}



export default {
    Itemdetails,
    AddToCart,
    CartItem,
    DeleteCartItem,
    PromocodeVerify,
    getGpoint
};