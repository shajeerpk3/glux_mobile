import React from 'react';
import { TextInput, Alert, Animated, WebView, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api2 from "../../utils/Api/AccountDetails";
import commonsettings from '../../constants/commonsettings';
import icoMoonConfig from '../../src/selection.json';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Settings from '../../constants/Settings';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import RNPickerSelect from 'react-native-picker-select';


const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;

export default class ShippingDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showDialog: false,
            dataSource: [],
            shippingTabDelivery: false,
            shippingTab: true,
            ItemSummeryTab: false,
            // height: new Animated.Value(50),
            ShippingMethod: '',
            heightShippingDelivery: new Animated.Value(50),
            address: '',
            addresssate: false,
            items: [
                {
                    label: 'Anytime',
                    value: 'Anytime',
                }, {
                    label: '10 AM - 12 PM',
                    value: '10 AM - 12 PM',
                }, {
                    label: '1 PM - 3 PM',
                    value: '1 PM - 3 PM',
                }, {
                    label: '3 PM - 5 PM',
                    value: '3 PM - 5 PM',
                }, {
                    label: '5 PM - 7 PM',
                    value: '5 PM - 7 PM',
                }, {
                    label: '7 PM - 9 PM',
                    value: '7 PM - 9 PM',
                },
            ]
        };


    }

    ClickShippingTabMethod() {
        this.showdialog(true)
    }

    ClickShippingTabAddress() {
        Settings.nav.navigate("CheckOutAddress")
    }
    ClickShippingTabDelivery() {
        var tovalue = 50;
        if (this.state.shippingTabDelivery == false) {
            tovalue = 150
            this.setState({ shippingTabDelivery: true })
        } else {
            tovalue = 50
            this.setState({ shippingTabDelivery: false })
        }

        Animated.timing(                  // Animate over time
            this.state.heightShippingDelivery,            // The animated value to drive
            {
                toValue: tovalue,                   // Animate to opacity: 1 (opaque)
                duration: 450,              // Make it take a while
            }
        ).start();
    }
    ClickShippingTab() {


        if (this.state.shippingTab == false) {

            this.setState({ ItemSummeryTab: false })
            this.setState({ shippingTab: true })
        } else {
            this.setState({ ItemSummeryTab: true })
            this.setState({ shippingTab: false })
        }


    }

    showdialog(showorhide) {
        this.setState({ showDialog: showorhide })
    }


    handleShippingMethodTextChange = (text) => {

        this.setState({ ShippingMethod: text });
        Settings.PaymentMethod = text;
    }


    async componentDidMount() {

        Api2.GetBillingAndShippingAddress()
            .then((responseJson) => {
                this.setState({ address: responseJson });
                if (this.state.address[0].S_Fullname != undefined && this.state.address[0].S_Fullname != '' && this.state.address[0].S_Fullname != null) {
                    this.setState({ addresssate: true })
                    Settings.ShippingAddressExistance = true
                    Settings.shippingaddress = responseJson
                }
                else {
                    this.setState({ addresssate: false })
                    Settings.ShippingAddressExistance = false
                    Settings.shippingaddress = ""
                }

            })
            .catch((error) => {
                console.log(error.message)
            });
    }


    // }
    render() {
        return (
            <View>
                <Dialog
                    title="Enter Delivery Instruction"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: '#fff'
                        }
                    }
                    onTouchOutside={() => this.showdialog(false)}
                    visible={this.state.showDialog}
                >
                    <View>
                        <View style={{ marginBottom: 4 }}>

                            <TextInput
                                multiline={true}
                                value={this.state.ShippingMethod}
                                style={{ borderWidth: 1, borderColor: 'gray', padding: 3, paddingLeft: 6, width: 216, height: 100 }}
                                underlineColorAndroid="transparent"
                                onChangeText={this.handleShippingMethodTextChange}
                            />

                        </View>
                        <View style={{ flexDirection: 'row', width: 220 }}>
                            <TouchableOpacity onPress={() => this.showdialog(false)} style={[s.buttonContainer1, { marginRight: 2 }]}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Apply </Text>
                            </TouchableOpacity >
                            <TouchableOpacity onPress={() => this.showdialog(false)} style={[s.buttonContainer1, { marginLeft: 2 }]}>
                                <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold', margin: 2 }}>Close </Text>
                            </TouchableOpacity >
                        </View>
                    </View>



                </Dialog>


                <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20 }}>

                    <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                        <TouchableOpacity onPress={this.ClickShippingTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                            <View></View>
                            <Text style={{
                                fontSize: 18,
                                fontFamily: 'Helvetica',
                                textAlign: 'center',
                                alignSelf: 'center',
                                fontWeight: 'bold'
                            }}>SHIPPING DETAILS
                        </Text>
                            <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                {!this.state.shippingTab && (
                                    <Icon name="left-open" size={22} style={{
                                        marginRight: 15, textAlign: 'center',
                                        alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                    }} />)}

                                {this.state.shippingTab && (
                                    <Icon name="left-open" size={22} style={{
                                        marginRight: 15, textAlign: 'center',
                                        alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                    }} />
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>



                    {this.state.shippingTab && (<View>


                        <View style={{ margin: 5, marginBottom: 2, marginTop: 10 }}>

                            <View><Text style={{
                                fontSize: 17,
                                fontFamily: 'Helvetica',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}> Shipping Address</Text></View>

                        </View>
                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                            <View style={{ flexDirection: 'column', padding: 4, paddingLeft: 8, width: '78%' }}>
                                {!this.state.addresssate && (<Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>You do not currently have a shipping address set up on your account</Text>)}
                                {this.state.addresssate && (<View>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>{this.state.address[0].S_Fullname}</Text>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>{this.state.address[0].S_Address1}</Text>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>{this.state.address[0].S_Address2}</Text>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>{this.state.address[0].S_Address3}</Text>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>SINGAPORE ({this.state.address[0].S_Postalcode})</Text>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4
                                    }}>{this.state.address[0].S_ContactNo}</Text>
                                </View>)}


                            </View>

                            <View>
                                <TouchableOpacity onPress={this.ClickShippingTabAddress.bind(this)} style={{ alignItems: "center", width: 50 }}>
                                    <Icon name="left-open" size={22} style={{ marginRight: 15, transform: [{ rotate: '180deg' }] }} />
                                </TouchableOpacity>
                            </View>
                        </View>









                        <View style={{ margin: 5, marginBottom: 2, marginTop: 10 }}>

                            <View><Text style={{
                                fontSize: 17,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}> Delivery Method
                            </Text></View>

                        </View>

                        <Animated.View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, height: this.state.heightShippingDelivery }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <View style={{ flexDirection: 'column' }}><Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4,


                                }}>  <Icon name="glux-logo" color={"black"} size={30} style={{ marginRight: 15, marginBottom: 25, transform: [{ rotate: '270deg' }] }} /></Text></View>
                                <View style={{ flexDirection: 'column', width: '55%' }}>
                                    {!this.state.shippingTabDelivery && (<Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'left',
                                        alignSelf: 'flex-start',
                                        padding: 4,


                                    }}> Free Delivery
                            </Text>)}
                                </View>
                                <View style={{ flexDirection: 'column' }}>
                                    <TouchableOpacity onPress={this.ClickShippingTabDelivery.bind(this)} style={{ alignItems: "center", width: 50 }}>
                                        {!this.state.shippingTabDelivery && (<Icon name="left-open" size={22} style={{ marginRight: 15, transform: [{ rotate: '180deg' }] }} />)}
                                        {this.state.shippingTabDelivery && (<Icon name="left-open" size={22} style={{ marginRight: 15, transform: [{ rotate: '270deg' }] }} />)}
                                    </TouchableOpacity></View>
                            </View>
                            {this.state.shippingTabDelivery && (<View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 8 }}>
                                <View style={{ flexDirection: 'column' }}>
                                    <View style={{ flexDirection: 'column', }}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontFamily: 'Helveticaregular',
                                            marginLeft: 9,
                                            padding: 4,
                                            paddingBottom: 1,

                                            fontWeight: 'bold'

                                        }}>{Settings.SHippingChageLabel1}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', width: 140 }}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontFamily: 'Helveticaregular',
                                            marginLeft: 9,
                                            padding: 4,
                                            paddingTop: 1,

                                            color: 'gray'

                                        }}>({Settings.SHippingChageValue})</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'column' }}>
                                    <Text style={{
                                        fontSize: 16,
                                        fontFamily: 'Helveticaregular',
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        padding: 4,

                                        fontWeight: 'bold'
                                    }}>{Settings.SHippingChageLabel}</Text>
                                </View>
                            </View>)}

                        </Animated.View>











                        <View style={{ margin: 5, marginBottom: 2, marginTop: 10 }}>

                            <View><Text style={{
                                fontSize: 17,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}> Shipping Instruction </Text></View>

                        </View>
                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'column' }}><Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                padding: 4,

                            }}>  <Icon name="glux-logo" color={"black"} size={30} style={{ marginRight: 15, marginBottom: 25, transform: [{ rotate: '270deg' }] }} /></Text></View>
                            <View style={{ margin: 5, flexDirection: 'column', width: '55%' }}>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4,

                                }}>{Settings.PaymentMethod}

                                </Text>
                            </View>
                            <TouchableOpacity onPress={this.ClickShippingTabMethod.bind(this)} style={{ justifyContent: "flex-start", alignItems: "center", width: 50 }}>
                                <Icon name="left-open" size={22} style={{ marginRight: 15, transform: [{ rotate: '180deg' }] }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ margin: 5, marginBottom: 2, marginTop: 10 }}>

                            <View><Text style={{
                                fontSize: 17,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                fontWeight: 'bold'
                            }}> Delivery Timeslots (Week days) </Text></View>

                        </View>
                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <RNPickerSelect
                                useNativeAndroidPickerStyle={false}
                                hideIcon={true}
                                placeholder={{}}
                                items={this.state.items}
                                style={{ ...pickerSelectStyles }}
                                underlineColorAndroid="transparent"
                                onValueChange={(Selectvalue) => {
                                   Settings.DeliveryTime=Selectvalue;
                                
                                }}

                            // onValueChange= {(value)=>{this.ValueChange.bind(this,value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)}}
                            >

                            </RNPickerSelect>
                        </View>
                    </View>)}
                </View>
            </View>


        )
    }


}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        paddingTop:10,
        paddingBottom:10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {
        paddingTop:10,
        paddingBottom:10,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        width:'100%'
    }



});