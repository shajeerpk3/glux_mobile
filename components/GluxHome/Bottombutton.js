import * as React from 'react';
import { Text,View, TouchableOpacity, StyleSheet,Dimensions } from 'react-native';
import Settings from "../../constants/Settings"
const left = Dimensions.get('window').width/2;
export default class BottomButton extends React.Component {

    onPressLearnMore(type) {

       Settings.category_filter=type
       Settings.nav.navigate("ShopallStack")

    }
    
    render() {
        
        return (

            <View  key="MainSelectionContaier" style={{flexDirection: 'row',paddingBottom:40}}>
            {/* <View  key="WomenSelectionContaier" style={styles.selectionstyle2 }   >
            </View>
            <View  key="MenSelectionContaier" style={styles.selectionstyle1 } >
            </View> */}
             <View style={styles.container}>
                    <TouchableOpacity onPress={this.onPressLearnMore.bind(this,",Women")} style={styles.buttonContainer}>
                        <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc',fontSize:16 }}>WOMEN</Text>

                    </TouchableOpacity >
                </View>
                <View style={styles.container}>
                    <TouchableOpacity onPress={this.onPressLearnMore.bind(this,",Men")} style={styles.buttonContainer}>
                        <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc' ,fontSize:16}}>MEN</Text>

                    </TouchableOpacity >
                </View>
          </View>
        );
      }
}
const styles = StyleSheet.create({
  
      buttonContainer: {
        backgroundColor: 'black',
      
        padding:12,
      
        borderWidth: 1, borderColor: '#414142',
      
        width: left,
        alignItems: "center"
    }, container: {
        backgroundColor: '#e2e2e2',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});

