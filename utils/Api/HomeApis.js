import React from "react";
import settingss from '../../constants/Settings';

const servername = settingss.serverUrl;
const servername1 = settingss.serverUrl1;
const endUrl = {
    TopWomenItems: servername + "/api",
    TopMenItems: servername + "/api",
    SpotlightHome: servername + "/api",
    AllProductSearchingada:servername1 + '/product/list'
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
    } catch (e) {
        bodyIsJson = false;
    }

    const fullUrl = endpoint + constructParams(params);
 
    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...contentTypeHeader,
        },
        body: body,
    };

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {
           
          
            if (!response.ok) {
                
                return Promise.reject(json);
            }
          
            return json;
        });

}

function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}



function getTopWomenItems(params) {
    return callApi(endUrl.TopWomenItems, "GET", null, params);
}

function getTopMenItems(params) {
    return callApi(endUrl.TopMenItems, "GET", null, params);
}
function SpotlightHome(params) {
    return callApi(endUrl.TopMenItems, "GET", null, params);
}
function AllProductSearchingada(params) {
    
    return callApi(endUrl.AllProductSearchingada, "GET", null, params);
}



export default {
    getTopWomenItems,
    getTopMenItems,
    SpotlightHome,
    AllProductSearchingada
};