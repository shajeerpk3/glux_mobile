import React from "react";
import settingss from '../../constants/Settings';
const servername = settingss.serverUrl1;
const endUrl = {
    ShippingAddress: servername + "/buyer/shipto",
    BillingAddress: servername + "/buyer/billto",
    GetBillingAndShippingAddress:servername + "/buyer/Delivery",
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}



function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
        // if(body==null)
        // {
        //     bodyIsJson = false;
        // }
    } catch (e) {
        bodyIsJson = false;
        //  console.log("mindulaaa",bodyIsJson)
    }

    // console.log("mindano")

    let fullUrl: string = endpoint + constructParams(params);

    // if(lastaddid!=null){
    fullUrl = fullUrl + DeleteParam(lastaddid);
    // }

    // console.log("fullUrl", fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    //console.log("hello",fetchOptions)
    // if (type == "op") {
    //     console.log("fullUrl")
    //     return fetch(fullUrl, fetchOptions)
    //         .then((response) => console.log("rerererer",response))

    // }
    // if (type != "op") {

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }

    return ("/" + params)
}




function ShippingAddress(Fullname, Address1, Address2, Address3, Country, Postalcode, ContactNo) {

    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        S_Fullname: Fullname,
        S_Address1: Address1,
        S_Address2: Address2,
        S_Address3: Address3,
        S_Country: Country,
        S_Postalcode: Postalcode,
        S_ContactNo: ContactNo
    });

    return callApi(endUrl.ShippingAddress, "POST", body, null, settingss.authorization)
}




function BillingAddress(Fullname, Address1, Address2, Address3, Country, Postalcode, ContactNo) {

    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        B_Fullname: Fullname,
        B_Address1: Address1,
        B_Address2: Address2,
        B_Address3: Address3,
        B_Country: Country,
        B_Postalcode: Postalcode,
        B_ContactNo: ContactNo
    });

    return callApi(endUrl.BillingAddress, "POST", body, null, settingss.authorization)
}



function GetBillingAndShippingAddress() {

    return callApi(endUrl.GetBillingAndShippingAddress, "GET", null, null, settingss.authorization)
}

export default {
    ShippingAddress,
    BillingAddress,
    GetBillingAndShippingAddress
};