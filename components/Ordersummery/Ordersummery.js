import React, { Component } from 'react';
import { ActivityIndicator, Picker, Alert, Image, Animated, TouchableOpacity, WebView, Text, View, Button, StyleSheet, ScrollView, Dimensions, Platform, TextInput } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
const halfwidth = Dimensions.get('window').width / 8;
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import commonsettings from '../../constants/commonsettings';
import Settings from '../../constants/Settings';
import Api from "../../utils/Api/ordersummery";
import currencyFormatter from '../../constants/currncy_DateFormate';
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').height;


export default class OrderSummery extends Component {

    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {

            loading: true,
            dataSource: [],

        };



    }

    CheckOUt() {
        Settings.nav.navigate("ShopallStack")
    }
    clickBack() {
        Settings.nav.navigate("AccountsStack")
    }

    async componentWillMount() {

        this.webCall();

    }
    clickfulldtailsbind(product, BillingInfo, DeliveryInfo, Id, DateString, status) {

        Settings.nav.navigate("OrderSummeryDetails", {
            product: product, BillingInfo: BillingInfo, DeliveryInfo: DeliveryInfo, Id: Id, DateString: DateString, status: status
        })
    }
    webCall = () => {
        console.log("userid",Settings.userid)
        Api.getordersummery(Settings.userid)
            .then((responseJson) => {

                this.setState({

                    dataSource: responseJson
                });
                this.setState({
                    loading: false,
                });

            })
            .catch((error) => {
                console.error(error);
            });

    }
    
    render1(item, index) {
        var date = new Date(item.createdAt).getDate()
        var month = new Date(item.createdAt).getMonth() + 1; //Current Month
        var year = new Date(item.createdAt).getFullYear()
        month = commonsettings.monthNames[month]
        return (
            <View key={index} style={{ margin: 10, borderWidth: 1, borderColor: '#414142' }}>
                <TouchableOpacity onPress={this.clickfulldtailsbind.bind(this, item.products, item.BillingInfo, item.delivery, item._id, month + " " + date + ',' + year, item.status )} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'column', width: '90%' }} >
                            <Text style={{
                                fontSize: 18,
                                fontFamily: 'Helvetica', width: '100%',
                                textAlign: 'center',
                                paddingTop: 15,
                                fontWeight: 'bold',
                                alignSelf: 'center',
                                color: '#d87136'
                            }}>#{item._id}</Text>
                            <Text style={{
                                fontSize: 17,
                                fontFamily: 'Helvetica', width: '100%',
                                textAlign: 'center',
                                paddingTop: 2,
                                paddingBottom: 15,
                                alignSelf: 'center',
                                fontWeight: 'bold'
                            }}>ORDER DATE: <Text style={{
                                fontSize: 17,
                                fontWeight: 'normal',
                                fontFamily: 'Helveticaregular',
                            }}>  {month} {date}, {year}</Text> </Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Icon name="left-open" size={25} style={{ transform: [{ rotate: '180deg' }], marginRight: 5 }} />
                        </View>
                    </View>
                </TouchableOpacity>

                <View style={{ borderTopWidth: 1, borderColor: '#414142', justifyContent: 'flex-start' }}>
                    <View style={{ flexDirection: "row" }}>

                        <Text style={{
                            fontSize: 17,
                            fontFamily: 'Helvetica', width: '100%',
                            width: 100,
                            padding: 5,
                            fontWeight: 'bold'

                        }}>Total:</Text>
                        <Text style={{
                            fontSize: 17,
                            fontFamily: 'Helveticaregular', width: '100%',
                            padding: 5,
                            fontWeight: 'normal'
                        }}>{currencyFormatter.FormateCurrency(item.BillingInfo.NetAmount)}</Text>
                    </View>

                    <View style={{ flexDirection: "row" }}>

                        <Text style={{
                            fontSize: 17,
                            fontFamily: 'Helvetica', width: '100%',
                            width: 100,
                            padding: 5,
                            fontWeight: 'bold'

                        }}>STATUS:</Text>
                        <Text style={{
                            fontSize: 17,
                            fontFamily: 'Helveticaregular', width: '100%',
                            padding: 5,
                            fontWeight: 'normal'


                        }}>{item.status}</Text>
                    </View>
                </View>
            </View>)
    }

    render() {


        return (


            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (

                    <View style={s.header}>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center' }}>

                            <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}><Icon name="left-open" size={23} style={{ fontWeight: 5 }} /></TouchableOpacity>
                        </View>
                        <View style={{ width: windowhalfwidthSecond * 3, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#262626', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>ORDERS</Text>
                        </View>
                        <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center' }}>
                                <Icon name="cancel-1" size={23} style={{ fontWeight: 5 }} ></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                )}

            >
                {this.state.loading && (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100 }}>

                    <ActivityIndicator size="large" color="gray" />

                </View>)}
                {!this.state.loading && (<View style={{ borderColor: '#fff', }}>
                    {this.state.dataSource.map((image, index) => this.render1(image, index))}
                    {/* {console.log("maduthuuuuuuu",this.state.dataSource)} */}
                </View>)}




            </StickyHeaderFooterScrollView>
        )
    }

}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },

});
