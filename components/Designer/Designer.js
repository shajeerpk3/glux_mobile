import React, { Component } from 'react';
import { TextInput, ScrollView, FlatList, SafeAreaView, TouchableOpacity, StyleSheet, Dimensions, Text, View } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from '../../constants/Settings';
import SearchBar from 'react-native-search-bar'
import GluxHome from "../GluxHome/Home";
import Api from "../../utils/Api/Designer";
import { NavigationEvents } from "react-navigation";

const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const Designers = [
    "FENDI",
    "GUCCI",
    "LOUIS VUITTON",
    "LOVE MOSCHINO",
    "PRADA",
    "TORY BURCH",
    "FURLA",
    "SAINT LAURENT"
]
export default class Designer extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            searchData: []
        };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);

    }

    handleRefresh() {
        Settings.category_filter = '';
        Settings.sortby_filter = '';
        Settings.Designer_filter = '';
        Settings.Gender_filter = '';
        Settings.AllPrtoductFilterName = '';
    }


    renderPage(Name, index) {
        return (
            <View key={index} style={{ width: '100%', borderBottomColor: '#c6c6c6', borderBottomWidth: 1 }}>
                <View key={index} style={{ flexDirection: "column", width: '100%' }}>
                    <TouchableOpacity style={{ width: '100%' }} onPress={this.ProductDetails.bind(this, Name)}>
                        <Text style={{
                            fontSize: 14,
                            fontFamily: 'lucidabrightdemiregular', width: '100%',
                            textAlign: 'left',
                            paddingTop: 20,
                            paddingBottom: 20,
                            alignSelf: 'center',
                        }}>{Name}</Text></TouchableOpacity>

                </View>
            </View>
        )
    }

    handleInputChangeName = (text) => {

        const newData = Designers.filter(item => {

            const itemData = `${item.toUpperCase()}   `;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });

        // console.log("newData", newData)
        this.setState({
            searchData: newData
        })

        Settings.Designer_filter = text
    }
    onSubmitEdit(item) {
       
        Settings.Designer_filter = item;
        Settings.nav.navigate("ShopallStack")
    }
    ProductDetails(Name) {
      
        Settings.Designer_filter = Name;
        Settings.nav.navigate("ShopallStack")
    }

    async componentDidMount() {
        // Api.Designer({
        //     limit: 10,
        //     page: 1
        //   }

        //   )
        //     .then((responseJson) => {
        //         this.setState({ searchData: responseJson.docs })



        //     })
        //     .catch((error) => {
        //       console.error(error);
        //     });
        this.setState({ searchData: Designers })
    }


    render() {
        return (
            <View >
                <View style={{ backgroundColor: '#fff' }}>
                    <StickyHeaderFooterScrollView
                        makeScrollable={true}
                        fitToScreen={true}
                        contentBackgroundColor={'#fff'}

                        renderStickyHeader={() => (

                            <SafeAreaView>
                                <GluxHome></GluxHome>
                                <View style={s.header}>

                                    <View style={{ marginLeft: 5, margin: 2, width: 350, height: 30, borderRadius: 10, borderWidth: 1, paddingRight: 5, paddingLeft: 6, borderColor: 'gray', alignItems: "center", alignContent: 'center', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row" }}>
                                        <Icon2 name="Search" size={16} style={{ fontWeight: 5 }} />
                                        <TextInput
                                            onChangeText={this.handleInputChangeName}
                                            onSubmitEditing={this.onSubmitEdit.bind(this)}
                                            value={this.state.Name}
                                            clearButtonMode='while-editing'
                                            style={{ borderWidth: 0, borderColor: '#fff', paddingRight: 5, paddingLeft: 6, width: '100%' }}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>
                                </View>

                            </SafeAreaView>
                        )}


                    >

                        <View style={{ margin: 20 }}>
                            <NavigationEvents
                                onWillFocus={payload => {
                                    this.handleRefresh();
                                }}
                            />
                            {this.state.searchData.map((image, index) => this.renderPage(image, index))}

                        </View>
                    </StickyHeaderFooterScrollView>

                </View>
            </View >
        );
    }
}

const s = StyleSheet.create({
    containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {


        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',

    },
});