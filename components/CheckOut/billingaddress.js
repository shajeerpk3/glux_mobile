import React from 'react';
import { TextInput, Alert, Animated, WebView, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, View, Image, StyleSheet, Platform, Text } from 'react-native';
import currencyFormatter from '../../constants/currncy_DateFormate';
import Api from "../../utils/Api/ProductDetailsApis";
import Api2 from "../../utils/Api/AccountDetails";
import commonsettings from '../../constants/commonsettings';
import icoMoonConfig from '../../src/selection.json';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Settings from '../../constants/Settings';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";


const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;


export default class BillingDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          
            BillingTab: false,
            ItemSummeryTab: false,
           
        };


    }

    ClickBilliingTab() {


        if (this.state.BillingTab == false) {

          
            this.setState({ BillingTab: true })
        } else {
            
            this.setState({ BillingTab: false })
        }


    }
    ClickBillingTabAddress() {
        Settings.nav.navigate("CheckOutAddress")
    }


    async componentDidMount() {

        Api2.GetBillingAndShippingAddress()
            .then((responseJson) => {
                this.setState({ address: responseJson });
                if (this.state.address[0].B_Fullname != undefined && this.state.address[0].B_Fullname != '' && this.state.address[0].B_Fullname != null) {
                    this.setState({ addresssate: true })
                    Settings.BillingAddressExistance=true
                    Settings.Billingaddress=responseJson
                }
                else {
                    this.setState({ addresssate: false })
                    Settings.BillingAddressExistance=false
                    Settings.Billingaddress=''
                }

            })
            .catch((error) => {
                console.log(error.message)
            });
    }




    render() {
        return (
            <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20,marginTop: 5 }}>

                <View style={{ borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                    <TouchableOpacity onPress={this.ClickBilliingTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                        <View></View>
                        <Text style={{
                            fontSize: 18,
                            fontFamily: 'Helvetica',
                            textAlign: 'center',
                            alignSelf: 'center',
                            fontWeight: 'bold'
                        }}>BILLING ADDRESS
                        </Text>
                        <View style={{ justifyContent: 'center', alignItems: "center" }}>
                            {!this.state.BillingTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                }} />)}

                            {this.state.BillingTab && (
                                <Icon name="left-open" size={22} style={{
                                    marginRight: 15, textAlign: 'center',
                                    alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                }} />
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                {this.state.BillingTab &&( <View>

                    <View style={{ margin: 5, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                        <View style={{ flexDirection: 'column', padding: 4, paddingLeft: 8, width: '78%' }}>
                            {!this.state.addresssate && (<Text style={{
                                fontSize: 16,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                                padding: 4
                            }}>You do not currently have a shipping address set up on your account</Text>)}
                            {this.state.addresssate && (<View>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>{this.state.address[0].B_Fullname}</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>{this.state.address[0].B_Address1}</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>{this.state.address[0].B_Address2}</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>{this.state.address[0].B_Address3}</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>SINGAPORE ({this.state.address[0].B_Postalcode})</Text>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: 'Helveticaregular',
                                    textAlign: 'left',
                                    alignSelf: 'flex-start',
                                    padding: 4
                                }}>{this.state.address[0].B_ContactNo}</Text>
                            </View>)}


                        </View>

                        <View>
                            <TouchableOpacity onPress={this.ClickBillingTabAddress.bind(this)} style={{ alignItems: "center", width: 50 }}>
                                <Icon name="left-open" size={22} style={{ marginRight: 15, transform: [{ rotate: '180deg' }] }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>)}



            </View>

        )
    }




}