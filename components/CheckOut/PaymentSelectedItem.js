import React from 'react';
import { Image, TouchableOpacity, Dimensions, View, StyleSheet, Text } from 'react-native';
import commonsettings from '../../constants/commonsettings';
import icoMoonConfig from '../../src/selection.json';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import Settings from '../../constants/Settings';
// import S from '../../assets/images/Card';

const Icon = createIconSetFromFontello(fontelloConfig);

const windowheight = Dimensions.get('window').height;
const windowwidth = Dimensions.get('window').width;

export default class PaymentSelectedMethod extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        PaymentSelectedMethod:2
        }

    }



async componentDidMount(){
    this.setState({PaymentSelectedMethod:Settings.selectedPaymentMode})
}





    render() {
        return (
            <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 5 }}>

                <View style={{width: '100%', borderColor: '#c6c6c6', borderBottomWidth: 1, height: 50, }}>
                    <View style={{ width: '100%',justifyContent:'center', alignItems: "center", flexDirection: 'row', height: 50 }}>
                        <View></View>
                        <Text style={{
                            fontSize: 18,
                            fontFamily: 'Helvetica',
                            textAlign: 'center',
                            alignSelf: 'center',
                            fontWeight: 'bold'
                        }}>PAYMENT METHOD
                        </Text>

                    </View>
                </View>

               <View style={{ flexDirection: 'column' }}>
               {this.state.PaymentSelectedMethod==1 &&( <View style={{ margin: 10, marginBottom: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column' }}>

                        <View style={{ backgroundColor: '#fff', borderColor: '#c6c6c6', borderBottomWidth: 1, flexDirection: 'row' }}>


                            <View style={{ width: 110, flexDirection: 'row' }}>
                                <Image
                                    resizeMode="contain"
                                    source={require('../../assets/images/Card/Visa.png')}
                                    style={styles.CardChiled}
                                />

                                <Image
                                    resizeMode="contain"
                                    source={require('../../assets/images/Card/MasterCard.png')}
                                    style={styles.CardChiled1}
                                />
                            </View>

                            <Text style={{
                                fontSize: 18,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',
                            }}>Credit/Debit Card </Text>



                        </View>


                    </View>)}



                    {this.state.PaymentSelectedMethod==2 &&(     <View style={{ margin: 10, marginBottom: 5, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column' }}>


                        <View style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
                            <Image
                                resizeMode="contain"
                                source={require('../../assets/images/Card/PayPal.png')}
                                style={styles.CardHeadPaypall}
                            />

                            <Text style={{
                                fontSize: 18,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',

                            }}>Paypal
                            </Text>
                        </View>


                    </View>)}





                    {this.state.PaymentSelectedMethod==3 &&(   <View style={{ margin: 10, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column' }}>


                        <View style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
                            <Image
                                resizeMode="contain"
                                source={require('../../assets/images/Card/Installment.jpg')}
                                style={styles.CardHead}
                            />

                            <Text style={{
                                fontSize: 18,
                                fontFamily: 'Helveticaregular',
                                textAlign: 'center',
                                alignSelf: 'center',

                            }}>Instalment Plan
                            </Text>
                        </View>


                    </View>)}




                </View>



            </View>
        )
    }

}
const styles = StyleSheet.create({

    CardHead: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,

        resizeMode: 'contain'
    },
    CardHeadPaypall: {

        overflow: 'visible',
        height: 60,
        width: 50,
        margin: 15,
        marginTop: 1,
        marginBottom: 1,
        resizeMode: 'contain'
    },
    CardChiled: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,
        marginLeft: 25,
        marginRight: 2,
        resizeMode: 'contain'
    },
    CardChiled1: {

        overflow: 'visible',
        height: 30,
        width: 35,
        margin: 15,
        marginLeft: 3,
        resizeMode: 'contain'
    },


});