import settingss from '../../constants/Settings';

const servername = settingss.serverUrl1;
const endUrl = {
    getShopall: servername + "/product/list",
    GetCategory:servername + "/product/category",
    GetDesigner:servername + "/product/ui/home/brands"
  
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
    } catch (e) {
        bodyIsJson = false;
    }

    const fullUrl = endpoint + constructParams(params);
    // console.log(fullUrl)
    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...contentTypeHeader,
        },
        body: body,
    };

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {


            if (!response.ok) {
                return Promise.reject(json);
            }
           
            return json;
        });

}

function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}



function getShopall(params) {
    // console.log("params",params)
    return callApi(endUrl.getShopall, "GET", null, params);
   
}

function GetCategory(params) {
    return callApi(endUrl.GetCategory, "GET", null, params);
}

function GetDesigner(params) {
    return callApi(endUrl.GetDesigner, "GET", null, params);
}



export default {
    getShopall,
    GetCategory,
    GetDesigner
};